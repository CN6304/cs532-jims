/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package booking;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class BookingValidation {

    ///overlaps: CheckName, CheckPhoneNumber,
    ///          CheckCity, CheckState, 
    ///          CheckCountry, CheckColor
    //// Inmates Section ////  
    //reuse for emergency contacts 
    public Boolean CheckName(String s) {
        return s.matches("^[a-zA-Z -.']*$");
    }

    public Boolean CheckMM(String m, String d, String y) {
        if(((m.length() == 2 && m.matches("[\\d]+"))
                && (Integer.parseInt(m) >= 1 && Integer.parseInt(m) <= 12))
                || (m.length() == 0)) {
            if(m.length() == 0) {
                return true;
            }
            if(Integer.parseInt(m) == 1 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 2 && d.length() == 2) {
                if(((y.length() == 4) && (Integer.parseInt(y) % 4 == 0)) && ((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 29))) {
                    return false;
                }
                if(((y.length() == 4) && (Integer.parseInt(y) % 4 != 0)) && ((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 28))) {
                    return false;
                }
                if((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 29)) {
                    return false;
                }
            }
            else if(Integer.parseInt(m) == 3 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 4 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if(Integer.parseInt(m) == 5 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 6 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if(Integer.parseInt(m) == 7 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 8 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 9 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if(Integer.parseInt(m) == 10 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if(Integer.parseInt(m) == 11 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if(Integer.parseInt(m) == 12 && d.length() == 2 && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public Boolean CheckDD(String m, String d, String y) {
        if((d.length() == 2 && d.matches("[\\d]+")) || (d.length() == 0)) {
            if(d.length() == 0) {
                return true;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 1)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 2))) {
                if(((y.length() == 4) && (Integer.parseInt(y) % 4 == 0)) && ((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 29))) {
                    return false;
                }
                if(((y.length() == 4) && (Integer.parseInt(y) % 4 != 0)) && ((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 28))) {
                    return false;
                }
                if((Integer.parseInt(d) < 1) || (Integer.parseInt(d) > 29)) {
                    return false;
                }
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 3)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 4)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 5)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 6)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 7)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 8)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 9)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 10)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 11)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 30)) {
                return false;
            }
            else if((m.length() == 2 && (Integer.parseInt(m) == 12)) && (Integer.parseInt(d) < 1 || Integer.parseInt(d) > 31)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public Boolean CheckYYYY(String m, String d, String y) {
        if((y.length() == 4 && y.matches("[\\d]+")) || (y.length() == 0)) {
            if(y.length() == 0) {
                return true;
            }
            else if(m.length() == 2 && Integer.parseInt(m) == 2 && d.length() == 2 && Integer.parseInt(y) % 4 == 0 && Integer.parseInt(d) > 29) {
                return false;
            }
            else if(m.length() == 2 && Integer.parseInt(m) == 2 && d.length() == 2 && Integer.parseInt(y) % 4 != 0 && Integer.parseInt(d) > 28) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int CalcAge(int m, int d, int y) {
        Calendar cal = new GregorianCalendar(y, m - 1, d);
        Calendar now = new GregorianCalendar();
        int res = now.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
        if((cal.get(Calendar.MONTH)) > now.get(Calendar.MONTH)) {
            res--;
        }
        else if((((cal.get(Calendar.MONTH)) == now.get(Calendar.MONTH))
                && (cal.get(Calendar.DAY_OF_MONTH) > now.get(Calendar.DAY_OF_MONTH)))) {
            res--;
        }
        return res;
    }

    //reuse for emergency contacts
    public Boolean CheckPhoneNumber(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckPhoneNumber1or2(String s) {
        if(s.length() != 0 && s.length() != 3) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckPhoneNumber3(String s) {
        if(s.length() != 0 && s.length() != 4) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckSSN(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckSSN1(String s) {
        if(s.length() != 0 && s.length() != 3) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckSSN2(String s) {
        if(s.length() != 0 && s.length() != 2) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckSSN3(String s) {
        if(s.length() != 0 && s.length() != 4) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckSex(String s) {
        if(s.length() != 0 && s.length() != 1) {
            return false;
        }
        return s.matches("^[mMfF]*$");
    }

    public Boolean CheckHeightFeet(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckHeightInches(String s) {
        if(s.isEmpty()) {
            return true;
        }
        if(s.matches("[\\d]+$")) {
            int hInches = Integer.parseInt(s);
            if(hInches >= 0 && hInches <= 11) {
                return true;
            }
        }
        return false;
    }

    public Boolean CheckWeight(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckDescent(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    //use for car color, hair color, eye color
    public Boolean CheckColor(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckDLNumber(String s) {
        return s.matches("^[a-zA-Z0-9 ]*$");
    }

    public Boolean CheckGAStatus(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckOccupation(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckSkills(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckHGC(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    //// Emergency Contacts section ////
    public Boolean CheckRelationship(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    //// Addresses Section ////
    public Boolean CheckStreetNumber(String s) {
        return s.matches("^[a-zA-Z0-9 .]*$");
    }

    public Boolean CheckAptNumber(String s) {
        return s.matches("^[\\d]*$");
    }

    //reuse for inmate
    public Boolean CheckCity(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    //reuse for inmate
    public Boolean CheckState(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckZipCode(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckCountry(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckDistrict(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    ////Employers Section////
    public Boolean CheckBusinessType(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    ////Bookings Section////
    public Boolean CheckArrestingAgency(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckTime24(String s) {
        if(s.matches("^[\\d]*$")) {
            if(s.length() == 0) {
                return true;
            }
            else if(s.length() != 4) {
                return false;
            }
            else if(s.length() == 4 && s.charAt(0) > '2') {
                return false;
            }
            else if(s.length() == 4 && s.charAt(0) == '2' && s.charAt(1) > '3') {
                return false;
            }
            else if(s.length() == 4 && s.charAt(2) > '5') {
                return false;
            }
            return true;
        }
        return false;
    }

    public Boolean CheckVehicleDisposition(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckVehicleImpoundCompany(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckBailAmount(String s) {
        int period = 0;
        if(!s.matches("^[\\d.]*$")) {
            return false;
        }
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '.') {
                period++;
            }
        }
        if(period > 1) {
            return false;
        }
        return true;
    }

    public Boolean CheckPropertyCash(String s) {
        int period = 0;
        if(!s.matches("^[\\d.]*$")) {
            return false;
        }
        for(int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == '.') {
                period++;
            }
        }
        if(period > 1) {
            return false;
        }
        return true;
    }

    public Boolean CheckPropertyNonCash(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    ////Users Section////
    public Boolean CheckUserRole(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckPosition(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    ////Vehicles Section////
    public Boolean CheckPlateNumber(String s) {
        return s.matches("^[a-zA-Z0-9 ]*$");
    }

    public Boolean CheckMake(String s) {
        return s.matches("^[a-zA-Z ]*$");
    }

    public Boolean CheckModel(String s) {
        return s.matches("^[a-zA-Z0-9 ]*$");
    }

    public Boolean CheckYear(String s) {
        if(s.length() != 0 && s.length() != 4) {
            return false;
        }
        else {
            return s.matches("^[\\d]*$");
        }
    }

    public Boolean CheckBookingNumber(String s) {
        return s.matches("^[\\d]*$");
    }

    //////////////////////////////////////////////////////
    public Boolean CheckOnlyLetter(String s) {
        return s.matches("^[a-zA-Z -.']*$");
    }

    public Boolean CheckOnlyNumbers(String s) {
        return s.matches("^[\\d]*$");
    }

    public Boolean CheckOnlyLettersAndNumbers(String s) {
        return s.matches("^[a-zA-Z0-9 ]*$");
    }
}
