package report;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.AcroFields;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import inmate.Inmate;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CreateForm {

    private final String GENERAL_FILE = "PDFForms/General_Inmate.pdf";
    private final String PERSONAL_FILE = "PDFForms/Personal_Inmate.pdf";
    private final String VEHICLE_FILE = "PDFForms/Vehicle_Inmate.pdf";
    private final String DEPARTMENT_FILE = "PDFForms/Department_Inmate.pdf";

    private final String GENERAL_FILE_OUT = "PDFForms/temp/General_Inmate.pdf";
    private final String PERSONAL_FILE_OUT = "PDFForms/temp/Personal_Inmate.pdf";
    private final String VEHICLE_FILE_OUT = "PDFForms/temp/Vehicle_Inmate.pdf";
    private final String DEPARTMENT_FILE_OUT = "PDFForms/temp/Department_Inmate.pdf";

    private final String RESULT = "PDFForms/temp/RESULT.pdf";

    private final Inmate inmate;

    private ArrayList<String> files;

    public CreateForm(Inmate inmate, Boolean hasGeneral, Boolean hasPersonal, Boolean hasVehicle, Boolean hasDept) throws IOException, DocumentException {
        this.inmate = inmate;
        files = new ArrayList<>();

        for(File file : new File("PDFForms/temp").listFiles()) {
            file.delete();
        }

        if(hasGeneral) {
            createGeneral();
            files.add(GENERAL_FILE_OUT);
        }
        if(hasPersonal) {
            createPersonal();
            files.add(PERSONAL_FILE_OUT);
        }
        if(hasVehicle) {
            createVehicle();
            files.add(VEHICLE_FILE_OUT);
        }
        if(hasDept) {
            createDepartment();
            files.add(DEPARTMENT_FILE_OUT);
        }
        mergePDFs();
        File resultFile = new File(RESULT);
        Desktop desktop = Desktop.getDesktop();
        desktop.open(resultFile);
    }

    public void getFields(String namefile) throws IOException, DocumentException {
        PdfReader reader = new PdfReader(namefile);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("General Inmate_1.pdf"));
        AcroFields form = stamper.getAcroFields();

        //form.setField("text_1", "Bruno Lowagie");
        form.getFields().keySet().stream().forEach((i) -> {
            System.out.println("form.setField(\"" + i + "\"," + " );");
        });

        stamper.close();
        reader.close();
    }

    private void createGeneral() throws IOException, DocumentException {
        PdfReader reader = new PdfReader(GENERAL_FILE);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(GENERAL_FILE_OUT));
        AcroFields form = stamper.getAcroFields();

        form.setField("Suffix_NpYd0LoEyZjq7C*PPR9crg", inmate.suffix);
        form.setField("AKAs (separated by commas)_uZw9rcXVP1C8DJVB9yX1oA", inmate.aka);
        
        if(inmate.permanentTelephone1.isEmpty() || inmate.permanentTelephone1.isEmpty() || inmate.permanentTelephone1.isEmpty())
            form.setField("Telephone Number (Permanent)_I*fNPqMWHT*QaIIp-dFYsw","");
        else
            form.setField("Telephone Number (Permanent)_I*fNPqMWHT*QaIIp-dFYsw",
                "(" + inmate.permanentTelephone1 + ")" + inmate.permanentTelephone2 + "-" + inmate.permanentTelephone3);
        
        if(inmate.height1.isEmpty() || inmate.height1.isEmpty())
            form.setField("Height_67oceKvIADlCg9zAIOKAXQ", "");
        else
            form.setField("Height_67oceKvIADlCg9zAIOKAXQ", inmate.height1 + "\' " + inmate.height2 + "\"");
        
        if(inmate.temporaryAddressStreet.isEmpty() || inmate.temporaryAddressCity.isEmpty())
            form.setField("Temporary Address_AmMPXgO3icR01jT89Xc3*Q","");
        else
            form.setField("Temporary Address_AmMPXgO3icR01jT89Xc3*Q",
                inmate.temporaryAddressStreet + " " + inmate.temporaryAddressCity
                + ", " + inmate.temporaryAddressState + " " + inmate.temporaryAddressZip);
        form.setField("Hair Color_KiYZ2ZjQFeixTisap19zBQ", inmate.hairColor);
        form.setField("Descent_9abszmCSiA7iwmRUxYx9og", inmate.descent);
        form.setField("Scars/Marks/Tattoos_b7so2F020SiErKBrJt94Wg", inmate.SMT);
        
        if(inmate.permanentAddressStreet.isEmpty() || inmate.permanentAddressCity.isEmpty())
            form.setField("Permanent Address_2TNvX1o9YIOoY9gXgXLdpg","");
        else
            form.setField("Permanent Address_2TNvX1o9YIOoY9gXgXLdpg",
                inmate.permanentAddressStreet + " " + inmate.permanentAddressCity
                + ", " + inmate.permanentAddressState + " " + inmate.permanentAddressZip);
        form.setField("Sex_hJs7CF0xthrN86iKBaOaJQ", inmate.sex);
        form.setField("First Name_at8YAEtvXuC1WvZ75kEQkQ", inmate.firstName);
        
        if(inmate.temporaryTelephone1.isEmpty()||inmate.temporaryTelephone2.isEmpty()||inmate.temporaryTelephone3.isEmpty())
            form.setField("Telephone Number (Temporary)_78l2yNccAScX69llz370aw","");
        else
            form.setField("Telephone Number (Temporary)_78l2yNccAScX69llz370aw",
                "(" + inmate.temporaryTelephone1 + ")" + inmate.temporaryTelephone2 + "-" + inmate.temporaryTelephone3);
        form.setField("Middle Name_Sna0P2TTBVyUcF3aPYbF0Q", inmate.middleName);
        form.setField("Eye Color_9-rUZtA4EQB8hOZf6w*FBA", inmate.eyeColor);
        //form.setField("fc-int01-generateAppearances","");
        form.setField("Foreign National (Country)_44IQzOdIDILv712XztSDDQ", inmate.foreignNationalCountry);
        
        if(inmate.dob1.isEmpty()||inmate.dob2.isEmpty()||inmate.dob3.isEmpty())
            form.setField("Date of Birth_3eaM46aBOhWrbHR1M2n2YQ", "");
        else
            form.setField("Date of Birth_3eaM46aBOhWrbHR1M2n2YQ", inmate.dob1 + "/" + inmate.dob2 + "/" + inmate.dob3);
        
        if(inmate.placeOfBirthCity.isEmpty() || inmate.placeOfBirthState.isEmpty())
            form.setField("Place of Birth_-rA*hk9pgxaiO3Pn*ZW-2w", "");
        else
            form.setField("Place of Birth_-rA*hk9pgxaiO3Pn*ZW-2w", inmate.placeOfBirthCity + ", "
                + inmate.placeOfBirthState + " " + inmate.placeOfBirthCountry);
        form.setField("Last Name_t2nxlBtd*HzreN9Y6SmCZQ", inmate.lastName);
        
        if(inmate.SSN1.isEmpty())
            form.setField("Social Security Number_v9RSUmlhe*eIL2VmtwrDvQ", "");
        else
            form.setField("Social Security Number_v9RSUmlhe*eIL2VmtwrDvQ", inmate.SSN1 + "-"
                + inmate.SSN2 + "-" + inmate.SSN3);
        
        if(inmate.weight.isEmpty())
            form.setField("Weight_0Bl-g8xRRJYMu42V4VOZiA", "");
        else
            form.setField("Weight_0Bl-g8xRRJYMu42V4VOZiA", inmate.weight + "lb");
        form.setField("Age_aBRR750MfBj9DTxzgFi6iw", inmate.age);
        stamper.close();
        reader.close();
    }

    private void createPersonal() throws IOException, DocumentException {
        PdfReader reader = new PdfReader(PERSONAL_FILE);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(PERSONAL_FILE_OUT));
        AcroFields form = stamper.getAcroFields();

        form.setField("Type of Business_U9WbV9P4lPTvMPdlJUv0HA", inmate.employerTypeOfBusiness);
        form.setField("Gang Affiliations (Gang Name)_dCmZvu-R5t*6aDnM3eCXJg", inmate.gangAffiliationName);
        form.setField("Gang Location_NXBGco4g5Qq*ddTSkD5sLg", inmate.gangAffiliationCity);
        form.setField("Skills_qC3pVeHxQ-pHUNkmKKConw", inmate.skills);
        
        if(inmate.employerAddressCity.isEmpty())
            form.setField("Employer Address_-3k2ygZp7LzrFe97PqVqzA", "");
        else
            form.setField("Employer Address_-3k2ygZp7LzrFe97PqVqzA", inmate.employerAddressStreet + " "
                + inmate.employerAddressCity + ", " + inmate.employerAddressState + " " + inmate.employerAddressZip);
        
        if(inmate.employerTelephone1.isEmpty()||inmate.employerTelephone2.isEmpty()||inmate.employerTelephone3.isEmpty())
            form.setField("Employer Telephone Number_wE067RLIdVyC94fuatnW4A", "");
        else
            form.setField("Employer Telephone Number_wE067RLIdVyC94fuatnW4A", "(" + inmate.employerTelephone1 + ")"
                + inmate.employerTelephone2 + "-" + inmate.employerTelephone3);
        
        if(inmate.eContactAddressCity.isEmpty())
            form.setField("Emergency Contact (Address)_lOloE*FalAzDrvwE8tQrZg", "");
        else
            form.setField("Emergency Contact (Address)_lOloE*FalAzDrvwE8tQrZg", inmate.eContactAddressStreet + " "
                + inmate.eContactAddressCity + ", " + inmate.eContactAddressState + " " + inmate.eContactAddressZip);
        // form.setField("fc-int01-generateAppearances",);
        
        if(inmate.eContactTelephone2.isEmpty())
            form.setField("Emergency Contact  (Telephone _cbNXlbOEUs5u4URsN0Wa*g", "");
        else
            form.setField("Emergency Contact  (Telephone _cbNXlbOEUs5u4URsN0Wa*g", "(" + inmate.eContactTelephone1 + ")"
                + inmate.eContactTelephone2 + "-" + inmate.eContactTelephone3);
        form.setField("Highest Grade Completed_12gTjJQ7Dl-o66A0VrlMgg", inmate.highestGradeCompleted);
        form.setField("Level of English_ZMeyw9zY3ayuesmFQMcTig", inmate.levelOfEnglish);
        form.setField("Relationship_oOoGQbR-7h8lcBsxgNaiTA", inmate.eContactRelationship);
        form.setField("Special Indentifiers_icHLX5qOR8rHJ-gMG90XqQ", inmate.specialIdentifiers);
        form.setField("Position_ljIZK7uXw4rKhijsYubzPA", inmate.position);
        form.setField("Employer Information_7BYzfThXywsMjPbZ4ZAdiA", inmate.employerName);
        form.setField("Gang Member Status_GQVj3WJNekKJIawWbf-xkw", inmate.gangAffiliationStatus);
        
        if(inmate.eContactLastName.isEmpty())
            form.setField("Emergency Contact (Name)_hFC9FPLj0j0G2iQwHI7vIg", "");
        else
            form.setField("Emergency Contact (Name)_hFC9FPLj0j0G2iQwHI7vIg", inmate.eContactLastName + ", " + inmate.eContactFirstName + " "
                + inmate.eContactMiddleName);
        form.setField("Occupation_5oqSjEUWiGYhXGMRGUnBgg", inmate.occupation);

        stamper.close();
        reader.close();
    }

    private void createVehicle() throws IOException, DocumentException {
        PdfReader reader = new PdfReader(VEHICLE_FILE);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(VEHICLE_FILE_OUT));
        AcroFields form = stamper.getAcroFields();

        form.setField("Impound Company_dL5U48362CNzZ8AQ5SSvSg", inmate.vehicleImpoundCompany);
        form.setField("Other (i_e_ Destroyed/Friend/F_ZgIcxGhGEEhPwr8GLOAygw", inmate.vehicleOther);
        form.setField("Vehicle Model_-FIXEvp1G2xG7qsj1Kktuw", inmate.vehicleModel);
        form.setField("State Validated_fW3KULVpXl7Yhop*QZII1w", inmate.vehiclePlateState);
        
        if(inmate.vehicleParkedAddressCity.isEmpty())
            form.setField("Vehicle Parked Address_6YwJiXrwoYXhVg*DL*A32Q", "");
        else
            form.setField("Vehicle Parked Address_6YwJiXrwoYXhVg*DL*A32Q", inmate.vehicleParkedAddressStreet + " "
                + inmate.vehicleParkedAddressCity + ", " + inmate.vehicleParkedAddressState + " " + inmate.vehicleParkedAddressZip);
        form.setField("Vehicle Color_Cul-GzlBMsS3bSp-CsK0WQ", inmate.vehicleColor);
        
        if(inmate.vehicleImpoundCompanyAddressCity.isEmpty())
            form.setField("Impound Company Address_hRivrGyAzF39wDq1LaoNtg", "");
        else
            form.setField("Impound Company Address_hRivrGyAzF39wDq1LaoNtg", inmate.vehicleImpoundCompanyAddressStreet + " "
                + inmate.vehicleImpoundCompanyAddressCity + ", " + inmate.vehicleImpoundCompanyAddressState + " " + inmate.vehicleImpoundCompanyAddressZip);
        form.setField("Driver_s License Number_q7j5ynAgBYHKZMa9nsp1xw", inmate.driversLicenseNumber);
        form.setField("State Issued_9xwRRYxMtqabotMg9RFrtw", inmate.driversLicenseState);
        //form.setField("fc-int01-generateAppearances",);
        form.setField("Year Validated_D3*cCb7SlEqH6i-RAjwNLg", inmate.vehicleYear);
        form.setField("Vehicle License Number_qZ1AQaw-*HChWaVsYD2edw", inmate.vehiclePlateNumber);
        form.setField("Vehicle Make_N9eVqVA1iVh*oMBrcgNJOA", inmate.vehicleMake);
        form.setField("Vehicle Disposition (Impounded_8Na9YLxF*d6*9dEmg**3Jg", inmate.vehicleDisposition);

        stamper.close();
        reader.close();
    }

    private void createDepartment() throws IOException, DocumentException {
        PdfReader reader = new PdfReader(DEPARTMENT_FILE);
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(DEPARTMENT_FILE_OUT));
        AcroFields form = stamper.getAcroFields();

        form.setField("Time Booked(24-Hour)_eW*iqzhi2l55Qs1cNBENMQ", inmate.timeBooked);
        
        if(inmate.dateArrested1.isEmpty())
            form.setField("Date Arrested_AHq6r0rGAeAvMH5V4NTlMQ", "");
        else
            form.setField("Date Arrested_AHq6r0rGAeAvMH5V4NTlMQ", inmate.dateArrested1 + "/" + inmate.dateArrested2 + "/" + inmate.dateArrested3);
        if(inmate.arrestLocationStreet.isEmpty())
            form.setField("Arrest Location_8TaAYB4Bpe417p3Xikst5w", "");
        else
            form.setField("Arrest Location_8TaAYB4Bpe417p3Xikst5w", inmate.arrestLocationStreet + ", "
                + inmate.arrestLocationCity);
        form.setField("Non-Cash Property (seperated b_qvUzDvJ3DmXAtpH3IkBYFw", inmate.nonCash);
        form.setField("Booking Clerk_s Dept__ryZrX1WsX-4RT3UIUFQorQ", inmate.bookingClerkDepartment);
        form.setField("Time Arrested_HF*xAnkVAzl8E4wc5EByDg", inmate.timeArrested);
        form.setField("Date Booked_KmkGBzz2gI2kf7JMwd8RdQ", inmate.bookingDate1 + "/" + inmate.bookingDate2 + "/" + inmate.bookingDate3);
        form.setField("Transporting Officer_s Dept__waDcXKMzoIKE4pr1ijCgig", inmate.transportingOfficerDepartment);
        form.setField("Arrest Bail_omWLqSHFo7k6tSprjUPyag", "$" + inmate.arrestBail);
        form.setField("Searching Officer_s Dept__Dr7Q8ckaFERJjdLvDkMTZg", inmate.searchingOfficerDepartment);
        form.setField("Arresting Agency_OXDu2XBOGKAtH4mG9N-MCw", inmate.arrestingAgency);
        form.setField("Reporting District_SujCCx8jGYINvL11bRMSAA", inmate.reportingDistrict);
        form.setField("Arrest Charge_BJdKZTu2jHNtZM5sjLM04A", inmate.arrestCharges);
        //form.setField("fc-int01-generateAppearances",);
        
        if(inmate.cash.isEmpty()){
            form.setField("Property  (Cash)_miebnGTOC1O1e01gsaVznQ", "");
        }
        else
            form.setField("Property  (Cash)_miebnGTOC1O1e01gsaVznQ", "$" + inmate.cash);
        form.setField("Booking Number_1IhjAvPtWqRyQyXJDgKpzA", "");
        form.setField("Arresting Officer_s Dept__7TVa6GmWlArYse7gGVonbg", inmate.arrestingOfficerDepartment);

        stamper.close();
        reader.close();
    }

    private void mergePDFs() throws IOException, DocumentException {
        Document document = new Document();
        // step 2
        PdfCopy copy = new PdfCopy(document, new FileOutputStream(RESULT));
        // step 3
        document.open();
        // step 4
        PdfReader reader;
        int n;

        for(String file : this.files) {
            // loop over the documents you want to concatenate
            reader = new PdfReader(file);
            // loop over the pages in that document
            n = reader.getNumberOfPages();
            for(int page = 0; page < n;) {
                copy.addPage(copy.getImportedPage(reader, ++page));
            }
            copy.freeReader(reader);
            reader.close();
        }
        // step 5
        document.close();
    }
}
