package report;

import booking.BookingControl;
import booking.BookingValidation;
import com.itextpdf.text.DocumentException;
import database.SQL_GET;
import database.SQL_SET;
import error_handle.HandlePopUp;
import inmate.Inmate;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import login.PasswordEncrypt;
import window_manager.WindowManager;

public class DisplayControl implements Initializable {

    // General Information ///////////////////////////////////////////////////////////////////////////////////////
    @FXML
    private TextField lastNameInmate, firstNameInmate, middleNameInmate, suffixInmate, akaInmate,
            dobInmateMM, dobInmateDD, dobInmateYYYY, ageInmate, pobInmateCity, pobInmateState, pobInmateCountry,
            SSN1, SSN2, SSN3, sexInmate, heightInchesInmate, heightFeetInmate, weightInmate, permaAdressInmate,
            permaCityInmate, permaStateInmate, permaZipInmate, permaTN1Inmate, permaTN2Inmate, permaTN3Inmate,
            tempAdressInmate, tempCityInmate, tempStateInmate, tempZipInmate, tempTN1Inmate, tempTN2Inmate, tempTN3Inmate;
    @FXML
    private ComboBox foreignNatInmateBool, foreignNatInmateCountry, hairColorInmate, descentInmate, eyeColorInmate;

    @FXML
    private TextArea SMTList;

    // Personal Information
    @FXML
    private TextField firstNameEC, lastNameEC, middleNameEC, addressEC, cityEC, stateEC, zipcodeEC, TN1EC, TN2EC, TN3EC,
            occupationInmate, HGCInmate, typeOfBusiness, position, employerName, addressEmployer, cityEmployer, stateEmployer,
            zipcodeEmployer, employerTN2, employerTN1, employerTN3, gangNameOther, gangLocation;
    @FXML
    private ComboBox relationshipEC, levelOfEnglishInmate, gangName, gangMembership, skillsInmate, specialIdentifiers;

    // Vehicle Information
    @FXML
    private CheckBox noVehicleCB;

    @FXML
    private TextField driverLicense, stateIssued, vehicleLicenseNumber, stateValidated, yearValidated,
            impoundCompanyManually, impoundComAddress, impoundComCity, impoundComState, impoundComZipcode,
            parkedAddress, parkedCity, parkedState, parkedZip;
    @FXML
    private ComboBox vehicleMake, vehicleModel, vehicleColor, impoundCompany, vehDisposition;
    @FXML
    private TextArea vehicleOther;

    // Department Record
    @FXML
    private TextField arrestLocation, arrestCity, arrestingAgency, arrestedMonth, arrestedDay, arrestedYear, arrestedTime,
            bookingNumber, bookedMonth, bookedDay, bookedYear, bookedTime, cashProperty, arrestBail;
    @FXML
    private ComboBox reportingDistrict, arrestingOfficerID, transportingOfficerID, bookingClerkDept, searchingOffDept, arrestCharges;

    @FXML
    private TextArea nonCashProperty;

    @FXML
    private TabPane tabPane;

    @FXML
    private Button submitInmate;

    @FXML
    private CheckBox generalCB, personalCB, vehicleCB, deptCB;

    Inmate displayInmate = WindowManager.displayInmate;

    private String inmateID;

    @FXML
    void quickPrint() throws IOException, DocumentException {
        Inmate in = createInmate();
        new CreateForm(in, generalCB.isSelected(), personalCB.isSelected(), vehicleCB.isSelected(), deptCB.isSelected());
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        inmateID = makeID(displayInmate);
        try {
            formSetUp();
        }
        catch(SQLException ex) {
            Logger.getLogger(DisplayControl.class.getName()).log(Level.SEVERE, null, ex);
        }

        errorCheck();

        foreignNatInmateBool.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                if(foreignNatInmateBool.getValue().toString().equals("Yes")) {
                    foreignNatInmateCountry.setDisable(false);
                }
                else {
                    foreignNatInmateCountry.setDisable(true);
                }
            }
        });

    }

    @FXML
    void noVehicle() {
        Boolean check = noVehicleCB.isSelected();

        driverLicense.setDisable(check);
        stateIssued.setDisable(check);
        vehicleLicenseNumber.setDisable(check);
        stateValidated.setDisable(check);
        yearValidated.setDisable(check);
        impoundCompanyManually.setDisable(check);
        impoundComAddress.setDisable(check);
        impoundComCity.setDisable(check);
        impoundComState.setDisable(check);
        impoundComZipcode.setDisable(check);
        parkedAddress.setDisable(check);
        parkedCity.setDisable(check);
        parkedState.setDisable(check);
        parkedZip.setDisable(check);
        vehicleMake.setDisable(check);
        vehicleModel.setDisable(check);
        vehicleColor.setDisable(check);
        impoundCompany.setDisable(check);
        vehDisposition.setDisable(check);
        vehicleOther.setDisable(check);
    }

    @FXML
    void submitInmatePressed() throws IOException {
        BookingValidation validate = new BookingValidation();

        String lastName = "• Inmate Last Name";
        String firstName = "• Inmate First Name";
        String midName = "• Inmate Middle Name";
        String sex = "• Sex";
        String feet = "• Height(feet)";
        String inches = "• Height(inches)";
        String weight = "• Weight";
        String arStNum = "• Arrest Location(Address)";
        String arCity = "• Arrest Location(City)";
        String arAgency = "• Arresting Agency";
        String arMM = "• Date Arrested(Month)";
        String arDD = "• Date Arrested(Day)";
        String arYYYY = "• Date Arrested(Year)";
        String arTime = "• Time Arrested";

        ArrayList<String> general = new ArrayList<>();
        general.clear();
        ArrayList<String> dept = new ArrayList<>();
        dept.clear();

        if(lastNameInmate.getText().isEmpty()) {
            general.add(lastName);
        }
        if(firstNameInmate.getText().isEmpty()) {
            general.add(firstName);
        }
        if(middleNameInmate.getText().isEmpty()) {
            general.add(midName);
        }
        if(sexInmate.getText().isEmpty()) {
            general.add(sex);
        }
        if(heightFeetInmate.getText().isEmpty()) {
            general.add(feet);
        }
        if(heightInchesInmate.getText().isEmpty()) {
            general.add(inches);
        }
        if(weightInmate.getText().isEmpty()) {
            general.add(weight);
        }
        if(arrestLocation.getText().isEmpty()) {
            dept.add(arStNum);
        }
        if(arrestCity.getText().isEmpty()) {
            dept.add(arCity);
        }
        if(arrestingAgency.getText().isEmpty()) {
            dept.add(arAgency);
        }
        if(arrestedMonth.getText().isEmpty()) {
            dept.add(arMM);
        }
        if(arrestedMonth.getText().isEmpty()) {
            dept.add(arDD);
        }
        if(arrestedMonth.getText().isEmpty()) {
            dept.add(arYYYY);
        }
        if(arrestedTime.getText().isEmpty()) {
            dept.add(arTime);
        }

        if(!general.isEmpty() || !dept.isEmpty()) {
            String error = "";
            if(!general.isEmpty()) {
                error += "General Information\n";
                for(String i : general) {
                    error += i + "\n";
                }
            }
            if(!dept.isEmpty()) {
                error += "\nDepartment Information\n";
                for(String i : dept) {
                    error += i + "\n";
                }
            }
            HandlePopUp.show("Submission Entry Error", "Cannot submit to database without required fields:\n\n" + error);
        }
        else {
            Inmate in = createInmate();
            SQL_SET.submitButtonPushed(in);
        }
    }

    // Checks the respected textfield for user defined error.
    private void errorCheck() {
        BookingValidation validate = new BookingValidation();

        //General Information
        lastNameInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!lastNameInmate.isFocused() && !validate.CheckName(lastNameInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Last Name \n\n"
                                + "• Last Name must only contain letters, hyphens, or spaces.\n\n"
                                + "Please enter a valid Last Name containing only letters, hyphens, and/or space characters only.");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    lastNameInmate.requestFocus();
                    lastNameInmate.selectAll();
                }
            }
        });
        firstNameInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!firstNameInmate.isFocused() && !validate.CheckName(firstNameInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: First Name \n\n"
                                + "• First Name must only contain letters, hyphens, or spaces.\n\n"
                                + "Please enter a valid First Name containing only letters, hyphens, and/or space characters only.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    firstNameInmate.requestFocus();
                    firstNameInmate.selectAll();
                }
            }
        });
        middleNameInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!middleNameInmate.isFocused() && !validate.CheckName(middleNameInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Middle Name \n\n"
                                + "• Middle Name must only contain letters, hyphens, or spaces.\n\n"
                                + "Please enter a valid Middle Name containing only letters, hyphens, and/or space characters only.");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    middleNameInmate.requestFocus();
                    middleNameInmate.selectAll();
                }
            }
        });
        suffixInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!suffixInmate.isFocused() && !validate.CheckOnlyLetter(suffixInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Suffix \n\n"
                                + "• Suffix must only contain letters, hyphens, or spaces.\n\n"
                                + "Please enter a valid Suffix containing only letters, hyphens, and/or space characters only.");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    suffixInmate.requestFocus();
                    suffixInmate.selectAll();
                }
            }
        });
        dobInmateMM.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                int month = 0;
                int day = 0;
                int year = 0;
                int age = 0;
                if((validate.CheckMM(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText()))
                        && (dobInmateMM.getText().length() == 2 && dobInmateDD.getText().length() == 2 && dobInmateYYYY.getText().length() == 4)) {
                    month = Integer.parseInt(dobInmateMM.getText());
                    day = Integer.parseInt(dobInmateDD.getText());
                    year = Integer.parseInt(dobInmateYYYY.getText());
                    age = validate.CalcAge(month, day, year);
                    if(age >= 0) {
                        ageInmate.setText(Integer.toString(validate.CalcAge(month, day, year)));
                    }
                }
                if(!dobInmateMM.isFocused() && !validate.CheckMM(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText())
                        && WindowManager.isOpenBookingWindow() || age < 0) {
                    try {
                        if(age < 0) {
                            HandlePopUp.show("Entry Error", "No birthdays in the future");
                        }
                        else {
                            HandlePopUp.show("Entry Error", "Invalid Date for Entry Field: Date of Birth (year) \n\n"
                                    + "• Date of Birth (year) must only contain number in the form YYYY \n\n"
                                    + "Please enter a valid Date of Birth (YYYY) containing only numbers.");
                        }
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    dobInmateMM.requestFocus();
                    dobInmateMM.setText("");
                    ageInmate.setText("");
                }
                else if(!dobInmateMM.isFocused() && (dobInmateMM.getText().matches("^[ ]*$")
                        || dobInmateDD.getText().matches("^[ ]*$")
                        || dobInmateYYYY.getText().matches("^[ ]*$"))) {
                    ageInmate.setText("");
                }
            }
        });
        dobInmateDD.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                int month = 0;
                int day = 0;
                int year = 0;
                int age = 0;
                if((validate.CheckDD(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText()))
                        && (dobInmateMM.getText().length() == 2 && dobInmateDD.getText().length() == 2 && dobInmateYYYY.getText().length() == 4)) {
                    month = Integer.parseInt(dobInmateMM.getText());
                    day = Integer.parseInt(dobInmateDD.getText());
                    year = Integer.parseInt(dobInmateYYYY.getText());
                    age = validate.CalcAge(month, day, year);
                    if(age >= 0) {
                        ageInmate.setText(Integer.toString(validate.CalcAge(month, day, year)));
                    }
                }
                if(!dobInmateDD.isFocused() && !validate.CheckDD(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText())
                        && WindowManager.isOpenBookingWindow() || age < 0) {
                    try {
                        if(age < 0) {
                            HandlePopUp.show("Entry Error", "No birthdays in the future");
                        }
                        else {
                            HandlePopUp.show("Entry Error", "Invalid Date for Entry Field: Date of Birth (year) \n\n"
                                    + "• Date of Birth (year) must only contain number in the form YYYY \n\n"
                                    + "Please enter a valid Date of Birth (YYYY) containing only numbers.");
                        }
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    dobInmateDD.requestFocus();
                    dobInmateDD.setText("");
                    ageInmate.setText("");
                }
                else if(!dobInmateDD.isFocused() && (dobInmateMM.getText().matches("^[ ]*$")
                        || dobInmateDD.getText().matches("^[ ]*$")
                        || dobInmateYYYY.getText().matches("^[ ]*$"))) {
                    ageInmate.setText("");
                }
            }
        });
        dobInmateYYYY.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                int month = 0;
                int day = 0;
                int year = 0;
                int age = 0;
                if((validate.CheckDD(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText()))
                        && (dobInmateMM.getText().length() == 2 && dobInmateDD.getText().length() == 2 && dobInmateYYYY.getText().length() == 4)) {
                    month = Integer.parseInt(dobInmateMM.getText());
                    day = Integer.parseInt(dobInmateDD.getText());
                    year = Integer.parseInt(dobInmateYYYY.getText());
                    age = validate.CalcAge(month, day, year);
                    if(age >= 0) {
                        ageInmate.setText(Integer.toString(validate.CalcAge(month, day, year)));
                    }
                }
                if(!dobInmateYYYY.isFocused() && !validate.CheckYYYY(dobInmateMM.getText(), dobInmateDD.getText(), dobInmateYYYY.getText())
                        && WindowManager.isOpenBookingWindow() || age < 0) {
                    try {
                        if(age < 0) {
                            HandlePopUp.show("Entry Error", "No birthdays in the future");
                        }
                        else {
                            HandlePopUp.show("Entry Error", "Invalid Date for Entry Field: Date of Birth (year) \n\n"
                                    + "• Date of Birth (year) must only contain number in the form YYYY \n\n"
                                    + "Please enter a valid Date of Birth (YYYY) containing only numbers.");
                        }
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    dobInmateYYYY.requestFocus();
                    dobInmateYYYY.setText("");
                    ageInmate.setText("");
                }
                else if(!dobInmateYYYY.isFocused() && (dobInmateMM.getText().matches("^[ ]*$")
                        || dobInmateDD.getText().matches("^[ ]*$")
                        || dobInmateYYYY.getText().matches("^[ ]*$"))) {
                    ageInmate.setText("");
                }
            }
        });
        pobInmateCity.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!pobInmateCity.isFocused() && !validate.CheckCity(pobInmateCity.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Place of Birth (City) \n\n"
                                + "• Place of Birth City must only contain letters \n\n"
                                + "Please enter a valid Place of Birth (City) containing only letters.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    pobInmateCity.requestFocus();
                    pobInmateCity.selectAll();
                }
            }
        });
        pobInmateState.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!pobInmateState.isFocused() && !validate.CheckState(pobInmateState.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Place of Birth (State) \n\n"
                                + "• Place of Birth State must only contain letters \n\n"
                                + "Please enter a valid Place of Birth (State) containing only letters.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    pobInmateState.requestFocus();
                    pobInmateState.selectAll();
                }
            }
        });
        pobInmateCountry.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!pobInmateCountry.isFocused() && !validate.CheckCity(pobInmateCountry.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Place of Birth (Country) \n\n"
                                + "• Place of Birth Country must only contain letters \n\n"
                                + "Please enter a valid Place of Birth (Country) containing only letters.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    pobInmateCountry.requestFocus();
                    pobInmateCountry.selectAll();
                }
            }
        });
        SSN1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!SSN1.isFocused() && !validate.CheckSSN1(SSN1.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Social Security Number \n\n"
                                + "• Social Security Number must only contain numbers \n\n"
                                + "Please enter a valid Social Security Number containing only numbers.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    SSN1.requestFocus();
                    SSN1.selectAll();
                }
            }
        });
        SSN2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!SSN2.isFocused() && !validate.CheckSSN2(SSN2.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Social Security Number \n\n"
                                + "• Social Security Number must only contain numbers \n\n"
                                + "Please enter a valid Social Security Number containing only numbers.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    SSN2.requestFocus();
                    SSN2.selectAll();
                }
            }
        });
        SSN3.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!SSN3.isFocused() && !validate.CheckSSN3(SSN3.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Social Security Number \n\n"
                                + "• Social Security Number must only contain numbers \n\n"
                                + "Please enter a valid Social Security Number containing only numbers.");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    SSN3.requestFocus();
                    SSN3.selectAll();
                }
            }
        });
        sexInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!sexInmate.isFocused() && !validate.CheckSex(sexInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Sex\n\n"
                                + "• Sex must be (M) or (F) only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    sexInmate.requestFocus();
                    sexInmate.selectAll();
                }
            }
        });
        heightFeetInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!heightFeetInmate.isFocused() && !validate.CheckHeightFeet(heightFeetInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Height(feet)\n\n"
                                + "• feet must only contain numbers \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    heightFeetInmate.requestFocus();
                    heightFeetInmate.selectAll();
                }
            }
        });
        heightInchesInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!heightInchesInmate.isFocused() && !validate.CheckHeightInches(heightInchesInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Height(inches)\n\n"
                                + "• inches must only contain numbers in the range 0 to 11 \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    heightInchesInmate.requestFocus();
                    heightInchesInmate.selectAll();
                }
            }
        });
        permaAdressInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaAdressInmate.isFocused() && !validate.CheckStreetNumber(permaAdressInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Address (Address)\n\n"
                                + "• Address must contain numbers and letters \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaAdressInmate.requestFocus();
                    permaAdressInmate.selectAll();
                }
            }
        });
        permaCityInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaCityInmate.isFocused() && !validate.CheckCity(permaCityInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Address (City)\n\n"
                                + "• City must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaCityInmate.requestFocus();
                    permaCityInmate.selectAll();
                }
            }
        });
        permaStateInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaStateInmate.isFocused() && !validate.CheckState(permaStateInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Address (State)\n\n"
                                + "• State must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaStateInmate.requestFocus();
                    permaStateInmate.selectAll();
                }
            }
        });
        permaZipInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaZipInmate.isFocused() && !validate.CheckZipCode(permaZipInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Address (Zip Code)\n\n"
                                + "• Zip Code must contain numbers only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaZipInmate.requestFocus();
                    permaZipInmate.selectAll();
                }
            }
        });
        permaTN1Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaTN1Inmate.isFocused() && !validate.CheckPhoneNumber1or2(permaTN1Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaTN1Inmate.requestFocus();
                    permaTN1Inmate.selectAll();
                }
            }
        });
        permaTN2Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaTN2Inmate.isFocused() && !validate.CheckPhoneNumber1or2(permaTN2Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaTN2Inmate.requestFocus();
                    permaTN2Inmate.selectAll();
                }
            }
        });
        permaTN3Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!permaTN3Inmate.isFocused() && !validate.CheckPhoneNumber3(permaTN3Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    permaTN3Inmate.requestFocus();
                    permaTN3Inmate.selectAll();
                }
            }
        });
        tempAdressInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempAdressInmate.isFocused() && !validate.CheckStreetNumber(tempAdressInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Address (Address)\n\n"
                                + "• Address must contain numbers and letters \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempAdressInmate.requestFocus();
                    tempAdressInmate.selectAll();
                }
            }
        });
        tempCityInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempCityInmate.isFocused() && !validate.CheckCity(tempCityInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Address (City)\n\n"
                                + "• City must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempCityInmate.requestFocus();
                    tempCityInmate.selectAll();
                }
            }
        });
        tempStateInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempStateInmate.isFocused() && !validate.CheckState(tempStateInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Address (State)\n\n"
                                + "• State must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempStateInmate.requestFocus();
                    tempStateInmate.selectAll();
                }
            }
        });
        tempZipInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempZipInmate.isFocused() && !validate.CheckZipCode(tempZipInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Permanent Address (Zip Code)\n\n"
                                + "• Zip Code must contain numbers only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempZipInmate.requestFocus();
                    tempZipInmate.selectAll();
                }
            }
        });
        tempTN1Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempTN1Inmate.isFocused() && !validate.CheckPhoneNumber1or2(tempTN1Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempTN1Inmate.requestFocus();
                    tempTN1Inmate.selectAll();
                }
            }
        });
        tempTN2Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempTN2Inmate.isFocused() && !validate.CheckPhoneNumber1or2(tempTN2Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempTN2Inmate.requestFocus();
                    tempTN2Inmate.selectAll();
                }
            }
        });
        tempTN3Inmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!tempTN3Inmate.isFocused() && !validate.CheckPhoneNumber3(tempTN3Inmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Temporary Telephone Number\n\n"
                                + "• Telephone Number must follow format(###-##-####) \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToGeneral();
                    tempTN3Inmate.requestFocus();
                    tempTN3Inmate.selectAll();
                }
            }
        });
////////////////////////////////Personal Information Section////////////////////////////////////////////////////////////////////////////////////        
        lastNameEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!lastNameEC.isFocused() && !validate.CheckName(lastNameEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact Last Name\n\n"
                                + "• Last Name must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    lastNameEC.requestFocus();
                    lastNameEC.selectAll();
                }
            }
        });
        firstNameEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!firstNameEC.isFocused() && !validate.CheckName(firstNameEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact First Name\n\n"
                                + "• First Name must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    firstNameEC.requestFocus();
                    firstNameEC.selectAll();
                }
            }
        });
        middleNameEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!middleNameEC.isFocused() && !validate.CheckName(middleNameEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact Middle Name\n\n"
                                + "• Middle Name must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    middleNameEC.requestFocus();
                    middleNameEC.selectAll();
                }
            }
        });
        addressEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!addressEC.isFocused() && !validate.CheckStreetNumber(addressEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact Address\n\n"
                                + "• Address must contain letters and numbers only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    addressEC.requestFocus();
                    addressEC.selectAll();
                }
            }
        });
        cityEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!cityEC.isFocused() && !validate.CheckCity(cityEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact City\n\n"
                                + "• City must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    cityEC.requestFocus();
                    cityEC.selectAll();
                }
            }
        });
        stateEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!stateEC.isFocused() && !validate.CheckState(stateEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact State\n\n"
                                + "• State must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    stateEC.requestFocus();
                    stateEC.selectAll();
                }
            }
        });
        zipcodeEC.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!zipcodeEC.isFocused() && !validate.CheckZipCode(zipcodeEC.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Emergency Contact Zip Code\n\n"
                                + "• Zip Code must contain numbers only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    zipcodeEC.requestFocus();
                    zipcodeEC.selectAll();
                }
            }
        });
        occupationInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!occupationInmate.isFocused() && !validate.CheckOccupation(occupationInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Occupation \n\n"
                                + "• Occupation must contain letters only \n\n");

                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    occupationInmate.requestFocus();
                    occupationInmate.selectAll();
                }
            }
        });
        HGCInmate.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!HGCInmate.isFocused() && !validate.CheckHGC(HGCInmate.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Highest Grade Completed \n\n"
                                + "• Highest Grade Completed must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    HGCInmate.requestFocus();
                    HGCInmate.selectAll();
                }
            }
        });
        employerName.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!employerName.isFocused() && !validate.CheckName(employerName.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Name \n\n"
                                + "• Employer Name must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    employerName.requestFocus();
                    employerName.selectAll();
                }
            }
        });
        addressEmployer.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!addressEmployer.isFocused() && !validate.CheckStreetNumber(addressEmployer.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Address \n\n"
                                + "• Employer Address must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    addressEmployer.requestFocus();
                    addressEmployer.selectAll();
                }
            }
        });
        cityEmployer.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!cityEmployer.isFocused() && !validate.CheckCity(cityEmployer.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer City \n\n"
                                + "• Employer City must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    cityEmployer.requestFocus();
                    cityEmployer.selectAll();
                }
            }
        });
        stateEmployer.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!stateEmployer.isFocused() && !validate.CheckState(stateEmployer.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer State \n\n"
                                + "• Employer State must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    stateEmployer.requestFocus();
                    stateEmployer.selectAll();
                }
            }
        });
        zipcodeEmployer.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!zipcodeEmployer.isFocused() && !validate.CheckZipCode(zipcodeEmployer.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Zip Code \n\n"
                                + "• Employer Zip Code must contain numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    zipcodeEmployer.requestFocus();
                    zipcodeEmployer.selectAll();
                }
            }
        });
        employerTN1.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!employerTN1.isFocused() && !validate.CheckPhoneNumber1or2(employerTN1.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Telephone Number \n\n"
                                + "• Employer Telephone Number must follow format(###-##-####) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    employerTN1.requestFocus();
                    employerTN1.selectAll();
                }
            }
        });
        employerTN2.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!employerTN2.isFocused() && !validate.CheckPhoneNumber1or2(employerTN2.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Telephone Number \n\n"
                                + "• Employer Telephone Number must follow format(###-##-####) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    employerTN2.requestFocus();
                    employerTN2.selectAll();
                }
            }
        });
        employerTN3.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!employerTN3.isFocused() && !validate.CheckPhoneNumber3(employerTN3.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Employer Telephone Number \n\n"
                                + "• Employer Telephone Number must follow format(###-##-####) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    employerTN3.requestFocus();
                    employerTN3.selectAll();
                }
            }
        });
        typeOfBusiness.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!typeOfBusiness.isFocused() && !validate.CheckBusinessType(typeOfBusiness.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Type Of Business \n\n"
                                + "• Type of Business must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    typeOfBusiness.requestFocus();
                    typeOfBusiness.selectAll();
                }
            }
        });
        position.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!position.isFocused() && !validate.CheckPosition(position.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Position \n\n"
                                + "• Position must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    position.requestFocus();
                    position.selectAll();
                }
            }
        });
        gangLocation.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!gangLocation.isFocused() && !validate.CheckCity(gangLocation.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Gang Location \n\n"
                                + "• Gang Location must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    gangLocation.requestFocus();
                    gangLocation.selectAll();
                }
            }
        });
        gangNameOther.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!gangNameOther.isFocused() && !validate.CheckName(gangNameOther.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Type Of Business \n\n"
                                + "• Type of Business must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToPersonal();
                    gangNameOther.requestFocus();
                    gangNameOther.selectAll();
                }
            }
        });
/////////////////////////////////Vehicle Section/////////////////////////////////////////////////////////////////////////////////////////////        
        driverLicense.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!driverLicense.isFocused() && !validate.CheckDLNumber(driverLicense.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Driver's License \n\n"
                                + "• Driver's License must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    driverLicense.requestFocus();
                    driverLicense.selectAll();
                }
            }
        });
        stateIssued.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!stateIssued.isFocused() && !validate.CheckState(stateIssued.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle State Issued \n\n"
                                + "• Vehicle State Issued must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    stateIssued.requestFocus();
                    stateIssued.selectAll();
                }
            }
        });
        vehicleLicenseNumber.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!vehicleLicenseNumber.isFocused() && !validate.CheckPlateNumber(vehicleLicenseNumber.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle License Number \n\n"
                                + "• Vehicle License Number must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    vehicleLicenseNumber.requestFocus();
                    vehicleLicenseNumber.selectAll();
                }
            }
        });
        stateValidated.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!stateValidated.isFocused() && !validate.CheckState(stateValidated.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle State Validated \n\n"
                                + "• Vehicle State Validated must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    stateValidated.requestFocus();
                    stateValidated.selectAll();
                }
            }
        });
        yearValidated.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!yearValidated.isFocused() && !validate.CheckYear(yearValidated.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Year Validated \n\n"
                                + "• Vehicle Year Validated must contain numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    yearValidated.requestFocus();
                    yearValidated.selectAll();
                }
            }
        });
        vehDisposition.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(vehDisposition.getValue() == "Impound" && WindowManager.isOpenBookingWindow()) {
                    impoundCompany.setDisable(false);
                    impoundCompanyManually.setDisable(false);
                    impoundComAddress.setDisable(false);
                    impoundComCity.setDisable(false);
                    impoundComState.setDisable(false);
                    impoundComZipcode.setDisable(false);
                    parkedAddress.setDisable(true);
                    parkedCity.setDisable(true);
                    parkedState.setDisable(true);
                    parkedZip.setDisable(true);
                    vehicleOther.setDisable(true);
                }
                else if(vehDisposition.getValue() == "Parked" && WindowManager.isOpenBookingWindow()) {
                    impoundCompany.setDisable(true);
                    impoundCompanyManually.setDisable(true);
                    impoundComAddress.setDisable(true);
                    impoundComCity.setDisable(true);
                    impoundComState.setDisable(true);
                    impoundComZipcode.setDisable(true);
                    parkedAddress.setDisable(false);
                    parkedCity.setDisable(false);
                    parkedState.setDisable(false);
                    parkedZip.setDisable(false);
                    vehicleOther.setDisable(true);
                }
                else if(vehDisposition.getValue() == "Other" && WindowManager.isOpenBookingWindow()) {
                    impoundCompany.setDisable(true);
                    impoundCompanyManually.setDisable(true);
                    impoundComAddress.setDisable(true);
                    impoundComCity.setDisable(true);
                    impoundComState.setDisable(true);
                    impoundComZipcode.setDisable(true);
                    parkedAddress.setDisable(true);
                    parkedCity.setDisable(true);
                    parkedState.setDisable(true);
                    parkedZip.setDisable(true);
                    vehicleOther.setDisable(false);
                }
                tabToVehicle();
            }
        });
        impoundCompanyManually.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!impoundCompanyManually.isFocused() && !validate.CheckVehicleImpoundCompany(impoundCompanyManually.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Impound Company (Other) \n\n"
                                + "• Vehicle Impound Company (Other) must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    impoundCompanyManually.requestFocus();
                    impoundCompanyManually.selectAll();
                }
            }
        });
        impoundComAddress.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!impoundComAddress.isFocused() && !validate.CheckStreetNumber(impoundComAddress.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Impound Company Address \n\n"
                                + "• Vehicle Impound Company Address must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    impoundComAddress.requestFocus();
                    impoundComAddress.selectAll();
                }
            }
        });
        impoundComCity.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!impoundComCity.isFocused() && !validate.CheckCity(impoundComCity.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Impound Company City \n\n"
                                + "• Vehicle Impound Company City must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    impoundComCity.requestFocus();
                    impoundComCity.selectAll();
                }
            }
        });
        impoundComState.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!impoundComState.isFocused() && !validate.CheckState(impoundComState.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Impound Company State \n\n"
                                + "• Vehicle Impound Company State must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    impoundComState.requestFocus();
                    impoundComState.selectAll();
                }
            }
        });
        impoundComZipcode.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!impoundComZipcode.isFocused() && !validate.CheckZipCode(impoundComZipcode.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Vehicle Impound Company Zip Code \n\n"
                                + "• Vehicle Impound Company Zip Code must contain numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    impoundComZipcode.requestFocus();
                    impoundComZipcode.selectAll();
                }
            }
        });
        parkedAddress.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!parkedAddress.isFocused() && !validate.CheckStreetNumber(parkedAddress.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Parked Vehicle Address \n\n"
                                + "• Parked Vehicle Address must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    parkedAddress.requestFocus();
                    parkedAddress.selectAll();
                }
            }
        });
        parkedCity.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!parkedCity.isFocused() && !validate.CheckCity(parkedCity.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Parked Vehicle City \n\n"
                                + "• Parked Vehicle City must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    parkedCity.requestFocus();
                    parkedCity.selectAll();
                }
            }
        });
        parkedState.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!parkedState.isFocused() && !validate.CheckState(parkedState.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Parked Vehicle State \n\n"
                                + "• Parked Vehicle State must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    parkedState.requestFocus();
                    parkedState.selectAll();
                }
            }
        });
        parkedZip.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!parkedZip.isFocused() && !validate.CheckZipCode(parkedZip.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Parked Vehicle Zip Code \n\n"
                                + "• Parked Vehicle Zip Code must contain numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToVehicle();
                    parkedZip.requestFocus();
                    parkedZip.selectAll();
                }
            }
        });
/////////////////////////////////////Department Section/////////////////////////////////////////////////////////////////////////////////////////
        arrestLocation.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!arrestLocation.isFocused() && !validate.CheckStreetNumber(arrestLocation.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Arrest Location \n\n"
                                + "• Arrest Location must contain letters and numbers only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    arrestLocation.requestFocus();
                    arrestLocation.selectAll();
                }
            }
        });
        arrestCity.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!arrestCity.isFocused() && !validate.CheckCity(arrestCity.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Arrest Location City \n\n"
                                + "• Arrest Location City must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    arrestCity.requestFocus();
                    arrestCity.selectAll();
                }
            }
        });
        arrestingAgency.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!arrestingAgency.isFocused() && !validate.CheckArrestingAgency(arrestingAgency.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Arresting Agency \n\n"
                                + "• Arresting Agency must contain letters only \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    arrestingAgency.requestFocus();
                    arrestingAgency.selectAll();
                }
            }
        });
        arrestedTime.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!arrestedTime.isFocused() && !validate.CheckTime24(arrestedTime.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Time Arrested \n\n"
                                + "• Time Arrested must contain numbers only in a (24-hour base) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    arrestedTime.requestFocus();
                    arrestedTime.selectAll();
                }
            }
        });
        cashProperty.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!cashProperty.isFocused() && !validate.CheckPropertyCash(cashProperty.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Property Cash \n\n"
                                + "• Property Cash must contain numbers only and at most one period(.) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    cashProperty.requestFocus();
                    cashProperty.selectAll();
                }
            }
        });
        arrestBail.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if(!arrestBail.isFocused() && !validate.CheckBailAmount(arrestBail.getText())
                        && WindowManager.isOpenBookingWindow()) {
                    try {
                        HandlePopUp.show("Entry Error", "Invalid Entry Field: Arrest Bail \n\n"
                                + "• Arrest Bail must contain numbers only and at most one period(.) \n\n");
                    }
                    catch(IOException ex) {
                        Logger.getLogger(BookingControl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    tabToDept();
                    arrestBail.requestFocus();
                    arrestBail.selectAll();
                }
            }
        });
    }

    // Set up the form. Fills in user-mained list.
    private void formSetUp() throws SQLException {
        SQL_GET fetch = new SQL_GET();
        foreignNatInmateBool.setValue(displayInmate.foreignNationalStatus);
        foreignNatInmateCountry.setValue(displayInmate.foreignNationalCountry);
        descentInmate.setValue(displayInmate.descent);
        hairColorInmate.setValue(displayInmate.hairColor);
        eyeColorInmate.setValue(displayInmate.eyeColor);

        relationshipEC.setValue(displayInmate.eContactRelationship);
        skillsInmate.setValue(displayInmate.skills);
        levelOfEnglishInmate.setValue(displayInmate.levelOfEnglish);
        specialIdentifiers.setValue(displayInmate.specialIdentifiers);
        gangName.setValue(displayInmate.gangAffiliationName);
        gangMembership.setValue(displayInmate.gangAffiliationStatus);

        vehicleMake.setValue(displayInmate.vehicleMake);
        vehicleModel.setValue(displayInmate.vehicleModel);
        vehicleColor.setValue(displayInmate.vehicleColor);
        vehDisposition.setValue(displayInmate.vehicleDisposition);
        impoundCompany.setValue(displayInmate.vehicleImpoundCompany);

        reportingDistrict.setValue(displayInmate.reportingDistrict);
        arrestCharges.setValue(displayInmate.arrestCharges);
        arrestingOfficerID.setValue(displayInmate.arrestingOfficerDepartment);
        transportingOfficerID.setValue(displayInmate.transportingOfficerDepartment);
        bookingClerkDept.setValue(displayInmate.bookingClerkDepartment);
        searchingOffDept.setValue(displayInmate.searchingOfficerDepartment);

        // Populate List
        foreignNatInmateBool.getItems().addAll(FXCollections.observableArrayList("Yes", "No"));
        foreignNatInmateCountry.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList("Other"))); // DUMMY DATA
        descentInmate.getItems().addAll(FXCollections.observableList(fetch.getDescentList()));
        hairColorInmate.getItems().addAll(FXCollections.observableList(fetch.getHairColorList()));
        eyeColorInmate.getItems().addAll(FXCollections.observableList(fetch.getEyeColorList()));

        relationshipEC.getItems().addAll(FXCollections.observableArrayList(fetch.getRelationshipsList()));
        skillsInmate.getItems().addAll(FXCollections.observableList(fetch.getSkillsList()));
        levelOfEnglishInmate.getItems().addAll(FXCollections.observableList(fetch.getEnglishAbilityList()));
        specialIdentifiers.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList("Sex Offender", "Child Abuse Defender", "Child Abuse Victim", "Career Criminal", "Other")));   // DUMMY DATA
        gangName.getItems().addAll(FXCollections.observableList(fetch.getGangsList()));
        gangMembership.getItems().addAll(FXCollections.observableArrayList("Active", "Non-Active"));

        vehicleMake.getItems().addAll(FXCollections.observableList(fetch.getVehicleMakesList()));
        vehicleModel.getItems().addAll(FXCollections.observableList(fetch.getVehicleModelList()));
        vehicleColor.getItems().addAll(FXCollections.observableList(fetch.getVehicleColorList()));
        vehDisposition.getItems().addAll(FXCollections.observableArrayList("Impound", "Parked", "Other"));
        impoundCompany.getItems().addAll(FXCollections.observableList(fetch.getImpoundCompanyNamesList()));

        reportingDistrict.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList(fetch.getDepartmentsList())));   // DUMMY DATA
        arrestCharges.getItems().addAll(FXCollections.observableList(fetch.getArrestChargesList()));
        arrestingOfficerID.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList(fetch.getDepartmentsList())));   // DUMMY DATA
        transportingOfficerID.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList(fetch.getDepartmentsList())));   // DUMMY DATA
        bookingClerkDept.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList(fetch.getDepartmentsList())));   // DUMMY DATA
        searchingOffDept.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList(fetch.getDepartmentsList())));   // DUMMY DATA

        // Fill in all fields with privous info.
        lastNameInmate.setText(displayInmate.lastName);

        firstNameInmate.setText(displayInmate.firstName);
        middleNameInmate.setText(displayInmate.middleName);
        suffixInmate.setText(displayInmate.suffix);
        akaInmate.setText(displayInmate.aka);
        dobInmateMM.setText(displayInmate.dob1);
        dobInmateDD.setText(displayInmate.dob2);
        dobInmateYYYY.setText(displayInmate.dob3);
        ageInmate.setText(displayInmate.age);
        pobInmateCity.setText(displayInmate.permanentAddressCity);
        pobInmateState.setText(displayInmate.placeOfBirthState);
        pobInmateCountry.setText(displayInmate.placeOfBirthCountry);
        SSN1.setText(displayInmate.SSN1);
        SSN2.setText(displayInmate.SSN2);
        SSN3.setText(displayInmate.SSN3);
        sexInmate.setText(displayInmate.sex);
        heightInchesInmate.setText(displayInmate.height1);
        heightFeetInmate.setText(displayInmate.height2);
        weightInmate.setText(displayInmate.weight);
        permaAdressInmate.setText(displayInmate.permanentAddressStreet);
        permaCityInmate.setText(displayInmate.permanentAddressCity);
        permaStateInmate.setText(displayInmate.permanentAddressState);
        permaZipInmate.setText(displayInmate.permanentAddressZip);
        permaTN1Inmate.setText(displayInmate.permanentTelephone1);
        permaTN2Inmate.setText(displayInmate.permanentTelephone2);
        permaTN3Inmate.setText(displayInmate.permanentTelephone3);
        tempAdressInmate.setText(displayInmate.temporaryAddressStreet);
        tempCityInmate.setText(displayInmate.temporaryAddressCity);
        tempStateInmate.setText(displayInmate.temporaryAddressState);
        tempZipInmate.setText(displayInmate.temporaryAddressZip);
        tempTN1Inmate.setText(displayInmate.temporaryTelephone1);
        tempTN2Inmate.setText(displayInmate.temporaryTelephone2);
        tempTN3Inmate.setText(displayInmate.temporaryTelephone3);

        SMTList.setText(displayInmate.SMT);

        firstNameEC.setText(displayInmate.eContactFirstName);
        lastNameEC.setText(displayInmate.eContactLastName);
        middleNameEC.setText(displayInmate.eContactMiddleName);
        addressEC.setText(displayInmate.eContactAddressStreet);
        cityEC.setText(displayInmate.eContactAddressCity);
        stateEC.setText(displayInmate.eContactAddressState);
        zipcodeEC.setText(displayInmate.eContactAddressZip);
        TN1EC.setText(displayInmate.eContactTelephone1);
        TN2EC.setText(displayInmate.eContactTelephone2);
        TN3EC.setText(displayInmate.eContactTelephone3);
        occupationInmate.setText(displayInmate.occupation);
        HGCInmate.setText(displayInmate.highestGradeCompleted);
        typeOfBusiness.setText(displayInmate.employerTypeOfBusiness);
        position.setText(displayInmate.position);
        employerName.setText(displayInmate.employerName);
        addressEmployer.setText(displayInmate.employerAddressStreet);
        cityEmployer.setText(displayInmate.employerAddressCity);
        stateEmployer.setText(displayInmate.employerAddressState);
        zipcodeEmployer.setText(displayInmate.employerAddressZip);
        employerTN2.setText(displayInmate.employerTelephone1);
        employerTN1.setText(displayInmate.employerTelephone2);
        employerTN3.setText(displayInmate.employerTelephone3);
        gangNameOther.setText(displayInmate.gangAffiliationName);
        gangLocation.setText(displayInmate.gangAffiliationCity);

        driverLicense.setText(displayInmate.driversLicenseNumber);
        stateIssued.setText(displayInmate.driversLicenseState);
        vehicleLicenseNumber.setText(displayInmate.vehiclePlateNumber);
        stateValidated.setText(displayInmate.vehiclePlateState);
        yearValidated.setText(displayInmate.vehicleYear);
        impoundCompanyManually.setText(displayInmate.vehicleImpoundCompany);
        impoundComAddress.setText(displayInmate.vehicleImpoundCompanyAddressStreet);
        impoundComCity.setText(displayInmate.vehicleImpoundCompanyAddressCity);
        impoundComState.setText(displayInmate.vehicleImpoundCompanyAddressState);
        impoundComZipcode.setText(displayInmate.vehicleParkedAddressZip);
        parkedAddress.setText(displayInmate.vehicleParkedAddressStreet);
        parkedCity.setText(displayInmate.vehicleParkedAddressCity);
        parkedState.setText(displayInmate.vehicleImpoundCompanyAddressState);
        parkedZip.setText(displayInmate.vehicleImpoundCompanyAddressZip);
        vehicleOther.setText(displayInmate.vehicleOther);

        arrestLocation.setText(displayInmate.arrestLocationStreet);
        arrestCity.setText(displayInmate.arrestLocationCity);
        arrestingAgency.setText(displayInmate.arrestingAgency);
        arrestedMonth.setText(displayInmate.dateArrested1);
        arrestedDay.setText(displayInmate.dateArrested2);
        arrestedYear.setText(displayInmate.dateArrested3);
        arrestedTime.setText(displayInmate.timeArrested);
        //bookingNumber.setText(displayInmate);
        bookedMonth.setText(displayInmate.bookingDate1);
        bookedDay.setText(displayInmate.bookingDate2);
        bookedYear.setText(displayInmate.bookingDate3);
        bookedTime.setText(displayInmate.timeBooked);
        cashProperty.setText(displayInmate.cash);
        arrestBail.setText(displayInmate.arrestBail);
        nonCashProperty.setText(displayInmate.nonCash);

    }

    private Inmate createInmate() {
        Inmate inmate = new Inmate();

        if(lastNameInmate.getText() != null) {
            inmate.lastName = lastNameInmate.getText();
        }
        if(firstNameInmate.getText() != null) {
            inmate.firstName = firstNameInmate.getText();
        }
        if(middleNameInmate.getText() != null) {
            inmate.middleName = middleNameInmate.getText();
        }
        if(suffixInmate.getText() != null) {
            inmate.suffix = suffixInmate.getText();
        }
        if(akaInmate.getText() != null) {
            inmate.aka = akaInmate.getText();
        }
        if(dobInmateMM.getText() != null) {
            inmate.dob1 = dobInmateMM.getText();
        }
        if(dobInmateDD.getText() != null) {
            inmate.dob2 = dobInmateDD.getText();
        }
        if(dobInmateYYYY.getText() != null) {
            inmate.dob3 = dobInmateYYYY.getText();
        }
        if(ageInmate.getText() != null) {
            inmate.age = ageInmate.getText();
        }
        if(pobInmateCity.getText() != null) {
            inmate.placeOfBirthCity = pobInmateCity.getText();
        }
        if(pobInmateState.getText() != null) {
            inmate.placeOfBirthState = pobInmateState.getText();
        }
        if(pobInmateCountry.getText() != null) {
            inmate.placeOfBirthCountry = pobInmateCountry.getText();
        }
        if(foreignNatInmateBool.getValue() != null) {
            inmate.foreignNationalStatus = foreignNatInmateBool.getValue().toString();
        }
        if(foreignNatInmateCountry.getValue() != null) {
            inmate.foreignNationalCountry = foreignNatInmateCountry.getValue().toString();
        }
        if(SSN1.getText() != null) {
            inmate.SSN1 = SSN1.getText();
        }
        if(SSN2.getText() != null) {
            inmate.SSN2 = SSN2.getText();
        }
        if(SSN3.getText() != null) {
            inmate.SSN3 = SSN3.getText();
        }
        if(sexInmate.getText() != null) {
            inmate.sex = sexInmate.getText();
        }
        if(descentInmate.getValue() != null) {
            inmate.descent = descentInmate.getValue().toString();
        }
        if(hairColorInmate.getValue() != null) {
            inmate.hairColor = hairColorInmate.getValue().toString();
        }
        if(eyeColorInmate.getValue() != null) {
            inmate.eyeColor = eyeColorInmate.getValue().toString();
        }
        if(heightFeetInmate.getText() != null) {
            inmate.height1 = heightFeetInmate.getText();
        }
        if(heightInchesInmate.getText() != null) {
            inmate.height2 = heightInchesInmate.getText();
        }
        if(weightInmate.getText() != null) {
            inmate.weight = weightInmate.getText();
        }
        if(SMTList.getText() != null) {
            inmate.SMT = SMTList.getText();
        }
        if(permaAdressInmate.getText() != null) {
            inmate.permanentAddressStreet = permaAdressInmate.getText();
        }
        if(permaCityInmate.getText() != null) {
            inmate.permanentAddressCity = permaCityInmate.getText();
        }
        if(permaStateInmate.getText() != null) {
            inmate.permanentAddressState = permaStateInmate.getText();
        }
        if(permaZipInmate.getText() != null) {
            inmate.permanentAddressZip = permaZipInmate.getText();
        }
        if(permaTN1Inmate.getText() != null) {
            inmate.permanentTelephone1 = permaTN1Inmate.getText();
        }
        if(permaTN2Inmate.getText() != null) {
            inmate.permanentTelephone2 = permaTN2Inmate.getText();
        }
        if(permaTN3Inmate.getText() != null) {
            inmate.permanentTelephone3 = permaTN3Inmate.getText();
        }
        if(tempAdressInmate.getText() != null) {
            inmate.temporaryAddressStreet = tempAdressInmate.getText();
        }
        if(tempCityInmate.getText() != null) {
            inmate.temporaryAddressCity = tempCityInmate.getText();
        }
        if(tempStateInmate.getText() != null) {
            inmate.temporaryAddressState = tempStateInmate.getText();
        }
        if(tempZipInmate.getText() != null) {
            inmate.temporaryAddressZip = tempZipInmate.getText();
        }
        if(tempTN1Inmate.getText() != null) {
            inmate.temporaryTelephone1 = tempTN1Inmate.getText();
        }
        if(tempTN2Inmate.getText() != null) {
            inmate.temporaryTelephone2 = tempTN2Inmate.getText();
        }
        if(tempTN3Inmate.getText() != null) {
            inmate.temporaryTelephone3 = tempTN3Inmate.getText();
        }
        if(lastNameEC.getText() != null) {
            inmate.eContactLastName = lastNameEC.getText();
        }
        if(firstNameEC.getText() != null) {
            inmate.eContactFirstName = firstNameEC.getText();
        }
        if(middleNameEC.getText() != null) {
            inmate.eContactMiddleName = middleNameEC.getText();
        }
        if(relationshipEC.getValue() != null) {
            inmate.eContactRelationship = relationshipEC.getValue().toString();
        }
        if(addressEC.getText() != null) {
            inmate.eContactAddressStreet = addressEC.getText();
        }
        if(cityEC.getText() != null) {
            inmate.eContactAddressCity = cityEC.getText();
        }
        if(stateEC.getText() != null) {
            inmate.eContactAddressState = stateEC.getText();
        }
        if(zipcodeEC.getText() != null) {
            inmate.eContactAddressZip = zipcodeEC.getText();
        }
        if(TN1EC.getText() != null) {
            inmate.eContactTelephone1 = TN1EC.getText();
        }
        if(TN2EC.getText() != null) {
            inmate.eContactTelephone2 = TN2EC.getText();
        }
        if(TN3EC.getText() != null) {
            inmate.eContactTelephone3 = TN3EC.getText();
        }
        if(occupationInmate.getText() != null) {
            inmate.occupation = occupationInmate.getText();
        }
        if(skillsInmate.getValue() != null) {
            inmate.skills = skillsInmate.getValue().toString();
        }
        if(HGCInmate.getText() != null) {
            inmate.highestGradeCompleted = HGCInmate.getText();
        }
        if(levelOfEnglishInmate.getValue() != null) {
            inmate.levelOfEnglish = levelOfEnglishInmate.getValue().toString();
        }
        if(employerName.getText() != null) {
            inmate.employerName = employerName.getText();
        }
        if(typeOfBusiness.getText() != null) {
            inmate.employerTypeOfBusiness = typeOfBusiness.getText();
        }
        if(addressEmployer.getText() != null) {
            inmate.employerAddressStreet = addressEmployer.getText();
        }
        if(cityEmployer.getText() != null) {
            inmate.employerAddressCity = cityEmployer.getText();
        }
        if(stateEmployer.getText() != null) {
            inmate.employerAddressState = stateEmployer.getText();
        }
        if(zipcodeEmployer.getText() != null) {
            inmate.employerAddressZip = zipcodeEmployer.getText();
        }
        if(employerTN1.getText() != null) {
            inmate.employerTelephone1 = employerTN1.getText();
        }
        if(employerTN2.getText() != null) {
            inmate.employerTelephone2 = employerTN2.getText();
        }
        if(employerTN3.getText() != null) {
            inmate.employerTelephone3 = employerTN3.getText();
        }
        if(position.getText() != null) {
            inmate.position = position.getText();
        }
        if(specialIdentifiers.getValue() != null) {
            inmate.specialIdentifiers = specialIdentifiers.getValue().toString();
        }

        if(gangName.getValue() != null) {
            inmate.gangAffiliationName = gangName.getValue().toString();
        }
        else {
            inmate.gangAffiliationName = gangNameOther.getText();
        }

        if(gangLocation.getText() != null) {
            inmate.gangAffiliationCity = gangLocation.getText();
        }
        if(gangMembership.getValue() != null) {
            inmate.gangAffiliationStatus = gangMembership.getValue().toString();
        }
        if(driverLicense.getText() != null) {
            inmate.driversLicenseNumber = driverLicense.getText();
        }
        if(stateIssued.getText() != null) {
            inmate.driversLicenseState = stateIssued.getText();
        }
        if(vehicleLicenseNumber.getText() != null) {
            inmate.vehiclePlateNumber = vehicleLicenseNumber.getText();
        }
        if(stateValidated.getText() != null) {
            inmate.vehiclePlateState = stateValidated.getText();
        }
        if(yearValidated.getText() != null) {
            inmate.vehicleYear = yearValidated.getText();
        }
        if(vehicleMake.getValue() != null) {
            inmate.vehicleMake = vehicleMake.getValue().toString();
        }
        if(vehicleModel.getValue() != null) {
            inmate.vehicleModel = vehicleModel.getValue().toString();
        }
        if(vehicleColor.getValue() != null) {
            inmate.vehicleColor = vehicleColor.getValue().toString();
        }
        if(vehDisposition.getValue() != null) {
            inmate.vehicleDisposition = vehDisposition.getValue().toString();
        }

        if(impoundCompany.getValue() != null) {
            inmate.vehicleImpoundCompany = impoundCompany.getValue().toString();
        }
        else {
            inmate.vehicleImpoundCompany = impoundCompanyManually.getText();
        }
        if(impoundComAddress.getText() != null) {
            inmate.vehicleImpoundCompanyAddressStreet = impoundComAddress.getText();
        }
        if(impoundComCity.getText() != null) {

            inmate.vehicleImpoundCompanyAddressCity = impoundComCity.getText();
        }
        if(impoundComState.getText() != null) {
            inmate.vehicleImpoundCompanyAddressState = impoundComState.getText();
        }
        if(impoundComZipcode.getText() != null) {
            inmate.vehicleImpoundCompanyAddressZip = impoundComZipcode.getText();
        }
        if(parkedAddress.getText() != null) {
            inmate.vehicleParkedAddressStreet = parkedAddress.getText();
        }
        if(parkedCity.getText() != null) {
            inmate.vehicleParkedAddressCity = parkedCity.getText();
        }
        if(parkedState.getText() != null) {
            inmate.vehicleParkedAddressState = parkedState.getText();
        }
        if(parkedZip.getText() != null) {
            inmate.vehicleParkedAddressZip = parkedZip.getText();
        }
        if(vehicleOther.getText() != null) {
            inmate.vehicleOther = vehicleOther.getText();
        }
        if(arrestLocation.getText() != null) {
            inmate.arrestLocationStreet = arrestLocation.getText();
        }
        if(arrestCity.getText() != null) {
            inmate.arrestLocationCity = arrestCity.getText();
        }
        if(reportingDistrict.getValue() != null) {
            inmate.reportingDistrict = reportingDistrict.getValue().toString();
        }
        if(arrestingAgency.getText() != null) {
            inmate.arrestingAgency = arrestingAgency.getText();
        }
        if(arrestedMonth.getText() != null) {
            inmate.dateArrested1 = arrestedMonth.getText();
        }
        if(arrestedDay.getText() != null) {
            inmate.dateArrested2 = arrestedDay.getText();
        }
        if(arrestedYear.getText() != null) {
            inmate.dateArrested3 = arrestedYear.getText();
        }
        if(arrestedTime.getText() != null) {
            inmate.timeArrested = arrestedTime.getText();
        }
        if(arrestCharges.getValue() != null) {
            inmate.arrestCharges = arrestCharges.getValue().toString();
        }
        if(arrestingOfficerID.getValue() != null) {
            inmate.arrestingOfficerDepartment = arrestingOfficerID.getValue().toString();
        }
        if(transportingOfficerID.getValue() != null) {
            inmate.transportingOfficerDepartment = transportingOfficerID.getValue().toString();
        }

        inmate.bookingDate1 = bookedMonth.getText();
        inmate.bookingDate2 = bookedDay.getText();
        inmate.bookingDate3 = bookedYear.getText();
        inmate.timeBooked = bookedTime.getText();

        if(bookingClerkDept.getValue() != null) {
            inmate.bookingClerkDepartment = bookingClerkDept.getValue().toString();
        }
        if(searchingOffDept.getValue() != null) {
            inmate.searchingOfficerDepartment = searchingOffDept.getValue().toString();
        }
        inmate.cash = cashProperty.getText();
        inmate.nonCash = nonCashProperty.getText();
        inmate.arrestBail = arrestBail.getText();

        return inmate;
    }

    // Helpers
    private void tabToGeneral() {
        tabPane.getSelectionModel().select(0);
    }

    private void tabToPersonal() {
        tabPane.getSelectionModel().select(1);
    }

    private void tabToVehicle() {
        tabPane.getSelectionModel().select(2);
    }

    private void tabToDept() {
        tabPane.getSelectionModel().select(3);
    }

    private String makeID(Inmate inmate) {
        String temp;
        String stringToHash = inmate.firstName + inmate.lastName + inmate.hairColor + inmate.eyeColor;
        String inmateSSN = inmate.SSN1 + inmate.SSN2 + inmate.SSN3;

        if(inmateSSN.isEmpty()) {
            temp = PasswordEncrypt.cryptPassword(stringToHash).substring(0, 10);
        }
        else {
            temp = inmateSSN;
        }
        return temp;
    }

}
