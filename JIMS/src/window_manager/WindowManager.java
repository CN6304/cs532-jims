package window_manager;

import inmate.Inmate;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 * Manages all the GUI windows. Displays and closes.
 */
public class WindowManager {

    private static final Image appIcon
            = new Image(WindowManager.class.getResourceAsStream("/assets/jims_icon.png")); // Default program icon.

    public static Stage loginStage;
    public static Stage mainStage;
    private static Stage dailyLogStage;
    private static Stage bookingStage;
    private static Stage displayStage;
    private static Stage adminStage;

    public static Inmate displayInmate;

    /**
     * Opens the main JIMS window when called.
     *
     * @throws java.io.IOException
     */
    public static void openMainWindow() throws IOException {
        mainStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/main/GUI-Main.fxml")); // Get the GUI interface.

        mainStage.getIcons().add(appIcon); // Set window icon.
        mainStage.setTitle("Jail Information Management System"); // Set title.
        mainStage.setScene(new Scene(root, 1190, 790));
        mainStage.setResizable(false);
        mainStage.show();
    }

    public static void openAdminWindow() throws IOException {
        adminStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/admin/GUI-Admin.fxml")); // Get the GUI interface.

        adminStage.getIcons().add(new Image(WindowManager.class.getResourceAsStream("/assets/admin_icon.png")));
        adminStage.setTitle("Admin Tools"); // Set title.
        adminStage.setScene(new Scene(root, 890, 590));
        adminStage.setResizable(false); // Window cannot be resized.
        adminStage.show();
    }

    /**
     * Opens the login window when called.
     *
     * @throws java.io.IOException
     */
    public static void openLoginWindow() throws IOException {
        loginStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/login/GUI-Login.fxml"));
        loginStage.getIcons().add(new Image(WindowManager.class.getResourceAsStream("/assets/jims_icon.png")));
        loginStage.setScene(new Scene(root, 590, 490));
        loginStage.setTitle("Login");
        loginStage.setResizable(false); // Window cannot be resized.
        loginStage.initModality(Modality.APPLICATION_MODAL); // Locks all other actions until the login window is closed.
        //loginStage.initStyle(StageStyle.UTILITY);
        loginStage.initOwner(mainStage); // Bound the login window with the main window.
        //loginStage.initStyle(StageStyle.UNDECORATED);
        loginStage.show();
    }

    /**
     * Opens when the booking window is called.
     *
     * @throws java.io.IOException
     */
    public static void openBookingWindow() throws IOException {
        bookingStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/booking/GUI-Booking.fxml"));

        bookingStage.getIcons().add(new Image(WindowManager.class.getResourceAsStream("/assets/book_icon.png")));
        bookingStage.setTitle("Inmate Booking");
        bookingStage.setScene(new Scene(root, 990, 690));
        bookingStage.setResizable(false); // User cannot resize this window.

        //bookingStage.initOwner(mainStage); // Bound the login window with the main window.
        bookingStage.show();
    }

    /**
     * Opens when the searcher window is called.
     *
     * @throws java.io.IOException
     */
    public static void openDailyLogWindow() throws IOException {
        dailyLogStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/daily_log/GUI-DailyLog.fxml"));

        dailyLogStage.getIcons().add(new Image(WindowManager.class.getResourceAsStream("/assets/inventory_icon.png")));
        dailyLogStage.setTitle("Inmate Daily Log");
        dailyLogStage.setScene(new Scene(root, 1090, 690));
        dailyLogStage.setResizable(false); // User cannot resize this window.

        //searcherStage.initOwner(mainStage); // Bound the login window with the main window.
        dailyLogStage.show();
    }

    public static void openDisplayWindow(Inmate inmate) throws IOException {
        displayInmate = inmate;
        displayStage = new Stage();
        Parent root = FXMLLoader.load(WindowManager.class.getResource("/report/GUI-Display.fxml"));

        displayStage.getIcons().add(new Image(WindowManager.class.getResourceAsStream("/assets/book_icon.png")));
        displayStage.setTitle("Inmate Display");
        displayStage.setScene(new Scene(root, 990, 690));
        displayStage.setResizable(false); // User cannot resize this window.

        //bookingStage.initOwner(mainStage); // Bound the login window with the main window.
        displayStage.show();
    }

    public static void closeMainWindow() {
        if(mainStage.isShowing()) {
            mainStage.close();
        }
    }             // Close the main window.

    public static void closeLoginWindow() {
        if(loginStage.isShowing()) {
            loginStage.close();
        }
    }           // Close the login window.

    public static void closeBookingWindow() {
        if(bookingStage.isShowing()) {
            bookingStage.close();
        }
    }       // Close the booking window.

    public static void closeDailyLogWindow() {
        if(dailyLogStage.isShowing()) {
            dailyLogStage.close();
        }
    }

    public static void closeDisplayWindow() {
        if(displayStage.isShowing()) {
            displayStage.close();
        }
    }

    public static void closeAdminWindow() {
        if(adminStage.isShowing()) {
            adminStage.close();
        }
    }

    public static boolean isOpenBookingWindow() {

        return bookingStage.isShowing();
    }
}
