/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmate;

/**
 *
 * @author cello
 */
public class Inmate {

    public String lastName;
    public String firstName;
    public String middleName;
    public String suffix;
    public String aka;
    public String dob1;
    public String dob2;
    public String dob3;
    public String age;
    public String placeOfBirthCity;
    public String placeOfBirthState;
    public String placeOfBirthCountry;
    public String foreignNationalStatus;
    public String foreignNationalCountry;
    public String SSN1;
    public String SSN2;
    public String SSN3;
    public String sex;
    public String descent;
    public String hairColor;
    public String eyeColor;
    public String height1;
    public String height2;
    public String weight;
    public String SMT;
    public String permanentAddressStreet;
    public String permanentAddressCity;
    public String permanentAddressState;
    public String permanentAddressZip;
    public String permanentTelephone1;
    public String permanentTelephone2;
    public String permanentTelephone3;
    public String temporaryAddressStreet;
    public String temporaryAddressCity;
    public String temporaryAddressState;
    public String temporaryAddressZip;
    public String temporaryTelephone1;
    public String temporaryTelephone2;
    public String temporaryTelephone3;
    public String eContactLastName;
    public String eContactFirstName;
    public String eContactMiddleName;
    public String eContactRelationship;
    public String eContactAddressStreet;
    public String eContactAddressCity;
    public String eContactAddressState;
    public String eContactAddressZip;
    public String eContactTelephone1;
    public String eContactTelephone2;
    public String eContactTelephone3;
    public String occupation;
    public String skills;
    public String highestGradeCompleted;
    public String levelOfEnglish;
    public String employerName;
    public String employerTypeOfBusiness;
    public String employerAddressStreet;
    public String employerAddressCity;
    public String employerAddressState;
    public String employerAddressZip;
    public String employerTelephone1;
    public String employerTelephone2;
    public String employerTelephone3;
    public String position;
    public String specialIdentifiers;
    public String gangAffiliationName;
    public String gangAffiliationCity;
    public String gangAffiliationStatus;
    public String driversLicenseNumber;
    public String driversLicenseState;
    public String vehiclePlateNumber;
    public String vehiclePlateState;
    public String vehicleYear;
    public String vehicleMake;
    public String vehicleModel;
    public String vehicleColor;
    public String vehicleDisposition;
    public String vehicleImpoundCompany;
    public String vehicleImpoundCompanyAddressStreet;
    public String vehicleImpoundCompanyAddressCity;
    public String vehicleImpoundCompanyAddressState;
    public String vehicleImpoundCompanyAddressZip;
    public String vehicleParkedAddressStreet;
    public String vehicleParkedAddressCity;
    public String vehicleParkedAddressState;
    public String vehicleParkedAddressZip;
    public String vehicleOther;
    public String arrestLocationStreet;
    public String arrestLocationCity;
    public String reportingDistrict;
    public String arrestingAgency;
    public String dateArrested1;
    public String dateArrested2;
    public String dateArrested3;
    public String timeArrested;
    public String arrestCharges;
    public String arrestingOfficerDepartment;
    public String transportingOfficerDepartment;
    public String bookingDate1;
    public String bookingDate2;
    public String bookingDate3;
    public String timeBooked;
    public String bookingClerkDepartment;
    public String searchingOfficerDepartment;
    public String cash;
    public String nonCash;
    public String arrestBail;

    public Inmate() {
        this.lastName = "";
        this.firstName = "";
        this.middleName = "";
        this.suffix = "";
        this.aka = "";
        this.dob1 = "";
        this.dob2 = "";
        this.dob3 = "";
        this.age = "";
        this.placeOfBirthCity = "";
        this.placeOfBirthState = "";
        this.placeOfBirthCountry = "";
        this.foreignNationalStatus = "";
        this.foreignNationalCountry = "";
        this.SSN1 = "";
        this.SSN2 = "";
        this.SSN3 = "";
        this.sex = "";
        this.descent = "";
        this.hairColor = "";
        this.eyeColor = "";
        this.height1 = "";
        this.height2 = "";
        this.weight = "";
        this.SMT = "";
        this.permanentAddressStreet = "";
        this.permanentAddressCity = "";
        this.permanentAddressState = "";
        this.permanentAddressZip = "";
        this.permanentTelephone1 = "";
        this.permanentTelephone2 = "";
        this.permanentTelephone3 = "";
        this.temporaryAddressStreet = "";
        this.temporaryAddressCity = "";
        this.temporaryAddressState = "";
        this.temporaryAddressZip = "";
        this.temporaryTelephone1 = "";
        this.temporaryTelephone2 = "";
        this.temporaryTelephone3 = "";
        this.eContactLastName = "";
        this.eContactFirstName = "";
        this.eContactMiddleName = "";
        this.eContactRelationship = "";
        this.eContactAddressStreet = "";
        this.eContactAddressCity = "";
        this.eContactAddressState = "";
        this.eContactAddressZip = "";
        this.eContactTelephone1 = "";
        this.eContactTelephone2 = "";
        this.eContactTelephone3 = "";
        this.occupation = "";
        this.skills = "";
        this.highestGradeCompleted = "";
        this.levelOfEnglish = "";
        this.employerName = "";
        this.employerTypeOfBusiness = "";
        this.employerAddressStreet = "";
        this.employerAddressCity = "";
        this.employerAddressState = "";
        this.employerAddressZip = "";
        this.employerTelephone1 = "";
        this.employerTelephone2 = "";
        this.employerTelephone3 = "";
        this.position = "";
        this.specialIdentifiers = "";
        this.gangAffiliationName = "";
        this.gangAffiliationCity = "";
        this.gangAffiliationStatus = "";
        this.driversLicenseNumber = "";
        this.driversLicenseState = "";
        this.vehiclePlateNumber = "";
        this.vehiclePlateState = "";
        this.vehicleYear = "";
        this.vehicleMake = "";
        this.vehicleModel = "";
        this.vehicleColor = "";
        this.vehicleDisposition = "";
        this.vehicleImpoundCompany = "";
        this.vehicleImpoundCompanyAddressStreet = "";
        this.vehicleImpoundCompanyAddressCity = "";
        this.vehicleImpoundCompanyAddressState = "";
        this.vehicleImpoundCompanyAddressZip = "";
        this.vehicleParkedAddressStreet = "";
        this.vehicleParkedAddressCity = "";
        this.vehicleParkedAddressState = "";
        this.vehicleParkedAddressZip = "";
        this.vehicleOther = "";
        this.arrestLocationStreet = "";
        this.arrestLocationCity = "";
        this.reportingDistrict = "";
        this.arrestingAgency = "";
        this.dateArrested1 = "";
        this.dateArrested2 = "";
        this.dateArrested3 = "";
        this.timeArrested = "";
        this.arrestCharges = "";
        this.arrestingOfficerDepartment = "";
        this.transportingOfficerDepartment = "";
        this.bookingDate1 = "";
        this.bookingDate2 = "";
        this.bookingDate3 = "";
        this.timeBooked = "";
        this.bookingClerkDepartment = "";
        this.searchingOfficerDepartment = "";
        this.cash = "";
        this.nonCash = "";
        this.arrestBail = "";
    }

    public void printAll() {
        System.out.println(lastName);
        System.out.println(firstName);
        System.out.println(middleName);
        System.out.println(suffix);
        System.out.println(aka);
        System.out.println(dob1);
        System.out.println(dob2);
        System.out.println(dob3);
        System.out.println(age);
        System.out.println(placeOfBirthCity);
        System.out.println(placeOfBirthState);
        System.out.println(placeOfBirthCountry);
        System.out.println(foreignNationalStatus);
        System.out.println(foreignNationalCountry);
        System.out.println(SSN1);
        System.out.println(SSN2);
        System.out.println(SSN3);
        System.out.println(sex);
        System.out.println(descent);
        System.out.println(hairColor);
        System.out.println(eyeColor);
        System.out.println(height1);
        System.out.println(height2);
        System.out.println(weight);
        System.out.println(SMT);
        System.out.println(permanentAddressStreet);
        System.out.println(permanentAddressCity);
        System.out.println(permanentAddressState);
        System.out.println(permanentAddressZip);
        System.out.println(permanentTelephone1);
        System.out.println(permanentTelephone2);
        System.out.println(permanentTelephone3);
        System.out.println(temporaryAddressStreet);
        System.out.println(temporaryAddressCity);
        System.out.println(temporaryAddressState);
        System.out.println(temporaryAddressZip);
        System.out.println(temporaryTelephone1);
        System.out.println(temporaryTelephone2);
        System.out.println(temporaryTelephone3);
        System.out.println(eContactLastName);
        System.out.println(eContactFirstName);
        System.out.println(eContactMiddleName);
        System.out.println(eContactRelationship);
        System.out.println(eContactAddressStreet);
        System.out.println(eContactAddressCity);
        System.out.println(eContactAddressState);
        System.out.println(eContactAddressZip);
        System.out.println(eContactTelephone1);
        System.out.println(eContactTelephone2);
        System.out.println(eContactTelephone3);
        System.out.println(occupation);
        System.out.println(skills);
        System.out.println(highestGradeCompleted);
        System.out.println(levelOfEnglish);
        System.out.println(employerName);
        System.out.println(employerTypeOfBusiness);
        System.out.println(employerAddressStreet);
        System.out.println(employerAddressCity);
        System.out.println(employerAddressState);
        System.out.println(employerAddressZip);
        System.out.println(employerTelephone1);
        System.out.println(employerTelephone2);
        System.out.println(employerTelephone3);
        System.out.println(position);
        System.out.println(specialIdentifiers);
        System.out.println(gangAffiliationName);
        System.out.println(gangAffiliationCity);
        System.out.println(gangAffiliationStatus);
        System.out.println(driversLicenseNumber);
        System.out.println(driversLicenseState);
        System.out.println(vehiclePlateNumber);
        System.out.println(vehiclePlateState);
        System.out.println(vehicleYear);
        System.out.println(vehicleMake);
        System.out.println(vehicleModel);
        System.out.println(vehicleColor);
        System.out.println(vehicleDisposition);
        System.out.println(vehicleImpoundCompany);
        System.out.println(vehicleImpoundCompanyAddressStreet);
        System.out.println(vehicleImpoundCompanyAddressCity);
        System.out.println(vehicleImpoundCompanyAddressState);
        System.out.println(vehicleImpoundCompanyAddressZip);
        System.out.println(vehicleParkedAddressStreet);
        System.out.println(vehicleParkedAddressCity);
        System.out.println(vehicleParkedAddressState);
        System.out.println(vehicleParkedAddressZip);
        System.out.println(vehicleOther);
        System.out.println(arrestLocationStreet);
        System.out.println(arrestLocationCity);
        System.out.println(reportingDistrict);
        System.out.println(arrestingAgency);
        System.out.println(dateArrested1);
        System.out.println(dateArrested2);
        System.out.println(dateArrested3);
        System.out.println(timeArrested);
        System.out.println(arrestCharges);
        System.out.println(arrestingOfficerDepartment);
        System.out.println(transportingOfficerDepartment);
        System.out.println(bookingDate1);
        System.out.println(bookingDate2);
        System.out.println(bookingDate3);
        System.out.println(timeBooked);
        System.out.println(bookingClerkDepartment);
        System.out.println(searchingOfficerDepartment);
        System.out.println(cash);
        System.out.println(nonCash);
        System.out.println(arrestBail);
    }

    public Inmate(InmateFromDB inmate) {
        this.lastName = inmate.lastName;
        this.firstName = inmate.firstName;
        this.middleName = inmate.middleName;
        this.suffix = inmate.suffix;
        this.aka = inmate.aka;
        this.dob1 = "";
        this.dob2 = "";
        this.dob3 = "";
        String[] dob = parseDate(inmate.dob);
        if(dob != null) {
            this.dob1 = dob[0];
            this.dob2 = dob[1];
            this.dob3 = dob[2];
        }
        this.age = inmate.age;
        this.placeOfBirthCity = inmate.placeOfBirthCity;
        this.placeOfBirthState = inmate.placeOfBirthState;
        this.placeOfBirthCountry = inmate.placeOfBirthCountry;
        this.foreignNationalStatus = inmate.foreignNationalStatus;
        this.foreignNationalCountry = inmate.foreignNationalCountry;
        this.SSN1 = "";
        this.SSN2 = "";
        this.SSN3 = "";
        String[] ssn = parseSSN(inmate.SSN);
        if(ssn != null) {
            this.SSN1 = ssn[0];
            this.SSN2 = ssn[1];
            this.SSN3 = ssn[2];
        }
        this.sex = inmate.sex;
        this.descent = inmate.descent;
        this.hairColor = inmate.hairColor;
        this.eyeColor = inmate.eyeColor;
        this.height1 = "";
        this.height2 = "";
        String[] height = parseHeight(inmate.height);
        if(height != null) {
            this.height1 = height[0];
            this.height2 = height[1];
        }
        this.weight = inmate.weight;
        this.SMT = inmate.SMT;
        this.permanentAddressStreet = inmate.permanentAddressStreet;
        this.permanentAddressCity = inmate.permanentAddressCity;
        this.permanentAddressState = inmate.permanentAddressState;
        this.permanentAddressZip = inmate.permanentAddressZip;
        this.permanentTelephone1 = "";
        this.permanentTelephone2 = "";
        this.permanentTelephone3 = "";
        String[] permPhone = parsePhone(inmate.permanentTelephone);
        if(permPhone != null) {
            this.permanentTelephone1 = permPhone[0];
            this.permanentTelephone2 = permPhone[1];
            this.permanentTelephone3 = permPhone[2];
        }
        this.temporaryAddressStreet = inmate.temporaryAddressStreet;
        this.temporaryAddressCity = inmate.temporaryAddressCity;
        this.temporaryAddressState = inmate.temporaryAddressState;
        this.temporaryAddressZip = inmate.temporaryAddressZip;
        this.temporaryTelephone1 = "";
        this.temporaryTelephone2 = "";
        this.temporaryTelephone3 = "";
        String[] tempPhone = parsePhone(inmate.temporaryTelephone);
        if(tempPhone != null) {
            this.temporaryTelephone1 = tempPhone[0];
            this.temporaryTelephone2 = tempPhone[1];
            this.temporaryTelephone3 = tempPhone[2];
        }
        this.eContactLastName = inmate.eContactLastName;
        this.eContactFirstName = inmate.eContactFirstName;
        this.eContactMiddleName = inmate.eContactMiddleName;
        this.eContactRelationship = inmate.eContactRelationship;
        this.eContactAddressStreet = inmate.eContactAddressStreet;
        this.eContactAddressCity = inmate.eContactAddressCity;
        this.eContactAddressState = inmate.eContactAddressState;
        this.eContactAddressZip = inmate.eContactAddressZip;
        this.eContactTelephone1 = "";
        this.eContactTelephone2 = "";
        this.eContactTelephone3 = "";
        String[] contactPhone = parsePhone(inmate.eContactTelephone);
        if(contactPhone != null) {
            this.eContactTelephone1 = contactPhone[0];
            this.eContactTelephone2 = contactPhone[1];
            this.eContactTelephone3 = contactPhone[2];
        }
        this.occupation = inmate.occupation;
        this.skills = inmate.skills;
        this.highestGradeCompleted = inmate.highestGradeCompleted;
        this.levelOfEnglish = inmate.levelOfEnglish;
        this.employerName = inmate.employerName;
        this.employerTypeOfBusiness = inmate.employerTypeOfBusiness;
        this.employerAddressStreet = inmate.employerAddressStreet;
        this.employerAddressCity = inmate.employerAddressCity;
        this.employerAddressState = inmate.employerAddressState;
        this.employerAddressZip = inmate.employerAddressZip;
        this.employerTelephone1 = "";
        this.employerTelephone2 = "";
        this.employerTelephone3 = "";
        String[] employerPhone = parsePhone(inmate.employerTelephone);
        if(employerPhone != null) {
            this.employerTelephone1 = employerPhone[0];
            this.employerTelephone2 = employerPhone[1];
            this.employerTelephone3 = employerPhone[2];
        }
        this.position = inmate.position;
        this.specialIdentifiers = inmate.specialIdentifiers;
        this.gangAffiliationName = inmate.gangAffiliationName;
        this.gangAffiliationCity = inmate.gangAffiliationCity;
        this.gangAffiliationStatus = inmate.gangAffiliationStatus;
        this.driversLicenseNumber = inmate.driversLicenseNumber;
        this.driversLicenseState = inmate.driversLicenseState;
        this.vehiclePlateNumber = inmate.vehiclePlateNumber;
        this.vehiclePlateState = inmate.vehiclePlateState;
        this.vehicleYear = inmate.vehicleYear;
        this.vehicleMake = inmate.vehicleMake;
        this.vehicleModel = inmate.vehicleModel;
        this.vehicleColor = inmate.vehicleColor;
        this.vehicleDisposition = inmate.vehicleDisposition;
        this.vehicleImpoundCompany = inmate.vehicleImpoundCompany;
        this.vehicleImpoundCompanyAddressStreet = inmate.vehicleImpoundCompanyAddressStreet;
        this.vehicleImpoundCompanyAddressCity = inmate.vehicleImpoundCompanyAddressCity;
        this.vehicleImpoundCompanyAddressState = inmate.vehicleImpoundCompanyAddressState;
        this.vehicleImpoundCompanyAddressZip = inmate.vehicleImpoundCompanyAddressZip;
        this.vehicleParkedAddressStreet = inmate.vehicleParkedAddressStreet;
        this.vehicleParkedAddressCity = inmate.vehicleParkedAddressCity;
        this.vehicleParkedAddressState = inmate.vehicleParkedAddressState;
        this.vehicleParkedAddressZip = inmate.vehicleParkedAddressZip;
        this.vehicleOther = inmate.vehicleOther;
        this.arrestLocationStreet = inmate.arrestLocationStreet;
        this.arrestLocationCity = inmate.arrestLocationCity;
        this.reportingDistrict = inmate.reportingDistrict;
        this.arrestingAgency = inmate.arrestingAgency;
        this.dateArrested1 = "";
        this.dateArrested2 = "";
        this.dateArrested3 = "";
        String[] arrest = parseDate(inmate.dateArrested);
        if(arrest != null) {
            this.dateArrested1 = arrest[0];
            this.dateArrested2 = arrest[1];
            this.dateArrested3 = arrest[2];
        }
        this.timeArrested = inmate.timeArrested;
        this.arrestCharges = inmate.arrestCharges;
        this.arrestingOfficerDepartment = inmate.arrestingOfficerDepartment;
        this.transportingOfficerDepartment = inmate.transportingOfficerDepartment;
        this.bookingDate1 = "";
        this.bookingDate2 = "";
        this.bookingDate3 = "";
        String[] booking = parseDate(inmate.bookingDate);
        if(booking != null) {
            this.bookingDate1 = booking[0];
            this.bookingDate2 = booking[1];
            this.bookingDate3 = booking[2];
        }
        this.timeBooked = inmate.timeBooked;
        this.bookingClerkDepartment = inmate.bookingClerkDepartment;
        this.searchingOfficerDepartment = inmate.searchingOfficerDepartment;
        this.cash = inmate.cash;
        this.nonCash = inmate.nonCash;
        this.arrestBail = inmate.arrestBail;
    }

    public static String[] parseDate(String date) {
        if(date.isEmpty()){
            System.out.println("parseDate: Date is empty");
            return null;
        }
        if(!date.contains("/") || (date.length() < 3)) {
            System.out.println("ERROR in parseDate: Invalid date representation");
            return null;
        }
        String[] array = date.split("/");
        return array;
    }

    public static String[] parsePhone(String number) {
        String[] array = {"", "", ""};
        if(number.isEmpty()){
            System.out.println("parsePhone: Number is empty");
            return null;
        }
        if(!number.contains("(") || !number.contains(")") || !number.contains("-")) {
            System.out.println("ERROR in parsePhone: phone number doesnt contain the required tokens");
            return null;
        }
        int firstIndex = number.indexOf("(");
        int secondIndex = number.indexOf(")");
        int thirdIndex = number.indexOf("-");

        array[0] = number.substring(firstIndex + 1, secondIndex);
        array[1] = number.substring(secondIndex + 1, thirdIndex);
        array[2] = number.substring(thirdIndex + 1);

        return array;
    }

    public static String[] parseHeight(String height) {
        String[] array = {"", ""};
        if(height.isEmpty()){
            System.out.println("parseHeight: Height is empty");
            return null;
        }
        if(!height.contains("'")) {
            System.out.println("ERROR in parseHeight: height doesnt contain the required tokens");
            return null;
        }

        int firstIndex = height.indexOf("'");
        int secondIndex = height.indexOf("'", firstIndex + 1);
        array[0] = height.substring(0, firstIndex);
        array[1] = height.substring(firstIndex + 1, secondIndex);

        return array;
    }

    public static String[] parseSSN(String ssn) {
        String[] array = {"", "", ""};
        if(ssn.isEmpty()){
            System.out.println("parseSSN: SSN is empty");
            return null;
        }
        if(ssn.length() != 9) {
            System.out.println("ERROR in parseSSN: the following ssn is in the wrong format: " + ssn);
            return null;
        }

        array[0] = ssn.substring(0, 3);
        array[1] = ssn.substring(3, 5);
        array[2] = ssn.substring(5);

        return array;
    }
}
