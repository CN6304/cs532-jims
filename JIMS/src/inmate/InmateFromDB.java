/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inmate;

/**
 *
 * @author cello
 */
public class InmateFromDB {

    public String ID;
    public String lastName;
    public String firstName;
    public String middleName;
    public String suffix;
    public String aka;
    public String dob;
    public String age;
    public String placeOfBirthCity;
    public String placeOfBirthState;
    public String placeOfBirthCountry;
    public String foreignNationalStatus;
    public String foreignNationalCountry;
    public String SSN;
    public String sex;
    public String descent;
    public String hairColor;
    public String eyeColor;
    public String height;
    public String weight;
    public String SMT;
    public String permanentAddressStreet;
    public String permanentAddressCity;
    public String permanentAddressState;
    public String permanentAddressZip;
    public String permanentTelephone;
    public String temporaryAddressStreet;
    public String temporaryAddressCity;
    public String temporaryAddressState;
    public String temporaryAddressZip;
    public String temporaryTelephone;
    public String eContactLastName;
    public String eContactFirstName;
    public String eContactMiddleName;
    public String eContactRelationship;
    public String eContactAddressStreet;
    public String eContactAddressCity;
    public String eContactAddressState;
    public String eContactAddressZip;
    public String eContactTelephone;
    public String occupation;
    public String skills;
    public String highestGradeCompleted;
    public String levelOfEnglish;
    public String employerName;
    public String employerTypeOfBusiness;
    public String employerAddressStreet;
    public String employerAddressCity;
    public String employerAddressState;
    public String employerAddressZip;
    public String employerTelephone;
    public String position;
    public String specialIdentifiers;
    public String gangAffiliationName;
    public String gangAffiliationCity;
    public String gangAffiliationStatus;
    public String driversLicenseNumber;
    public String driversLicenseState;
    public String vehiclePlateNumber;
    public String vehiclePlateState;
    public String vehicleYear;
    public String vehicleMake;
    public String vehicleModel;
    public String vehicleColor;
    public String vehicleDisposition;
    public String vehicleImpoundCompany;
    public String vehicleImpoundCompanyAddressStreet;
    public String vehicleImpoundCompanyAddressCity;
    public String vehicleImpoundCompanyAddressState;
    public String vehicleImpoundCompanyAddressZip;
    public String vehicleParkedAddressStreet;
    public String vehicleParkedAddressCity;
    public String vehicleParkedAddressState;
    public String vehicleParkedAddressZip;
    public String vehicleOther;
    public String arrestLocationStreet;
    public String arrestLocationCity;
    public String reportingDistrict;
    public String arrestingAgency;
    public String dateArrested;
    public String timeArrested;
    public String arrestCharges;
    public String arrestingOfficerDepartment;
    public String transportingOfficerDepartment;
    public String bookingDate;
    public String timeBooked;
    public String bookingClerkDepartment;
    public String searchingOfficerDepartment;
    public String cash;
    public String nonCash;
    public String arrestBail;

    public InmateFromDB() {
        this.ID = "";
        this.lastName = "";
        this.firstName = "";
        this.middleName = "";
        this.suffix = "";
        this.aka = "";
        this.dob = "";
        this.age = "";
        this.placeOfBirthCity = "";
        this.placeOfBirthState = "";
        this.placeOfBirthCountry = "";
        this.foreignNationalStatus = "";
        this.foreignNationalCountry = "";
        this.SSN = "";
        this.sex = "";
        this.descent = "";
        this.hairColor = "";
        this.eyeColor = "";
        this.height = "";
        this.weight = "";
        this.SMT = "";
        this.permanentAddressStreet = "";
        this.permanentAddressCity = "";
        this.permanentAddressState = "";
        this.permanentAddressZip = "";
        this.permanentTelephone = "";
        this.temporaryAddressStreet = "";
        this.temporaryAddressCity = "";
        this.temporaryAddressState = "";
        this.temporaryAddressZip = "";
        this.temporaryTelephone = "";
        this.eContactLastName = "";
        this.eContactFirstName = "";
        this.eContactMiddleName = "";
        this.eContactRelationship = "";
        this.eContactAddressStreet = "";
        this.eContactAddressCity = "";
        this.eContactAddressState = "";
        this.eContactAddressZip = "";
        this.eContactTelephone = "";
        this.occupation = "";
        this.skills = "";
        this.highestGradeCompleted = "";
        this.levelOfEnglish = "";
        this.employerName = "";
        this.employerTypeOfBusiness = "";
        this.employerAddressStreet = "";
        this.employerAddressCity = "";
        this.employerAddressState = "";
        this.employerAddressZip = "";
        this.employerTelephone = "";
        this.position = "";
        this.specialIdentifiers = "";
        this.gangAffiliationName = "";
        this.gangAffiliationCity = "";
        this.gangAffiliationStatus = "";
        this.driversLicenseNumber = "";
        this.driversLicenseState = "";
        this.vehiclePlateNumber = "";
        this.vehiclePlateState = "";
        this.vehicleYear = "";
        this.vehicleMake = "";
        this.vehicleModel = "";
        this.vehicleColor = "";
        this.vehicleDisposition = "";
        this.vehicleImpoundCompany = "";
        this.vehicleImpoundCompanyAddressStreet = "";
        this.vehicleImpoundCompanyAddressCity = "";
        this.vehicleImpoundCompanyAddressState = "";
        this.vehicleImpoundCompanyAddressZip = "";
        this.vehicleParkedAddressStreet = "";
        this.vehicleParkedAddressCity = "";
        this.vehicleParkedAddressState = "";
        this.vehicleParkedAddressZip = "";
        this.vehicleOther = "";
        this.arrestLocationStreet = "";
        this.arrestLocationCity = "";
        this.reportingDistrict = "";
        this.arrestingAgency = "";
        this.dateArrested = "";
        this.timeArrested = "";
        this.arrestCharges = "";
        this.arrestingOfficerDepartment = "";
        this.transportingOfficerDepartment = "";
        this.bookingDate = "";
        this.timeBooked = "";
        this.bookingClerkDepartment = "";
        this.searchingOfficerDepartment = "";
        this.cash = "";
        this.nonCash = "";
        this.arrestBail = "";
    }

    public InmateFromDB(InmateFromDB inmate) {
        this.ID = inmate.ID;
        this.lastName = inmate.lastName;
        this.firstName = inmate.firstName;
        this.middleName = inmate.middleName;
        this.suffix = inmate.suffix;
        this.aka = inmate.aka;
        this.dob = inmate.dob;
        this.age = inmate.age;
        this.placeOfBirthCity = inmate.placeOfBirthCity;
        this.placeOfBirthState = inmate.placeOfBirthState;
        this.placeOfBirthCountry = inmate.placeOfBirthCountry;
        this.foreignNationalStatus = inmate.foreignNationalStatus;
        this.foreignNationalCountry = inmate.foreignNationalCountry;
        this.SSN = inmate.SSN;
        this.sex = inmate.sex;
        this.descent = inmate.descent;
        this.hairColor = inmate.hairColor;
        this.eyeColor = inmate.eyeColor;
        this.height = inmate.height;
        this.weight = inmate.weight;
        this.SMT = inmate.SMT;
        this.permanentAddressStreet = inmate.permanentAddressStreet;
        this.permanentAddressCity = inmate.permanentAddressCity;
        this.permanentAddressState = inmate.permanentAddressState;
        this.permanentAddressZip = inmate.permanentAddressZip;
        this.permanentTelephone = inmate.permanentTelephone;
        this.temporaryAddressStreet = inmate.temporaryAddressStreet;
        this.temporaryAddressCity = inmate.temporaryAddressCity;
        this.temporaryAddressState = inmate.temporaryAddressState;
        this.temporaryAddressZip = inmate.temporaryAddressZip;
        this.temporaryTelephone = inmate.temporaryTelephone;
        this.eContactLastName = inmate.eContactLastName;
        this.eContactFirstName = inmate.eContactFirstName;
        this.eContactMiddleName = inmate.eContactMiddleName;
        this.eContactRelationship = inmate.eContactRelationship;
        this.eContactAddressStreet = inmate.eContactAddressStreet;
        this.eContactAddressCity = inmate.eContactAddressCity;
        this.eContactAddressState = inmate.eContactAddressState;
        this.eContactAddressZip = inmate.eContactAddressZip;
        this.eContactTelephone = inmate.eContactTelephone;
        this.occupation = inmate.occupation;
        this.skills = inmate.skills;
        this.highestGradeCompleted = inmate.highestGradeCompleted;
        this.levelOfEnglish = inmate.levelOfEnglish;
        this.employerName = inmate.employerName;
        this.employerTypeOfBusiness = inmate.employerTypeOfBusiness;
        this.employerAddressStreet = inmate.employerAddressStreet;
        this.employerAddressCity = inmate.employerAddressCity;
        this.employerAddressState = inmate.employerAddressState;
        this.employerAddressZip = inmate.employerAddressZip;
        this.employerTelephone = inmate.employerTelephone;
        this.position = inmate.position;
        this.specialIdentifiers = inmate.specialIdentifiers;
        this.gangAffiliationName = inmate.gangAffiliationName;
        this.gangAffiliationCity = inmate.gangAffiliationCity;
        this.gangAffiliationStatus = inmate.gangAffiliationStatus;
        this.driversLicenseNumber = inmate.driversLicenseNumber;
        this.driversLicenseState = inmate.driversLicenseState;
        this.vehiclePlateNumber = inmate.vehiclePlateNumber;
        this.vehiclePlateState = inmate.vehiclePlateState;
        this.vehicleYear = inmate.vehicleYear;
        this.vehicleMake = inmate.vehicleMake;
        this.vehicleModel = inmate.vehicleModel;
        this.vehicleColor = inmate.vehicleColor;
        this.vehicleDisposition = inmate.vehicleDisposition;
        this.vehicleImpoundCompany = inmate.vehicleImpoundCompany;
        this.vehicleImpoundCompanyAddressStreet = inmate.vehicleImpoundCompanyAddressStreet;
        this.vehicleImpoundCompanyAddressCity = inmate.vehicleImpoundCompanyAddressCity;
        this.vehicleImpoundCompanyAddressState = inmate.vehicleImpoundCompanyAddressState;
        this.vehicleImpoundCompanyAddressZip = inmate.vehicleImpoundCompanyAddressZip;
        this.vehicleParkedAddressStreet = inmate.vehicleParkedAddressStreet;
        this.vehicleParkedAddressCity = inmate.vehicleParkedAddressCity;
        this.vehicleParkedAddressState = inmate.vehicleParkedAddressState;
        this.vehicleParkedAddressZip = inmate.vehicleParkedAddressZip;
        this.vehicleOther = inmate.vehicleOther;
        this.arrestLocationStreet = inmate.arrestLocationStreet;
        this.arrestLocationCity = inmate.arrestLocationCity;
        this.reportingDistrict = inmate.reportingDistrict;
        this.arrestingAgency = inmate.arrestingAgency;
        this.dateArrested = inmate.dateArrested;
        this.timeArrested = inmate.timeArrested;
        this.arrestCharges = inmate.arrestCharges;
        this.arrestingOfficerDepartment = inmate.arrestingOfficerDepartment;
        this.transportingOfficerDepartment = inmate.transportingOfficerDepartment;
        this.bookingDate = inmate.bookingDate;
        this.timeBooked = inmate.timeBooked;
        this.bookingClerkDepartment = inmate.bookingClerkDepartment;
        this.searchingOfficerDepartment = inmate.searchingOfficerDepartment;
        this.cash = inmate.cash;
        this.nonCash = inmate.nonCash;
        this.arrestBail = inmate.arrestBail;
    }

    public void printAll() {
        System.out.println(lastName);
        System.out.println(firstName);
        System.out.println(middleName);
        System.out.println(suffix);
        System.out.println(aka);

        System.out.println(age);
        System.out.println(placeOfBirthCity);
        System.out.println(placeOfBirthState);
        System.out.println(placeOfBirthCountry);
        System.out.println(foreignNationalStatus);
        System.out.println(foreignNationalCountry);

        System.out.println(sex);
        System.out.println(descent);
        System.out.println(hairColor);
        System.out.println(eyeColor);

        System.out.println(weight);
        System.out.println(SMT);
        System.out.println(permanentAddressStreet);
        System.out.println(permanentAddressCity);
        System.out.println(permanentAddressState);
        System.out.println(permanentAddressZip);

        System.out.println(temporaryAddressStreet);
        System.out.println(temporaryAddressCity);
        System.out.println(temporaryAddressState);
        System.out.println(temporaryAddressZip);

        System.out.println(eContactLastName);
        System.out.println(eContactFirstName);
        System.out.println(eContactMiddleName);
        System.out.println(eContactRelationship);
        System.out.println(eContactAddressStreet);
        System.out.println(eContactAddressCity);
        System.out.println(eContactAddressState);
        System.out.println(eContactAddressZip);

        System.out.println(occupation);
        System.out.println(skills);
        System.out.println(highestGradeCompleted);
        System.out.println(levelOfEnglish);
        System.out.println(employerName);
        System.out.println(employerTypeOfBusiness);
        System.out.println(employerAddressStreet);
        System.out.println(employerAddressCity);
        System.out.println(employerAddressState);
        System.out.println(employerAddressZip);

        System.out.println(position);
        System.out.println(specialIdentifiers);
        System.out.println(gangAffiliationName);
        System.out.println(gangAffiliationCity);
        System.out.println(gangAffiliationStatus);
        System.out.println(driversLicenseNumber);
        System.out.println(driversLicenseState);
        System.out.println(vehiclePlateNumber);
        System.out.println(vehiclePlateState);
        System.out.println(vehicleYear);
        System.out.println(vehicleMake);
        System.out.println(vehicleModel);
        System.out.println(vehicleColor);
        System.out.println(vehicleDisposition);
        System.out.println(vehicleImpoundCompany);
        System.out.println(vehicleImpoundCompanyAddressStreet);
        System.out.println(vehicleImpoundCompanyAddressCity);
        System.out.println(vehicleImpoundCompanyAddressState);
        System.out.println(vehicleImpoundCompanyAddressZip);
        System.out.println(vehicleParkedAddressStreet);
        System.out.println(vehicleParkedAddressCity);
        System.out.println(vehicleParkedAddressState);
        System.out.println(vehicleParkedAddressZip);
        System.out.println(vehicleOther);
        System.out.println(arrestLocationStreet);
        System.out.println(arrestLocationCity);
        System.out.println(reportingDistrict);
        System.out.println(arrestingAgency);

        System.out.println(timeArrested);
        System.out.println(arrestCharges);
        System.out.println(arrestingOfficerDepartment);
        System.out.println(transportingOfficerDepartment);

        System.out.println(timeBooked);
        System.out.println(bookingClerkDepartment);
        System.out.println(searchingOfficerDepartment);
        System.out.println(cash);
        System.out.println(nonCash);
        System.out.println(arrestBail);
    }

}
