/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import error_handle.HandlePopUp;
import inmate.Inmate;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import login.PasswordEncrypt;

/**
 *
 * @author cello
 */
public class SQL_SET {

    // submitButtonPushed returns the int value of the booking ID for the form
    // that was processed if it was successfully added to the database.
    // It will return a value of -1 if there was an error adding the inmate to
    // the database or if there was an error adding the booking to the database.
    public static int submitButtonPushed(Inmate inmate) throws IOException {
        int pobAddyID = -1;             //is set
        int permAddyID = -1;            //is set
        int tempAddyID = -1;            //is set
        int eContactID = -1;            //is set
        int eContactAddyID = -1;        //is set
        int employerID = -1;            //is set
        int employerAddyID = -1;        //is set
        int vehicleColorID = -1;        //is set
        int vehicleID = -1;             //is set
        int vehicleAddyID = -1;         //is set
        int impoundCompanyID = -1;      //is set
        int impoundCompanyAddyID = -1;  //is set
        int arrestAddyID = -1;          //is set
        
        int arrestDeptID = -1;
        int transDeptID = -1;
        int bookingDeptID = -1;
        int searchDeptID = -1;
        
        if(!inmate.arrestingOfficerDepartment.isEmpty())
            arrestDeptID = SQL_GET.getDepartmentID(inmate.arrestingOfficerDepartment);
        if(!inmate.transportingOfficerDepartment.isEmpty())
            transDeptID = SQL_GET.getDepartmentID(inmate.transportingOfficerDepartment);
        if(!inmate.bookingClerkDepartment.isEmpty())
            bookingDeptID = SQL_GET.getDepartmentID(inmate.bookingClerkDepartment);
        if(!inmate.searchingOfficerDepartment.isEmpty())
            searchDeptID = SQL_GET.getDepartmentID(inmate.searchingOfficerDepartment);
        
        String inmateID = "";
        int bookingID = -1;

        // Add the addresses, employer info, vehicle info,
        // emergency contact info and impound company info (if applicable) in
        // order to get their IDs to send with the new inmate and
        // booking entries
        if(!inmate.placeOfBirthCity.isEmpty()
                || !inmate.placeOfBirthState.isEmpty()
                || !inmate.placeOfBirthCountry.isEmpty()) {
            ArrayList<String> pobList = new ArrayList<String>();
            pobList.add("");
            pobList.add(inmate.placeOfBirthCity);
            pobList.add(inmate.placeOfBirthState);
            pobList.add("");
            pobList.add(inmate.placeOfBirthCountry);
            pobList.add("");
            pobAddyID = addToAddresses(pobList);
        }
        if(!inmate.permanentAddressStreet.isEmpty()
                || !inmate.permanentAddressState.isEmpty()
                || !inmate.permanentAddressCity.isEmpty()
                || !inmate.permanentAddressZip.isEmpty()) {
            ArrayList<String> permAddyList = new ArrayList<String>();
            permAddyList.add(inmate.permanentAddressStreet);
            permAddyList.add(inmate.permanentAddressCity);
            permAddyList.add(inmate.permanentAddressState);
            permAddyList.add(inmate.permanentAddressZip);
            permAddyList.add("");
            permAddyList.add("");
            permAddyID = addToAddresses(permAddyList);
        }
        if(!inmate.temporaryAddressStreet.isEmpty()
                || !inmate.temporaryAddressState.isEmpty()
                || !inmate.temporaryAddressCity.isEmpty()
                || !inmate.temporaryAddressZip.isEmpty()) {
            ArrayList<String> tempAddyList = new ArrayList<String>();
            tempAddyList.add(inmate.temporaryAddressStreet);
            tempAddyList.add(inmate.temporaryAddressCity);
            tempAddyList.add(inmate.temporaryAddressState);
            tempAddyList.add(inmate.temporaryAddressZip);
            tempAddyList.add("");
            tempAddyList.add("");
            tempAddyID = addToAddresses(tempAddyList);
        }
        if(!inmate.eContactAddressStreet.isEmpty()
                || !inmate.eContactAddressCity.isEmpty()
                || !inmate.eContactAddressState.isEmpty()
                || !inmate.eContactAddressZip.isEmpty()) {
            ArrayList<String> eContactAddyList = new ArrayList<String>();
            eContactAddyList.add(inmate.eContactAddressStreet);
            eContactAddyList.add(inmate.eContactAddressCity);
            eContactAddyList.add(inmate.eContactAddressState);
            eContactAddyList.add(inmate.eContactAddressZip);
            eContactAddyList.add("");
            eContactAddyList.add("");
            eContactAddyID = addToAddresses(eContactAddyList);
        }
        if(!inmate.eContactFirstName.isEmpty()
                || !inmate.eContactLastName.isEmpty()
                || !inmate.eContactMiddleName.isEmpty()
                || !inmate.eContactTelephone1.isEmpty()
                || !inmate.eContactTelephone2.isEmpty()
                || !inmate.eContactTelephone3.isEmpty()) {
            ArrayList<String> eContactList = new ArrayList<String>();
            eContactList.add(inmate.eContactFirstName);
            eContactList.add(inmate.eContactLastName);
            eContactList.add(inmate.eContactMiddleName);
            eContactList.add("");
            eContactList.add("(" + inmate.eContactTelephone1 + ")" + inmate.eContactTelephone2 + "-" + inmate.eContactTelephone3);
            int eContactRelationshipID = SQL_GET.getRelationshipID(inmate.eContactRelationship);
            eContactID = addToEmergencyContacts(eContactList, eContactRelationshipID, eContactAddyID);
        }
        if(!inmate.employerAddressStreet.isEmpty()
                || !inmate.employerAddressCity.isEmpty()
                || !inmate.employerAddressState.isEmpty()
                || !inmate.employerAddressZip.isEmpty()) {
            ArrayList<String> employerAddyList = new ArrayList<String>();
            employerAddyList.add(inmate.employerAddressStreet);
            employerAddyList.add(inmate.employerAddressCity);
            employerAddyList.add(inmate.employerAddressState);
            employerAddyList.add(inmate.employerAddressZip);
            employerAddyList.add("");
            employerAddyList.add("");
            employerAddyID = addToAddresses(employerAddyList);
        }
        if(!inmate.employerName.isEmpty()
                || !inmate.employerTypeOfBusiness.isEmpty()
                || !inmate.employerTelephone1.isEmpty()
                || !inmate.employerTelephone2.isEmpty()
                || !inmate.employerTelephone3.isEmpty()) {
            ArrayList<String> employerList = new ArrayList<String>();
            employerList.add(inmate.employerName);
            employerList.add(inmate.employerTypeOfBusiness);
            employerList.add("(" + inmate.employerTelephone1 + ")" + inmate.employerTelephone2 + "-" + inmate.employerTelephone3);
            employerID = addToEmployers(employerList, employerAddyID);
        }
        if(!inmate.vehicleParkedAddressStreet.isEmpty()
                || !inmate.vehicleParkedAddressCity.isEmpty()
                || !inmate.vehicleParkedAddressState.isEmpty()
                || !inmate.vehicleParkedAddressZip.isEmpty()) {
            ArrayList<String> vehicleAddyList = new ArrayList<String>();
            vehicleAddyList.add(inmate.vehicleParkedAddressStreet);
            vehicleAddyList.add(inmate.vehicleParkedAddressCity);
            vehicleAddyList.add(inmate.vehicleParkedAddressState);
            vehicleAddyList.add(inmate.vehicleParkedAddressZip);
            vehicleAddyList.add("");
            vehicleAddyList.add("");
            vehicleAddyID = addToAddresses(vehicleAddyList);
        }
        if(!inmate.vehiclePlateNumber.isEmpty()
                || !inmate.vehiclePlateState.isEmpty()
                || !inmate.vehicleYear.isEmpty()
                || !inmate.vehicleMake.isEmpty()
                || !inmate.vehicleModel.isEmpty()
                || !inmate.vehicleColor.isEmpty()) {
            ArrayList<String> vehicleList = new ArrayList<String>();
            vehicleList.add(inmate.vehiclePlateNumber);
            vehicleList.add(inmate.vehiclePlateState);
            vehicleList.add(inmate.vehicleMake);
            vehicleList.add(inmate.vehicleModel);
            vehicleList.add(inmate.vehicleYear);
            vehicleColorID = SQL_GET.getColorID(inmate.vehicleColor);
            vehicleID = addToVehicles(vehicleList, vehicleColorID);
        }
        if(!inmate.vehicleImpoundCompanyAddressStreet.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressCity.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressState.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressZip.isEmpty()) {
            ArrayList<String> impoundAddyList = new ArrayList<String>();
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressStreet);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressCity);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressState);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressZip);
            impoundAddyList.add("");
            impoundAddyList.add("");
            impoundCompanyAddyID = addToAddresses(impoundAddyList);
        }
        if(!inmate.vehicleImpoundCompany.isEmpty()) {
            ArrayList<String> impoundCompanyList = new ArrayList<String>();
            impoundCompanyList.add(inmate.vehicleImpoundCompany);
            impoundCompanyList.add("");
            impoundCompanyID = addToImpoundCompanies(impoundCompanyList, impoundCompanyAddyID);
        }
        if(!inmate.arrestLocationCity.isEmpty()
                || !inmate.arrestLocationStreet.isEmpty()) {
            ArrayList<String> arrestLocationList = new ArrayList<String>();
            arrestLocationList.add(inmate.arrestLocationStreet);
            arrestLocationList.add(inmate.arrestLocationCity);
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestAddyID = addToAddresses(arrestLocationList);
        }

        /**
         * ****Start adding the inmate and the booking to the database*****
         */
        inmateID = addToInmates(inmate, permAddyID, tempAddyID, pobAddyID, eContactID, employerID);

        if(inmateID.isEmpty()) {
            System.out.println("ERROR in SQL_SET.subitButtonPushed: Inmate data unable to reach the server");
            return -1;
        }
        else {
            bookingID = addToBookings(inmate, inmateID, arrestAddyID, vehicleID, vehicleAddyID, arrestDeptID, transDeptID, bookingDeptID, searchDeptID);
        }

        /**
         * **********Do Removes if Booking doesn't make it to DB************
         */
        if(bookingID < 1) {
            System.out.println("ERROR in SQL_SET.subitButtonPushed: Booking data unable to reach the server");
            SQL_REMOVE.removeAddressForID(arrestAddyID);
            SQL_REMOVE.removeVehicleForID(vehicleID);
            SQL_REMOVE.removeAddressForID(vehicleAddyID);
        }

        return bookingID;
    }

    public static String updateButtonPushed(Inmate inmate) throws IOException {
        int pobAddyID = -1;             //is set
        int permAddyID = -1;            //is set
        int tempAddyID = -1;            //is set
        int eContactID = -1;            //is set
        int eContactAddyID = -1;        //is set
        int employerID = -1;            //is set
        int employerAddyID = -1;        //is set
        int vehicleColorID = -1;        //is set
        int vehicleID = -1;             //is set
        int vehicleAddyID = -1;         //is set
        int impoundCompanyID = -1;      //is set
        int impoundCompanyAddyID = -1;  //is set
        int arrestAddyID = -1;          //is set
        
        int arrestDeptID = -1;
        int transDeptID = -1;
        int bookingDeptID = -1;
        int searchDeptID = -1;
        
        if(!inmate.arrestingOfficerDepartment.isEmpty())
            arrestDeptID = SQL_GET.getDepartmentID(inmate.arrestingOfficerDepartment);
        if(!inmate.transportingOfficerDepartment.isEmpty())
            transDeptID = SQL_GET.getDepartmentID(inmate.transportingOfficerDepartment);
        if(!inmate.bookingClerkDepartment.isEmpty())
            bookingDeptID = SQL_GET.getDepartmentID(inmate.bookingClerkDepartment);
        if(!inmate.searchingOfficerDepartment.isEmpty())
            searchDeptID = SQL_GET.getDepartmentID(inmate.searchingOfficerDepartment);
        
        String inmateID = "";
        int bookingID = -1;

        // Add the addresses, employer info, vehicle info,
        // emergency contact info and impound company info (if applicable) in
        // order to get their IDs to send with the new inmate and
        // booking entries
        if(!inmate.placeOfBirthCity.isEmpty()
                || !inmate.placeOfBirthState.isEmpty()
                || !inmate.placeOfBirthCountry.isEmpty()) {
            ArrayList<String> pobList = new ArrayList<String>();
            pobList.add("");
            pobList.add(inmate.placeOfBirthCity);
            pobList.add(inmate.placeOfBirthState);
            pobList.add("");
            pobList.add(inmate.placeOfBirthCountry);
            pobList.add("");
            pobAddyID = addToAddresses(pobList);
        }
        if(!inmate.permanentAddressStreet.isEmpty()
                || !inmate.permanentAddressState.isEmpty()
                || !inmate.permanentAddressCity.isEmpty()
                || !inmate.permanentAddressZip.isEmpty()) {
            ArrayList<String> permAddyList = new ArrayList<String>();
            permAddyList.add(inmate.permanentAddressStreet);
            permAddyList.add(inmate.permanentAddressCity);
            permAddyList.add(inmate.permanentAddressState);
            permAddyList.add(inmate.permanentAddressZip);
            permAddyList.add("");
            permAddyList.add("");
            permAddyID = addToAddresses(permAddyList);
        }
        if(!inmate.temporaryAddressStreet.isEmpty()
                || !inmate.temporaryAddressState.isEmpty()
                || !inmate.temporaryAddressCity.isEmpty()
                || !inmate.temporaryAddressZip.isEmpty()) {
            ArrayList<String> tempAddyList = new ArrayList<String>();
            tempAddyList.add(inmate.temporaryAddressStreet);
            tempAddyList.add(inmate.temporaryAddressCity);
            tempAddyList.add(inmate.temporaryAddressState);
            tempAddyList.add(inmate.temporaryAddressZip);
            tempAddyList.add("");
            tempAddyList.add("");
            tempAddyID = addToAddresses(tempAddyList);
        }
        if(!inmate.eContactAddressStreet.isEmpty()
                || !inmate.eContactAddressCity.isEmpty()
                || !inmate.eContactAddressState.isEmpty()
                || !inmate.eContactAddressZip.isEmpty()) {
            ArrayList<String> eContactAddyList = new ArrayList<String>();
            eContactAddyList.add(inmate.eContactAddressStreet);
            eContactAddyList.add(inmate.eContactAddressCity);
            eContactAddyList.add(inmate.eContactAddressState);
            eContactAddyList.add(inmate.eContactAddressZip);
            eContactAddyList.add("");
            eContactAddyList.add("");
            eContactAddyID = addToAddresses(eContactAddyList);
        }
        if(!inmate.eContactFirstName.isEmpty()
                || !inmate.eContactLastName.isEmpty()
                || !inmate.eContactMiddleName.isEmpty()
                || !inmate.eContactTelephone1.isEmpty()
                || !inmate.eContactTelephone2.isEmpty()
                || !inmate.eContactTelephone3.isEmpty()) {
            ArrayList<String> eContactList = new ArrayList<String>();
            eContactList.add(inmate.eContactFirstName);
            eContactList.add(inmate.eContactLastName);
            eContactList.add(inmate.eContactMiddleName);
            eContactList.add("");
            eContactList.add("(" + inmate.eContactTelephone1 + ")" + inmate.eContactTelephone2 + "-" + inmate.eContactTelephone3);
            int eContactRelationshipID = SQL_GET.getRelationshipID(inmate.eContactRelationship);
            eContactID = addToEmergencyContacts(eContactList, eContactRelationshipID, eContactAddyID);
        }
        if(!inmate.employerAddressStreet.isEmpty()
                || !inmate.employerAddressCity.isEmpty()
                || !inmate.employerAddressState.isEmpty()
                || !inmate.employerAddressZip.isEmpty()) {
            ArrayList<String> employerAddyList = new ArrayList<String>();
            employerAddyList.add(inmate.employerAddressStreet);
            employerAddyList.add(inmate.employerAddressCity);
            employerAddyList.add(inmate.employerAddressState);
            employerAddyList.add(inmate.employerAddressZip);
            employerAddyList.add("");
            employerAddyList.add("");
            employerAddyID = addToAddresses(employerAddyList);
        }
        if(!inmate.employerName.isEmpty()
                || !inmate.employerTypeOfBusiness.isEmpty()
                || !inmate.employerTelephone1.isEmpty()
                || !inmate.employerTelephone2.isEmpty()
                || !inmate.employerTelephone3.isEmpty()) {
            ArrayList<String> employerList = new ArrayList<String>();
            employerList.add(inmate.employerName);
            employerList.add(inmate.employerTypeOfBusiness);
            employerList.add("(" + inmate.employerTelephone1 + ")" + inmate.employerTelephone2 + "-" + inmate.employerTelephone3);
            employerID = addToEmployers(employerList, employerAddyID);
        }
        if(!inmate.vehicleParkedAddressStreet.isEmpty()
                || !inmate.vehicleParkedAddressCity.isEmpty()
                || !inmate.vehicleParkedAddressState.isEmpty()
                || !inmate.vehicleParkedAddressZip.isEmpty()) {
            ArrayList<String> vehicleAddyList = new ArrayList<String>();
            vehicleAddyList.add(inmate.vehicleParkedAddressStreet);
            vehicleAddyList.add(inmate.vehicleParkedAddressCity);
            vehicleAddyList.add(inmate.vehicleParkedAddressState);
            vehicleAddyList.add(inmate.vehicleParkedAddressZip);
            vehicleAddyList.add("");
            vehicleAddyList.add("");
            vehicleAddyID = addToAddresses(vehicleAddyList);
        }
        if(!inmate.vehiclePlateNumber.isEmpty()
                || !inmate.vehiclePlateState.isEmpty()
                || !inmate.vehicleYear.isEmpty()
                || !inmate.vehicleMake.isEmpty()
                || !inmate.vehicleModel.isEmpty()
                || !inmate.vehicleColor.isEmpty()) {
            ArrayList<String> vehicleList = new ArrayList<String>();
            vehicleList.add(inmate.vehiclePlateNumber);
            vehicleList.add(inmate.vehiclePlateState);
            vehicleList.add(inmate.vehicleMake);
            vehicleList.add(inmate.vehicleModel);
            vehicleList.add(inmate.vehicleYear);
            vehicleColorID = SQL_GET.getColorID(inmate.vehicleColor);
            vehicleID = addToVehicles(vehicleList, vehicleColorID);
        }
        if(!inmate.vehicleImpoundCompanyAddressStreet.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressCity.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressState.isEmpty()
                || !inmate.vehicleImpoundCompanyAddressZip.isEmpty()) {
            ArrayList<String> impoundAddyList = new ArrayList<String>();
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressStreet);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressCity);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressState);
            impoundAddyList.add(inmate.vehicleImpoundCompanyAddressZip);
            impoundAddyList.add("");
            impoundAddyList.add("");
            impoundCompanyAddyID = addToAddresses(impoundAddyList);
        }
        if(!inmate.vehicleImpoundCompany.isEmpty()) {
            ArrayList<String> impoundCompanyList = new ArrayList<String>();
            impoundCompanyList.add(inmate.vehicleImpoundCompany);
            impoundCompanyList.add("");
            impoundCompanyID = addToImpoundCompanies(impoundCompanyList, impoundCompanyAddyID);
        }
        if(!inmate.arrestLocationCity.isEmpty()
                || !inmate.arrestLocationStreet.isEmpty()) {
            ArrayList<String> arrestLocationList = new ArrayList<String>();
            arrestLocationList.add(inmate.arrestLocationStreet);
            arrestLocationList.add(inmate.arrestLocationCity);
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestLocationList.add("");
            arrestAddyID = addToAddresses(arrestLocationList);
        }

        /**
         * ****Start adding the inmate to the database*****
         */
        inmateID = addToInmates(inmate, permAddyID, tempAddyID, pobAddyID, eContactID, employerID);

        if(inmateID.isEmpty()) {
            System.out.println("ERROR in SQL_SET.updateButtonPushed: Inmate data unable to reach the server");
            return "";
        }
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
        Date date = new Date();
        HandlePopUp.showSuccess("Successful Inmate Submission", "Inmate ID " + inmateID + " booked on " + dateFormat.format(date) + " at " + timeFormat.format(date));
        return inmateID;
    }

    // The following methods return an int ID of the indicated entry if the
    // add was successful or if the entry already exists in the database.
    // They will return a value of -1 in the event of an error.
    public static int addToAddresses(ArrayList<String> list) {
        int addressID = -1;
        String query = "INSERT INTO Addresses VALUES (null";

        // An Address list needs to have 6 entries, even if some are empty...
        // Street Address, City, State, Zip, Country, and District 
        if(list.size() != 6) {
            System.out.println("Error in addToAddresses: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                addressID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return addressID;
    }

    public static int addToVehicles(ArrayList<String> list, int colorID) {
        int vehicleID = -1;
        String query = "INSERT INTO Vehicles VALUES (null";

        if(SQL_GET.isVehicleInDB(list.get(0), list.get(1))) {
            System.out.println("Vehicle already in database with plates: " + list.get(1) + " " + list.get(0));
            return SQL_GET.getVehicleID(list.get(0), list.get(1));
        }

        // An Vehicle list needs to have 5 entries, even if some are empty...
        // Plate number, Plate state, make, model, and year
        if(list.size() != 5) {
            System.out.println("Error in addToVehicles: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(colorID < 1) {
            query += ", null";
        }
        else {
            query += ", " + colorID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                vehicleID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return vehicleID;
    }

    public static int addToEmployers(ArrayList<String> list, int addressID) {
        int employerID = -1;
        String query = "INSERT INTO Employers VALUES (null";

        if(SQL_GET.isEmployerInDB(list.get(0), list.get(2))) {
            System.out.println("Employer already in database with name and number: " + list.get(0) + " " + list.get(2));
            return SQL_GET.getEmployerID(list.get(0), list.get(2));
        }

        // An Employer list needs to have 3 entries, even if some are empty...
        // Name, Business Type, and Phone number
        if(list.size() != 3) {
            System.out.println("Error in addToEmployers: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(addressID < 1) {
            query += ", null";
        }
        else {
            query += ", " + addressID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                employerID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return employerID;
    }

    public static int addToDepartments(ArrayList<String> list, int addressID) {
        int departmentID = -1;
        String query = "INSERT INTO Departments VALUES (null";

        if(SQL_GET.isDepartmentInDB(list.get(0))) {
            System.out.println("Department already in database with name: " + list.get(0));
            return SQL_GET.getDepartmentID(list.get(0));
        }

        // A Department list needs to have 1 entry...
        // Name
        if(list.size() != 1) {
            System.out.println("Error in addToDepartments: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(addressID < 1) {
            query += ", null";
        }
        else {
            query += ", " + addressID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                departmentID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return departmentID;
    }

    public static int addToUsers(ArrayList<String> list, int departmentID) {
        int userID = -1;
        String query = "INSERT INTO Users VALUES (null";

        if(SQL_GET.isUserLoginNameInDB(list.get(0))) {
            System.out.println("User with login name " + list.get(0) + " already exists");
            return SQL_GET.getUserID(list.get(0));
        }

        // A User list needs to have 7 entries, even if some are empty...
        // Login name, Login password(pre-encrypted), User role, Last name, First Name, Position, Last login
        if(list.size() != 7) {
            System.out.println("Error in addToEmployers: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(departmentID < 1) {
            query += ", null";
        }
        else {
            query += ", " + departmentID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                userID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return userID;
    }

    public static int addToEmergencyContacts(ArrayList<String> list, int relationshipID, int addressID) {
        int emergencyContactID = -1;
        String query = "INSERT INTO Emergency_Contacts VALUES (null";

        // An Emergency Contact list needs to have 5 entries, even if some are empty...
        // First name, Last name, Middle name, Suffix, and Phone number
        if(list.size() != 5) {
            System.out.println("Error in addToEmployers: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(relationshipID < 1) {
            query += ", null";
        }
        else {
            query += ", " + relationshipID;
        }
        if(addressID < 1) {
            query += ", null";
        }
        else {
            query += ", " + addressID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                emergencyContactID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return emergencyContactID;
    }

    public static int addToImpoundCompanies(ArrayList<String> list, int addressID) {
        int impoundCompanyID = -1;
        String query = "INSERT INTO UM_Impound_Companies VALUES (null";

        if(SQL_GET.isImpoundCompanyInDB(list.get(0))) {
            return SQL_GET.getImpoundCompanyID(list.get(0));
        }

        // An Impound Company list needs to have 2 entries, even if some are empty...
        // Name, and Phone number
        if(list.size() != 2) {
            System.out.println("Error in addToImpoundCompanies: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(addressID < 1) {
            query += ", null";
        }
        else {
            query += ", " + addressID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                impoundCompanyID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return impoundCompanyID;
    }

    public static int addToBookings(Inmate inmate, String inmateID, int arrestAddyID, int vehicleID, int vehicleAddyID, int arrestDeptID, int transDeptID, int bookingDeptID, int searchDeptID) throws IOException {
        int bookingID = -1;
        String query = "INSERT INTO Bookings VALUES (null, \"" + inmateID + "\"";
        ArrayList<String> list = new ArrayList<String>();

        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
        Date date = new Date();
        System.out.println("Inmate ID " + inmateID + " booked on " + dateFormat.format(date) + " at " + timeFormat.format(date));
        HandlePopUp.showSuccess("Successful Inmate Submission", "Inmate ID " + inmateID + " booked on " + dateFormat.format(date) + " at " + timeFormat.format(date));

        list.add(inmate.dateArrested1 + "/" + inmate.dateArrested2 + "/" + inmate.dateArrested3);
        list.add(dateFormat.format(date));
        list.add(inmate.timeArrested);
        list.add(timeFormat.format(date));
        list.add(inmate.arrestCharges);
        list.add(inmate.arrestingAgency);
        list.add(inmate.vehicleDisposition);
        list.add(inmate.vehicleImpoundCompany);
        list.add(inmate.arrestBail);
        list.add(inmate.cash);
        list.add(inmate.nonCash);

        // A Booking list needs to have a lot of entries, even if some are empty...
        // 11 entries in total
        if(list.size() != 11) {
            System.out.println("Error in addToBookings: List is not the correct size");
            return -1;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }
        if(arrestAddyID < 1) {
            query += ", null";
        }
        else {
            query += ", " + arrestAddyID;
        }
        if(vehicleID < 1) {
            query += ", null";
        }
        else {
            query += ", " + vehicleID;
        }
        if(vehicleAddyID < 1) {
            query += ", null";
        }
        else {
            query += ", " + vehicleAddyID;
        }
        if(arrestDeptID < 1) {
            query += ", null";
        }
        else {
            query += ", " + arrestDeptID;
        }
        if(transDeptID < 1) {
            query += ", null";
        }
        else {
            query += ", " + transDeptID;
        }
        if(bookingDeptID < 1) {
            query += ", null";
        }
        else {
            query += ", " + bookingDeptID;
        }
        if(searchDeptID < 1) {
            query += ", null";
        }
        else {
            query += ", " + searchDeptID;
        }
        query += ");";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                bookingID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return bookingID;
    }

    // The following method returns the String value of the inmate's ID if the
    // add was successful or if the inmate already exists in the database.
    // addToInmates will return an empty string (not a null string!) in the
    // event of an error.
    public static String addToInmates(Inmate inmate, int permAddyID, int tempAddyID, int pobAddyID, int eContactID, int employerID) {
        int rowsAffected = 0;
        String inmateID = "";
        String stringToHash = inmate.firstName + inmate.lastName + inmate.hairColor + inmate.eyeColor;
        String inmateSSN = inmate.SSN1 + inmate.SSN2 + inmate.SSN3;

        if(inmateSSN.isEmpty()) {
            inmateID = PasswordEncrypt.cryptPassword(stringToHash).substring(0, 10);
        }
        else {
            inmateID = inmateSSN;
        }

        if(SQL_GET.isInmateInDB(inmateID)) {
            return inmateID;
        }

        String query = "INSERT INTO Inmates VALUES (\"" + inmateID + "\"";
        ArrayList<String> list = new ArrayList<String>();

        list.add(inmate.lastName);
        list.add(inmate.firstName);
        list.add(inmate.middleName);
        list.add(inmate.suffix);

        if(!inmate.dob1.isEmpty()
                || !inmate.dob2.isEmpty()
                || !inmate.dob3.isEmpty()) {
            list.add(inmate.dob1 + "/" + inmate.dob2 + "/" + inmate.dob3);
        }
        else {
            list.add("");
        }

        if(!inmate.permanentTelephone1.isEmpty()
                || !inmate.permanentTelephone2.isEmpty()
                || !inmate.permanentTelephone3.isEmpty()) {
            list.add("(" + inmate.permanentTelephone1 + ")" + inmate.permanentTelephone2 + "-" + inmate.permanentTelephone3);
        }
        else {
            list.add("");
        }

        if(!inmate.temporaryTelephone1.isEmpty()
                || !inmate.temporaryTelephone2.isEmpty()
                || !inmate.temporaryTelephone3.isEmpty()) {
            list.add("(" + inmate.temporaryTelephone1 + ")" + inmate.temporaryTelephone2 + "-" + inmate.temporaryTelephone3);
        }
        else {
            list.add("");
        }

        list.add(inmate.aka);
        list.add(inmate.foreignNationalStatus);
        list.add(inmate.foreignNationalCountry);
        list.add(inmate.SSN1 + inmate.SSN2 + inmate.SSN3);
        list.add(inmate.sex);
        list.add(inmate.descent);
        list.add(inmate.hairColor);
        list.add(inmate.eyeColor);

        if(!inmate.height1.isEmpty()
                || !inmate.height2.isEmpty()) {
            list.add(inmate.height1 + "'" + inmate.height2 + "''");
        }
        else {
            list.add("");
        }

        list.add(inmate.weight);
        list.add(inmate.SMT);
        list.add(inmate.driversLicenseNumber);
        list.add(inmate.driversLicenseState);
        list.add(inmate.specialIdentifiers);
        list.add(inmate.gangAffiliationName);
        list.add(inmate.gangAffiliationCity);
        list.add(inmate.gangAffiliationStatus);
        list.add(inmate.occupation);
        list.add(inmate.position);
        list.add(inmate.skills);
        list.add(inmate.highestGradeCompleted);
        list.add(inmate.levelOfEnglish);

        // An Inmate list needs to have a lot of entries, even if some are empty...
        // 29 entries in total
        if(list.size() != 29) {
            System.out.println("Error in addToInmates: List is not the correct size");
            return inmateID;
        }

        for(int i = 0; i < list.size(); i++) {
            query += ", \"";
            query += list.get(i);
            query += "\"";
        }

        if(permAddyID < 1) {
            query += ", null";
        }
        else {
            query += ", " + permAddyID;
        }
        if(tempAddyID < 1) {
            query += ", null";
        }
        else {
            query += ", " + tempAddyID;
        }
        if(pobAddyID < 1) {
            query += ", null";
        }
        else {
            query += ", " + pobAddyID;
        }
        if(eContactID < 1) {
            query += ", null";
        }
        else {
            query += ", " + eContactID;
        }
        if(employerID < 1) {
            query += ", null";
        }
        else {
            query += ", " + employerID;
        }
        query += ");";

        try {
            //ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rowsAffected = statement.executeUpdate(query);

            statement.close();
            //rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }

        if(rowsAffected > 0) {
            return inmateID;
        }
        else {
            return null;
        }
    }
    
    /**********************Methods to add to UM tables**********************/
    
    public static int addToUMArrestCharges(String entry, String codeNumber){
        int ID = -1;
        String fullString = entry.trim() + " (Code "+codeNumber.trim()+")";
        String query = "INSERT INTO UM_Arrest_Charges VALUES(NULL, \""+fullString+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMColors(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Colors VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMDescent(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Descent VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMEnglishAbility(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_English_Ability VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMEyeColor(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Eye_Color VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMGangs(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Gangs VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMHairColor(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Hair_Color VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMRelationships(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Relationships VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMSkills(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Skills VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMVehicleMakes(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Vehicle_Makes VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
    
    public static int addToUMVehicleModels(String entry){
        int ID = -1;
        String query = "INSERT INTO UM_Vehicle_Models VALUES(NULL, \""+entry.trim()+"\");";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            statement.execute(query, Statement.RETURN_GENERATED_KEYS);
            rs = statement.getGeneratedKeys();

            if(rs != null && rs.next()) {
                ID = rs.getInt(1);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return ID;
    }
}
