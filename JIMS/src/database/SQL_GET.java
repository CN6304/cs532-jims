package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import daily_log.Jesus;
import java.text.SimpleDateFormat;
import java.util.Date;
import inmate.InmateFromDB;

/**
 *
 * @author cello
 */
public class SQL_GET {
    
    /***********Methods to get listings from User-Maintained Tables************/

    public ArrayList<String> getArrestChargesList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Arrest_Charges;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }
    
    public ArrayList<String> getDepartmentsList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM Departments;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getVehicleModelList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Vehicle_Models ORDER BY NAME;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getVehicleColorList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Colors;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getDescentList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Descent;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getEnglishAbilityList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_English_Ability;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getEyeColorList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Eye_Color;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getGangsList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Gangs;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getHairColorList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Hair_Color;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getImpoundCompanyNamesList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Impound_Companies;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getImpoundCompanyPhoneList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT PHONE_NUMBER FROM UM_Impound_Companies;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("PHONE_NUMBER"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getRelationshipsList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Relationships;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getSkillsList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Skills;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getSMTList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Smt;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getSMTLocationsList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Smt_Locations;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }

    public ArrayList<String> getVehicleMakesList() throws SQLException {
        ArrayList<String> list = new ArrayList<String>();

        String query = "SELECT NAME FROM UM_Vehicle_Makes ORDER BY NAME;";

        ResultSet rs = null;
        Statement statement = SQL_Initialize.connection.createStatement();
        rs = statement.executeQuery(query);

        while(rs.next()) {
            list.add(rs.getString("NAME"));
        }
        statement.close();
        rs.close();
        return list;
    }
    
    /****************Methods to get various ID numbers from DB*****************/
    
    /**
     *
     * @param color
     * @return int value for the COLOR_ID
     */
    public static int getColorID(String color) {
        int colorID = -1;
        String query = "SELECT COLOR_ID FROM UM_Colors WHERE NAME=\"";
        query += color;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                colorID = rs.getInt(1);
            }
            else {
                System.out.println("getColorID returned no results for color" + color);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return colorID;
    }
    
    public static String getColorFromID(int ID) {
        String color = "";
        String query = "SELECT NAME FROM UM_Colors WHERE COLOR_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                color = rs.getString("NAME");
            }
            else {
                System.out.println("getColorFromID returned no results for color ID" + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return color;
    }
    
    public static String getColorFromVehicleID(int ID) {
        String color = "";
        String query = "SELECT COLOR_ID FROM Vehicles WHERE VEHICLE_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                color = getColorFromID(rs.getInt("COLOR_ID"));
            }
            else {
                System.out.println("getColorFromVehicleID returned no results for vehicle ID" + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return color;
    }
    
    public static int getDepartmentID (String departmentName) {
        int departmentID = -1;
        String query = "SELECT DEPARTMENT_ID FROM Departments WHERE NAME=\"";
        query += departmentName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                departmentID = rs.getInt(1);
            }
            else {
                System.out.println("getDepartmentID returned no results for department:" + departmentName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return departmentID;
    }
    
    public static String getDepartmentNameFromID (int ID) {
        String name = "";
        String query = "SELECT NAME FROM Departments WHERE DEPARTMENT_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                name = rs.getString("NAME");
            }
            else {
                System.out.println("getDepartmentNameFromID returned no results for department ID:" + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return name;
    }
    
    public static int getRelationshipID(String relationship){
        int relationshipID = -1;
        String query = "SELECT RELATIONSHIP_ID FROM UM_Relationships WHERE NAME=\"";
        query += relationship;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                relationshipID = rs.getInt(1);
            }
            else {
                System.out.println("getRelationshipID returned no results for relationship:" + relationship);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return relationshipID;
    }
    
    public static String getRelationshipFromID(int ID){
        String relationship = "";
        String query = "SELECT NAME FROM UM_Relationships WHERE RELATIONSHIP_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                relationship = rs.getString("NAME");
            }
            else {
                System.out.println("getRelationshipFromID returned no results for relationship ID:" + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return relationship;
    }
    
    public static int getImpoundCompanyID(String companyName){
        int companyID = -1;
        String query = "SELECT IMPOUND_COMPANY_ID FROM UM_Impound_Companies WHERE NAME=\"";
        query += companyName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                companyID = rs.getInt(1);
            }
            else {
                System.out.println("getImpoundCompanyID returned no results for company:" + companyName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return companyID;
    }
    
    public static int getVehicleID(String plateNumber, String plateState){
        int vehicleID = -1;
        String query = "SELECT VEHICLE_ID FROM Vehicles WHERE PLATE_NUMBER=\"";
        query += plateNumber;
        query += "\" AND PLATE_STATE=\""+plateState+"\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                vehicleID = rs.getInt(1);
            }
            else {
                System.out.println("getVehicleID returned no results for Vehicle with plate:" + plateState +" "+plateNumber);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return vehicleID;
    }
    
    public static String[] getVehicleAttributesFromID(int ID){
        String[] array = {"","","","",""};
        String query = "SELECT * FROM Vehicles WHERE VEHICLE_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                array[0] = rs.getString("PLATE_NUMBER");
                array[1] = rs.getString("PLATE_STATE");
                array[2] = rs.getString("MAKE");
                array[3] = rs.getString("MODEL");
                array[4] = rs.getString("YEAR");
            }
            else {
                System.out.println("getVehicleAttributesFromID returned no results for vehicle ID: "+ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return array;
    }
    
    public static String[] getImpoundAddressAttributesFromImpoundID(int ID){
        String[] array = {"","","",""};
        String query = "SELECT ADDRESS_ID FROM UM_Impound_Companies WHERE IMPOUND_COMPANY_ID=";
        int addressID = -1;
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                addressID = rs.getInt("ADDRESS_ID");
                array[0] = getAddressStreetFromID(addressID);
                array[1] = getAddressCityFromID(addressID);
                array[2] = getAddressStateFromID(addressID);
                array[3] = getAddressZipFromID(addressID);
            }
            else {
                System.out.println("getImpoundAddressAttributesFromImpoundID returned no results for Company ID: "+ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
            return array;
        }
        return array;
    }
    
    public static int getEmployerID(String employerName, String employerPhone){
        int employerID = -1;
        String query = "SELECT EMPLOYER_ID FROM Employers WHERE NAME=\"";
        query += employerName;
        query += "\" AND PHONE_NUMBER=\""+employerPhone+"\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                employerID = rs.getInt(1);
            }
            else {
                System.out.println("getEmployerID returned no results for Employer:" + employerName +" "+employerPhone);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return employerID;
    }
    
    public static int getUserID(String loginName){
        int userID = -1;
        String query = "SELECT USER_ID FROM Users WHERE LOGIN_NAME=\"";
        query += loginName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                userID = rs.getInt(1);
            }
            else {
                System.out.println("getUserID returned no results for Login Name:" + loginName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return userID;
    }
    
    /**************Methods to get particular fields from DB entries****************/
    
    public static String[] getAddressAttributesFromID(int ID){
        String[] array = {"","","","","",""};
        String query = "SELECT * FROM Addresses WHERE ADDRESS_ID=\""+ID+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                array[0] = rs.getString("NUMBER_AND_STREET");
                array[1] = rs.getString("CITY");
                array[2] = rs.getString("STATE");
                array[3] = rs.getString("ZIP");
                array[4] = rs.getString("COUNTRY");
                array[5] = rs.getString("DISTRICT");
            }
            else {
                System.out.println("getAddressAttributesFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
            return array;
        }
        
        return array;
    }
    
    public static String getAddressStreetFromID(int ID){
        String streetAddress = "";
        String query = "SELECT NUMBER_AND_STREET FROM Addresses WHERE ADDRESS_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                streetAddress = rs.getString(1);
            }
            else {
                System.out.println("getAddressStreetFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return streetAddress;
    }
    
    public static String getAddressCityFromID(int ID){
        String streetAddress = "";
        String query = "SELECT CITY FROM Addresses WHERE ADDRESS_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                streetAddress = rs.getString(1);
            }
            else {
                System.out.println("getAddressCityFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return streetAddress;
    }
    
    public static String getAddressStateFromID(int ID){
        String state = "";
        String query = "SELECT STATE FROM Addresses WHERE ADDRESS_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                state = rs.getString(1);
            }
            else {
                System.out.println("getAddressStateFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return state;
    }
    
    public static String getAddressZipFromID(int ID){
        String zip = "";
        String query = "SELECT ZIP FROM Addresses WHERE ADDRESS_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                zip = rs.getString(1);
            }
            else {
                System.out.println("getAddressZipFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return zip;
    }
    
    public static String getAddressCountryFromID(int ID){
        String country = "";
        String query = "SELECT COUNTRY FROM Addresses WHERE ADDRESS_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                country = rs.getString(1);
            }
            else {
                System.out.println("getAddressCountryFromID returned no results for Address ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return country;
    }
    
    
    public static String[] getEmergencyContactAttributesFromID(int ID){
        String[] array = {"","","","","",""};
        String query = "SELECT * FROM Emergency_Contacts WHERE CONTACT_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                array[0] = rs.getString("FIRST_NAME");
                array[1] = rs.getString("LAST_NAME");
                array[2] = rs.getString("MIDDLE_NAME");
                array[3] = rs.getString("SUFFIX_NAME");
                array[4] = rs.getString("PHONE_NUMBER");
                array[5] = getRelationshipFromID(rs.getInt("RELATIONSHIP_ID"));
            }
            else {
                System.out.println("getEmergencyContactAttributesFromID returned no results for Contact ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
            return array;
        }
        return array;
    }
    
    public static int getEmergencyContactAddressIDFromID(int ID){
        int id = -1;
        String query = "SELECT ADDRESS_ID FROM Emergency_Contacts WHERE CONTACT_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                id = rs.getInt("ADDRESS_ID");
            }
            else {
                System.out.println("getEmergencyContactAddressIDFromID returned no results for Contact ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return id;
    }
    
    
    public static String[] getEmployerAttributesFromID(int ID){
        String[] array = {"","",""};
        String query = "SELECT * FROM Employers WHERE EMPLOYER_ID=\""+ID+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                array[0] = rs.getString("NAME");
                array[1] = rs.getString("BUSINESS_TYPE");
                array[2] = rs.getString("PHONE_NUMBER");
            }
            else {
                System.out.println("getEmployerAttributesFromID returned no results for Employer ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
            return array;
        }
        
        return array;
    }
    
    public static String getEmployerNameFromID(int ID){
        String name = "";
        String query = "SELECT NAME FROM Employers WHERE EMPLOYER_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                name = rs.getString(1);
            }
            else {
                System.out.println("getEmployerNameFromID returned no results for Employer ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return name;
    }
    
    public static String getEmployerTypeFromID(int ID){
        String type = "";
        String query = "SELECT BUSINESS_TYPE FROM Employers WHERE EMPLOYER_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                type = rs.getString(1);
            }
            else {
                System.out.println("getEmployerTypeFromID returned no results for Employer ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return type;
    }
    
    public static String getEmployerPhoneFromID(int ID){
        String number = "";
        String query = "SELECT PHONE_NUMBER FROM Employers WHERE EMPLOYER_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                number = rs.getString(1);
            }
            else {
                System.out.println("getEmployerPhoneFromID returned no results for Employer ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return number;
    }
    
    public static int getEmployerAddressIDFromID(int ID){
        int id = -1;
        String query = "SELECT ADDRESS_ID FROM Employers WHERE EMPLOYER_ID=";
        query += ID;
        query += ";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                id = rs.getInt(1);
            }
            else {
                System.out.println("getEmployerAddressIDFromID returned no results for Employer ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return id;
    }
    
    
    public static String getInmateIDFromName(String firstName, String middleName, String lastName){
        String ID = "";
        String query1 = "SELECT INMATE_ID FROM Inmates WHERE FIRST_NAME=\""+firstName+"\" AND LAST_NAME=\""+lastName+"\";";
        String query2 = "SELECT INMATE_ID FROM Inmates WHERE FIRST_NAME=\""+firstName+"\" AND MIDDLE_NAME=\""+middleName+"\";";
        String query3 = "SELECT INMATE_ID FROM Inmates WHERE MIDDLE_NAME=\""+middleName+"\" AND LAST_NAME=\""+lastName+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            
            if((middleName == null || middleName.isEmpty())&&(lastName == null || lastName.isEmpty())&&(firstName == null || firstName.isEmpty())){
                System.out.println("ERROR from getInmateIDFromName...method requires at least 2 of the parameters to be non-null/non-empty");
                return "";
            }
            else if((firstName == null || firstName.isEmpty())&&(middleName == null || middleName.isEmpty())){
                System.out.println("ERROR from getInmateIDFromName...method requires at least 2 of the parameters to be non-null/non-empty");
                return "";
            }
            else if((firstName == null || firstName.isEmpty())&&(lastName == null || lastName.isEmpty())){
                System.out.println("ERROR from getInmateIDFromName...method requires at least 2 of the parameters to be non-null/non-empty");
                return "";
            }
            else if((middleName == null || middleName.isEmpty())&&(lastName == null || lastName.isEmpty())){
                System.out.println("ERROR from getInmateIDFromName...method requires at least 2 of the parameters to be non-null/non-empty");
                return "";
            }
            else if(middleName == null || middleName.isEmpty()){
                rs = statement.executeQuery(query1);
            }
            else if(lastName == null || lastName.isEmpty()){
                rs = statement.executeQuery(query2);
            }
            else if(firstName == null || firstName.isEmpty()){
                rs = statement.executeQuery(query3);
            }

            if(rs.next()) {
                ID = rs.getString("INMATE_ID");
            }
            else {
                System.out.println("getEmployerAddressIDFromID returned no results for Inmate Name: "+firstName+","+middleName+","+lastName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return ID;
    }
    
    public static String[] getInmateAttributesForJesus(String ID){
        String[] array = {"","","","","","",""};
        String query = "SELECT * FROM Inmates WHERE INMATE_ID=\""+ID+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                array[0] = rs.getString("FIRST_NAME");
                array[1] = rs.getString("LAST_NAME");
                array[2] = rs.getString("MIDDLE_NAME");
                array[3] = rs.getString("SUFFIX_NAME");
                array[4] = rs.getString("SEX");
                array[5] = rs.getString("DESCENT");
                array[6] = rs.getString("DOB");
            }
            else {
                System.out.println("getInmateFirstNameFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
            return array;
        }
        
        return array;
    }
    
    public static String getInmateFirstNameFromID(String ID){
        String firstName = "";
        String query = "SELECT FIRST_NAME FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                firstName = rs.getString(1);
            }
            else {
                System.out.println("getInmateFirstNameFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return firstName;
    }
    
    public static String getInmateLastNameFromID(String ID){
        String lastName = "";
        String query = "SELECT LAST_NAME FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                lastName = rs.getString(1);
            }
            else {
                System.out.println("getInmateLastNameFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return lastName;
    }
    
    public static String getInmateMiddleNameFromID(String ID){
        String middleName = "";
        String query = "SELECT MIDDLE_NAME FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                middleName = rs.getString(1);
            }
            else {
                System.out.println("getInmateMiddleNameFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return middleName;
    }
    
    public static String getInmateSuffixFromID(String ID){
        String suffix = "";
        String query = "SELECT SUFFIX_NAME FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                suffix = rs.getString(1);
            }
            else {
                System.out.println("getInmateSuffixFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return suffix;
    }
    
    public static String getInmateSexFromID(String ID){
        String sex = "";
        String query = "SELECT SEX FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                sex = rs.getString(1);
            }
            else {
                System.out.println("getInmateSexFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return sex;
    }
    
    public static String getInmateDescentFromID(String ID){
        String descent = "";
        String query = "SELECT DESCENT FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                descent = rs.getString(1);
            }
            else {
                System.out.println("getInmateDescentFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return descent;
    }
    
    public static String getInmateDOBFromID(String ID){
        String dob = "";
        String query = "SELECT DOB FROM Inmates WHERE INMATE_ID=\"";
        query += ID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                dob = rs.getString(1);
            }
            else {
                System.out.println("getInmateDOBFromID returned no results for Inmate ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return dob;
    }
    
    
    public static String getUserRoleFromUsername(String username){
        String role = "";
        String query = "SELECT USER_ROLE FROM Users WHERE LOGIN_NAME=\"";
        query += username;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                role = rs.getString(1);
            }
            else {
                System.out.println("getUserRoleFromUsername returned no results for login name: " + username);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return role;
    }
    
    /****************Methods to check if entries are in the DB*******************/
    
    public static boolean isInmateInDB(String inmateID){
        boolean inmateFound = false;
        String query = "SELECT INMATE_ID FROM Inmates WHERE INMATE_ID=\"";
        query += inmateID;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                inmateFound = true;
            }
            else {
                System.out.println("isInmateInDB returned no results for inmate:" + inmateID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return inmateFound;
    }
    
    public static boolean isImpoundCompanyInDB(String companyName){
        boolean companyFound = false;
        String query = "SELECT IMPOUND_COMPANY_ID FROM UM_Impound_Companies WHERE NAME=\"";
        query += companyName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                companyFound = true;
            }
            else {
                System.out.println("isImpoundCompanyInDB returned no results for company:" + companyName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return companyFound;
    }
    
    public static boolean isUserLoginNameInDB(String loginName){
        boolean loginNameFound = false;
        String query = "SELECT USER_ID FROM Users WHERE LOGIN_NAME=\"";
        query += loginName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                loginNameFound = true;
            }
            else {
                System.out.println("isUserLoginNameInDB returned no results for Login Name:" + loginName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return loginNameFound;
    }
    
    public static boolean isDepartmentInDB(String departmentName){
        boolean departmentFound = false;
        String query = "SELECT DEPARTMENT_ID FROM Departments WHERE NAME=\"";
        query += departmentName;
        query += "\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                departmentFound = true;
            }
            else {
                System.out.println("isDepartmentInDB returned no results for Department:" + departmentName);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return departmentFound;
    }
    
    public static boolean isEmployerInDB(String employerName, String employerPhone){
        boolean employerFound = false;
        String query = "SELECT EMPLOYER_ID FROM Employers WHERE NAME=\"";
        query += employerName;
        query += "\" AND PHONE_NUMBER=\""+employerPhone+"\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                employerFound = true;
            }
            else {
                System.out.println("isEmployerInDB returned no results for Employer:" + employerName +" "+employerPhone);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return employerFound;
    }
    
    public static boolean isVehicleInDB(String plateNumber, String plateState){
        boolean vehicleFound = false;
        String query = "SELECT VEHICLE_ID FROM Vehicles WHERE PLATE_NUMBER=\"";
        query += plateNumber;
        query += "\" AND PLATE_STATE=\""+plateState+"\";";

        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                vehicleFound = true;
            }
            else {
                System.out.println("isVehicleInDB returned no results for Vehicle:" + plateState +" "+plateNumber);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        return vehicleFound;
    }
    
    /*************Methods to get daily booking information****************/
    
    public static ArrayList<Jesus> getDailyLogForToday(){
        ArrayList<Jesus> dailyEntries = new ArrayList<Jesus>();
        int arrestAddyID = -1;
        String inmateID = "";
        
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();
        String todaysDate = dateFormat.format(date);
        
        String queryBooking = "SELECT * FROM Bookings WHERE DATE_BOOKED=\""+todaysDate+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(queryBooking);

            while(rs.next()) {
                Jesus entry = new Jesus();
                arrestAddyID = rs.getInt("ARREST_ADDRESS_ID");
                inmateID = rs.getString("INMATE_ID");
                entry.arrestLocation = getAddressStreetFromID(arrestAddyID)+", "+getAddressCityFromID(arrestAddyID);
                entry.arrestingAgency = rs.getString("ARRESTING_AGENCY");
                entry.bookingNumber = String.valueOf(rs.getInt("BOOKING_ID"));
                entry.bookingTime = rs.getString("TIME_BOOKED");
                entry.charges = rs.getString("CHARGES");
                
                String[] inmate = getInmateAttributesForJesus(inmateID);
                entry.firstName = inmate[0];
                entry.lastName = inmate[1];
                entry.middleName = inmate[2];
                entry.suffix = inmate[3];
                entry.sex = inmate[4];
                entry.race = inmate[5];
                entry.DOB = inmate[6];
                
                dailyEntries.add(entry);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return dailyEntries;
    }
    
    public static ArrayList<Jesus> getDailyLogForDate(String date){
        ArrayList<Jesus> dailyEntries = new ArrayList<Jesus>();
        int arrestAddyID = -1;
        String inmateID = "";
        
        String queryBooking = "SELECT * FROM Bookings WHERE DATE_BOOKED=\""+date+"\";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(queryBooking);

            while(rs.next()) {
                Jesus entry = new Jesus();
                arrestAddyID = rs.getInt("ARREST_ADDRESS_ID");
                inmateID = rs.getString("INMATE_ID");
                entry.arrestLocation = getAddressStreetFromID(arrestAddyID)+", "+getAddressCityFromID(arrestAddyID);
                entry.arrestingAgency = rs.getString("ARRESTING_AGENCY");
                entry.bookingNumber = String.valueOf(rs.getInt("BOOKING_ID"));
                entry.bookingTime = rs.getString("TIME_BOOKED");
                entry.charges = rs.getString("CHARGES");
                
                String[] inmate = getInmateAttributesForJesus(inmateID);
                entry.firstName = inmate[0];
                entry.lastName = inmate[1];
                entry.middleName = inmate[2];
                entry.suffix = inmate[3];
                entry.sex = inmate[4];
                entry.race = inmate[5];
                entry.DOB = inmate[6];
                
                dailyEntries.add(entry);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        return dailyEntries;
    }
    
    /****************Methods that return Inmate objects******************/
    
    public static ArrayList<InmateFromDB> getAllBookingsForInmateFromInmateID(String ID){
        ArrayList<InmateFromDB> allBookingsForInmate = new ArrayList<InmateFromDB>();
        String queryBooking = "SELECT * FROM Bookings WHERE INMATE_ID=\""+ID+"\";";
        String queryInmate = "SELECT * FROM Inmates WHERE INMATE_ID=\""+ID+"\";";
        InmateFromDB inmate = new InmateFromDB();
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(queryInmate);

            if(rs.next()) {
                inmate.ID = ID;
                inmate.firstName = rs.getString("FIRST_NAME");
                inmate.lastName = rs.getString("LAST_NAME");
                inmate.middleName = rs.getString("MIDDLE_NAME");
                inmate.suffix = rs.getString("SUFFIX_NAME");
                inmate.dob = rs.getString("DOB");
                inmate.permanentTelephone = rs.getString("PHONE_NUMBER");
                inmate.temporaryTelephone = rs.getString("TEMP_PHONE_NUMBER");
                inmate.aka = rs.getString("AKAS");
                inmate.foreignNationalStatus = rs.getString("FOREIGN_NATIONAL_STATUS");
                inmate.foreignNationalCountry = rs.getString("FOREIGN_NATIONAL_COUNTRY");
                inmate.SSN = rs.getString("SOCIAL_SECURITY_NUMBER");
                inmate.sex = rs.getString("SEX");
                inmate.descent = rs.getString("DESCENT");
                inmate.hairColor = rs.getString("HAIR_COLOR");
                inmate.eyeColor = rs.getString("EYE_COLOR");
                inmate.height = rs.getString("HEIGHT");
                inmate.weight = rs.getString("WEIGHT");
                inmate.SMT = rs.getString("SCARS_MARKS_TATTOOS");
                inmate.driversLicenseNumber = rs.getString("DL_NUMBER");
                inmate.driversLicenseState = rs.getString("DL_STATE");
                inmate.specialIdentifiers = rs.getString("SPECIAL_IDENTIFIERS");
                inmate.gangAffiliationName = rs.getString("GANG_AFFILIATION_NAME");
                inmate.gangAffiliationCity = rs.getString("GANG_AFFILIATION_CITY");
                inmate.gangAffiliationStatus = rs.getString("GANG_AFFILIATION_STATUS");
                inmate.occupation = rs.getString("OCCUPATION");
                inmate.position = rs.getString("POSITION");
                inmate.skills = rs.getString("SKILLS");
                inmate.highestGradeCompleted = rs.getString("HIGHEST_GRADE_COMPLETED");
                inmate.levelOfEnglish = rs.getString("ENGLISH_ABILITY");
                int permAddyID = rs.getInt("PERM_ADDRESS_ID");
                
                if(permAddyID > 0){
                    String[] permAddy = getAddressAttributesFromID(permAddyID);
                    inmate.permanentAddressStreet = permAddy[0];
                    inmate.permanentAddressCity = permAddy[1];
                    inmate.permanentAddressState = permAddy[2];
                    inmate.permanentAddressZip = permAddy[3];
                }
                
                int tempAddyID = rs.getInt("TEMP_ADDRESS_ID");
                
                if(tempAddyID > 0){
                    String[] tempAddy = getAddressAttributesFromID(tempAddyID);
                    inmate.temporaryAddressStreet = tempAddy[0];
                    inmate.temporaryAddressCity = tempAddy[1];
                    inmate.temporaryAddressState = tempAddy[2];
                    inmate.temporaryAddressZip = tempAddy[3];
                }
                
                int pobAddyID = rs.getInt("PLACE_OF_BIRTH_ADDRESS_ID");
                
                if(pobAddyID > 0){
                    String[] pobAddy = getAddressAttributesFromID(pobAddyID);
                    inmate.placeOfBirthCity = pobAddy[1];
                    inmate.placeOfBirthState = pobAddy[2];
                    inmate.placeOfBirthCountry = pobAddy[4];
                }
                
                int eContactID = rs.getInt("EMERGENCY_CONTACT_ID");
                String[] eContact = getEmergencyContactAttributesFromID(eContactID);
                inmate.eContactFirstName = eContact[0];
                inmate.eContactLastName = eContact[1];
                inmate.eContactMiddleName = eContact[2];
                inmate.eContactTelephone = eContact[4];
                inmate.eContactRelationship = eContact[5];
                int eContactAddyID = getEmergencyContactAddressIDFromID(eContactID);
                
                if(eContactAddyID > 0){
                    String[] eContactAddy = getAddressAttributesFromID(eContactAddyID);
                    inmate.eContactAddressStreet = eContactAddy[0];
                    inmate.eContactAddressCity = eContactAddy[1];
                    inmate.eContactAddressState = eContactAddy[2];
                    inmate.eContactAddressZip = eContactAddy[3];
                }
                
                int employerID = rs.getInt("EMPLOYER_ID");
                
                if(employerID > 0){
                    String[] employer = getEmployerAttributesFromID(employerID);
                    inmate.employerName = employer[0];
                    inmate.employerTypeOfBusiness = employer[1];
                    inmate.employerTelephone = employer[2];
                
                
                    int employerAddyID = getEmployerAddressIDFromID(employerID);
                
                    if(employerAddyID > 0){
                        String[] employerAddy = getAddressAttributesFromID(employerID);
                        inmate.employerAddressStreet = employerAddy[0]; 
                        inmate.employerAddressCity = employerAddy[1];
                        inmate.employerAddressState = employerAddy[2];
                        inmate.employerAddressZip = employerAddy[3];
                    }
                }
            }
            else{
                System.out.println("getAllBookingsFromInmateID couldn't find inmate with ID: "+ID);
                return allBookingsForInmate;
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        //Now do query on bookings, copy inmate to new InmateFromDB's and add to list
        
        try {
            ResultSet rs1 = null;
            Statement statement1 = SQL_Initialize.connection.createStatement();
            rs1 = statement1.executeQuery(queryBooking);

            while(rs1.next()) {
                InmateFromDB fullInmate = new InmateFromDB(inmate);
                fullInmate.dateArrested = rs1.getString("DATE_ARRESTED");
                fullInmate.bookingDate = rs1.getString("DATE_BOOKED");
                fullInmate.timeArrested = rs1.getString("TIME_ARRESTED");
                fullInmate.timeBooked = rs1.getString("TIME_BOOKED");
                fullInmate.arrestCharges = rs1.getString("CHARGES");
                fullInmate.arrestingAgency = rs1.getString("ARRESTING_AGENCY");
                fullInmate.vehicleDisposition = rs1.getString("VEHICLE_DISPOSITION");
                fullInmate.vehicleImpoundCompany = rs1.getString("VEHICLE_IMPOUND_COMPANY");
                fullInmate.arrestBail = rs1.getString("BAIL_AMOUNT");
                fullInmate.cash = rs1.getString("PROPERTY_CASH");
                fullInmate.nonCash = rs1.getString("PROPERTY_NONCASH");
                int arrestAddyID = rs1.getInt("ARREST_ADDRESS_ID");
                
                if(arrestAddyID > 0){
                    String[] arrestAddy = getAddressAttributesFromID(arrestAddyID);
                    fullInmate.arrestLocationStreet = arrestAddy[0];
                    fullInmate.arrestLocationCity = arrestAddy[1];
                }

                fullInmate.vehicleImpoundCompany = rs1.getString("VEHICLE_IMPOUND_COMPANY");
                int vehicleImpoundID = getImpoundCompanyID(rs1.getString("VEHICLE_IMPOUND_COMPANY"));
                
                if(vehicleImpoundID > 0){
                    String[] impoundAddress = getImpoundAddressAttributesFromImpoundID(vehicleImpoundID);
                    fullInmate.vehicleImpoundCompanyAddressStreet = impoundAddress[0];
                    fullInmate.vehicleImpoundCompanyAddressCity = impoundAddress[1];
                    fullInmate.vehicleImpoundCompanyAddressState = impoundAddress[2];
                    fullInmate.vehicleImpoundCompanyAddressZip = impoundAddress[3];
                }
                
                int vehicleID = rs1.getInt("VEHICLE_ID");
                
                if(vehicleID > 0){
                    String[] vehicle = getVehicleAttributesFromID(vehicleID);
                    fullInmate.vehiclePlateNumber = vehicle[0];
                    fullInmate.vehiclePlateState = vehicle[1];
                    fullInmate.vehicleMake = vehicle[2];
                    fullInmate.vehicleModel = vehicle[3];
                    fullInmate.vehicleYear = vehicle[4];
                    fullInmate.vehicleColor = getColorFromVehicleID(vehicleID);
                }
                
                int vehicleAddyID = rs1.getInt("VEHICLE_ADDRESS_ID");
                
                if(vehicleAddyID > 0 ){
                    fullInmate.vehicleParkedAddressStreet = getAddressStreetFromID(vehicleAddyID);
                    fullInmate.vehicleParkedAddressCity = getAddressCityFromID(vehicleAddyID);
                    fullInmate.vehicleParkedAddressState = getAddressStateFromID(vehicleAddyID);
                    fullInmate.vehicleParkedAddressZip = getAddressZipFromID(vehicleAddyID);
                }
                
                int arrestDeptID = rs1.getInt("ARRESTING_OFFICER_DEPT_ID");
                if(arrestDeptID > 0)
                    fullInmate.arrestingOfficerDepartment = getDepartmentNameFromID(arrestDeptID);
                
                int transDeptID = rs1.getInt("TRANSPORT_OFFICER_DEPT_ID");
                if(transDeptID > 0)
                    fullInmate.transportingOfficerDepartment = getDepartmentNameFromID(transDeptID);
                
                int bookingDeptID = rs1.getInt("BOOKING_CLERK_DEPT_ID");
                if(bookingDeptID > 0)
                    fullInmate.bookingClerkDepartment = getDepartmentNameFromID(bookingDeptID);
                
                int searchingDeptID = rs1.getInt("SEARCHING_CLERK_DEPT_ID");
                if(searchingDeptID > 0)
                    fullInmate.searchingOfficerDepartment = getDepartmentNameFromID(searchingDeptID);
                
                allBookingsForInmate.add(fullInmate);
            }

            statement1.close();
            rs1.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        
        return allBookingsForInmate;
    }
    
    public static ArrayList<InmateFromDB> getAllBookingsForInmateFromBookingID(int ID){
        ArrayList<InmateFromDB> allBookingsForInmate = new ArrayList<InmateFromDB>();
        String inmateID = "";
        String query = "SELECT INMATE_ID FROM Bookings WHERE BOOKING_ID=";
        query += ID;
        query += ";";
        
        try {
            ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rs = statement.executeQuery(query);

            if(rs.next()) {
                inmateID = rs.getString("INMATE_ID");
            }
            else {
                System.out.println("getAllBookingsForInmateFromBookingID could not find an inmate for booking ID: " + ID);
            }

            statement.close();
            rs.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        allBookingsForInmate = getAllBookingsForInmateFromInmateID(inmateID);
        return allBookingsForInmate;
    }
    
    public static ArrayList<InmateFromDB> getAllBookingsForInmateFromName(String firstName, String middleName, String lastName){
        ArrayList<InmateFromDB> list = new ArrayList<InmateFromDB>();
        String ID = "";
        ID = getInmateIDFromName(firstName, middleName, lastName);
        list = getAllBookingsForInmateFromInmateID(ID);
        return list;
    }
    
}
