/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import java.util.ArrayList;
import database.*;

/**
 *
 * @author cello
 */
public class SQL_REMOVE {
    
    public static int removeEntryForID(int ID, String IDColumnName, String table){
        int rowsRemoved = -1;
        String query = "DELETE FROM "+ table + " WHERE "+ IDColumnName + "=" + ID + ";";
        
        try {
            //ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rowsRemoved = statement.executeUpdate(query);

            statement.close();
            //rs.close();
        } catch (SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        if(rowsRemoved < 1)
            System.out.println("removeEntryForID could not find ID "+ID+" in "+table);
        
        return rowsRemoved;
    }
    
    public static int removeEntryForID(String ID, String IDColumnName, String table){
        int rowsRemoved = -1;
        String query = "DELETE FROM "+ table + " WHERE "+ IDColumnName + "=\"" + ID + "\";";
        
        try {
            //ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rowsRemoved = statement.executeUpdate(query);

            statement.close();
            //rs.close();
        } catch (SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }
        
        if(rowsRemoved < 1)
            System.out.println("removeEntryForID could not find an entry for "+ID+" in "+table);
        
        return rowsRemoved;
    }
    
    public static int removeInmateForID(String ID){
        if(ID.isEmpty())
            return -1;
        int rowsRemoved = removeEntryForID(ID, "INMATE_ID", "Inmates");
        return rowsRemoved;
    }
    
    public static int removeAddressForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "ADDRESS_ID","Addresses");
        return rowsRemoved;
    }
    
    public static int removeEmergencyContactForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "CONTACT_ID","Emergency_Contacts");
        return rowsRemoved;
    }
    
    public static int removeBookingForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "BOOKING_ID","Bookings");
        return rowsRemoved;
    }
    
    public static int removeEmployerForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "EMPLOYER_ID","Employers");
        return rowsRemoved;
    }
    
    public static int removeVehicleForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "VEHICLE_ID","Vehicles");
        return rowsRemoved;
    }
    
    public static int removeDepartmentForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "DEPARTMENT_ID","Departments");
        return rowsRemoved;
    }
    
    public static int removeUserForID(int ID){
        if(ID < 1)
            return -1;
        int rowsRemoved = removeEntryForID(ID, "USER_ID","Users");
        return rowsRemoved;
    }
    
    /*
    *********************Remove methods for UM tables**********************
    */
    
    public static int removeFromUMArrestCharges(int ID){
        if(ID < 1){
            System.out.println("removeFromUMArrestCharges found no Arrest Charge with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "ARREST_CHARGE_ID","UM_Arrest_Charges");
        return rowsRemoved;
    }
    
    public static int removeFromUMArrestCharges(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMArrestCharges cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Arrest_Charges");
        return rowsRemoved;
    }
    
    public static int removeFromUMColors(int ID){
        if(ID < 1){
            System.out.println("removeFromUMColors found no Color with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "COLOR_ID","UM_Colors");
        return rowsRemoved;
    }
    
    public static int removeFromUMColors(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMColors cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Colors");
        return rowsRemoved;
    }
    
    public static int removeFromUMDescent(int ID){
        if(ID < 1){
            System.out.println("removeFromUMDescent found no Descent with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "DESCENT_ID","UM_Descent");
        return rowsRemoved;
    }
    
    public static int removeFromUMDescent(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMDescent cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Descent");
        return rowsRemoved;
    }
    
    public static int removeFromUMEnglishAbility(int ID){
        if(ID < 1){
            System.out.println("removeFromUMEnglishAbility found no Ability with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "ENGLISH_ABILITY_ID","UM_English_Ability");
        return rowsRemoved;
    }
    
    public static int removeFromUMEnglishAbility(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMEnglishAbility cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_English_Ability");
        return rowsRemoved;
    }
    
    public static int removeFromUMEyeColor(int ID){
        if(ID < 1){
            System.out.println("removeFromUMEyeColor found no Eye Color with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "EYE_COLOR_ID","UM_Eye_Color");
        return rowsRemoved;
    }
    
    public static int removeFromUMEyeColor(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMEyeColor cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Eye_Color");
        return rowsRemoved;
    }
    
    public static int removeFromUMGangs(int ID){
        if(ID < 1){
            System.out.println("removeFromUMGangs found no Gang with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "GANG_ID","UM_Gangs");
        return rowsRemoved;
    }
    
    public static int removeFromUMGangs(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMGangs cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Gangs");
        return rowsRemoved;
    }
    
    public static int removeFromUMHairColor(int ID){
        if(ID < 1){
            System.out.println("removeFromUMHairColor found no Hair Color with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "HAIR_COLOR_ID","UM_Hair_Color");
        return rowsRemoved;
    }
    
    public static int removeFromUMHairColor(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMHairColor cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Hair_Color");
        return rowsRemoved;
    }
    
    public static int removeFromUMRelationships(int ID){
        if(ID < 1){
            System.out.println("removeFromUMRelationships found no Relationship with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "RELATIONSHIP_ID","UM_Relationships");
        return rowsRemoved;
    }
    
    public static int removeFromUMRelationships(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMRelationships cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Relationships");
        return rowsRemoved;
    }
    
    public static int removeFromUMSkills(int ID){
        if(ID < 1){
            System.out.println("removeFromUMSkills found no Skill with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "SKILL_ID","UM_Skills");
        return rowsRemoved;
    }
    
    public static int removeFromUMSkills(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMSkills cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Skills");
        return rowsRemoved;
    }
    
    public static int removeFromUMVehicleMakes(int ID){
        if(ID < 1){
            System.out.println("removeFromUMVehicleMakes found no Makes with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "MAKE_ID","UM_Vehicle_Makes");
        return rowsRemoved;
    }
    
    public static int removeFromUMVehicleMakes(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMVehicleMakes cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Vehicle_Makes");
        return rowsRemoved;
    }
    
    public static int removeFromUMVehicleModels(int ID){
        if(ID < 1){
            System.out.println("removeFromUMVehicleModels found no Models with ID: "+ID);
            return -1;
        }
        int rowsRemoved = removeEntryForID(ID, "MODEL_ID","UM_Vehicle_Models");
        return rowsRemoved;
    }
    
    public static int removeFromUMVehicleModels(String name){
        if(name.isEmpty()){
            System.out.println("removeFromUMVehicleModels cannot take an empty string for an argument");
            return -1;
        }
        int rowsRemoved = removeEntryForID(name, "NAME","UM_Vehicle_Models");
        return rowsRemoved;
    }
    
}
