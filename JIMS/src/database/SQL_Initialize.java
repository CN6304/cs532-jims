package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Provides the SQL_Initialize database basic login information.
 */
public class SQL_Initialize {

    public static final String DB_URL = "jdbc:mysql://db4free.net/cs532jimsteam";
    public static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    public static final String USERNAME = "jimsuser";
    public static final String PASSWORD = "idaho123";
    public static final String table_users = "Users";

    public static Connection connection;

    /**
     * Connects to the SQL_Initialize database.
     *
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    public static void SQLSetup() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER); // Register JDBC driver
        connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD); // Open a connection.
    }

    public static boolean isRunning() throws SQLException {
        return !(connection.isClosed() || connection == null);
    }


    /*
     //Insert elements into the database table.
     public static void insertNewUser(String table) {
     try {
     // Execute a query
     Statement statement = connection.createStatement();
     //statement.executeUpdate("INSERT INTO " + table + " VALUES (" + '\'' + dog + '\'' + "," + age + ")");
     statement.close();

     }catch(SQLException e){
     e.printStackTrace();
     }
     }*/
}
