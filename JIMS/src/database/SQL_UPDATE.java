/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author marcelo
 */
public class SQL_UPDATE {

    public static int updateBookingsForInmateWithID(String oldID, String newID) {
        int rowsUpdated = -1;
        String query = "UPDATE Bookings SET INMATE_ID=\"" + newID + "\" WHERE INMATE_ID=\"" + oldID + "\";";

        try {
            //ResultSet rs = null;
            Statement statement = SQL_Initialize.connection.createStatement();
            rowsUpdated = statement.executeUpdate(query);

            statement.close();
        }
        catch(SQLException e) {
            System.out.println("SQL Error: " + e.getMessage());
        }

        if(rowsUpdated < 1) {
            System.out.println("updateBookingsForInmateWithID did not update any booking entries");
        }

        return rowsUpdated;
    }
}
