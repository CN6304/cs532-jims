package login;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Allows varies use of password tools.
 */
public class PasswordEncrypt {
    /**
     * Encrypts a password to a hash value via MD5 hashing standards.
     * @param pass - The password to hash.
     * @return - The successfully hashed string; otherwise returns null.
     */
    public static String cryptPassword(String pass){
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            byte[] passBytes = pass.getBytes();
            md.reset();
            byte[] digested = md.digest(passBytes);
            StringBuffer sb = new StringBuffer();
            for(int i=0; i < digested.length; i++){
                sb.append(Integer.toHexString(0xff & digested[i]));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }
}
