/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package login;

/**
 *
 * @author Chris
 */
public class User {

    public static String username;
    public static String userid;
    public static String rank;

    public static void setUserName(String name) {
        username = name;
    }

    public static String getUserName() {
        return username;
    }

    public static void setUserId(String id) {
        userid = id;
    }

    public static String getUserId() {
        return userid;
    }

    public static String getDisplayName() {
        return username + " (ID#" + userid + ")";
    }

    public static void setRank(String r) {
        rank = r;
    }

    public static String getRank() {
        return rank;
    }
}
