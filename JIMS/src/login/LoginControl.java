package login;

import database.SQL_GET;
import database.SQL_Initialize;
import error_handle.HandlePopUp;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import window_manager.WindowManager;

/**
 * The controller class for the GUI_login interface; allows use of the GUI.
 */
public class LoginControl implements Initializable {

    @FXML
    private Text textPrompt;
    @FXML
    private TextField userIdTextField;
    @FXML
    private PasswordField passwordTextField;
    @FXML
    private Button loginButtonText;

    @FXML
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        // Exits the program when the use exits the login window.
        WindowManager.loginStage.setOnCloseRequest((WindowEvent event) -> {
            System.exit(0);
        });
    }

    /**
     * Starts when the user presses the "Log In" button.
     *
     * @throws java.io.IOException
     * @throws java.sql.SQLException
     */
    @FXML
    protected void loginButtonPressed() throws IOException, SQLException {// Attempts to connect with the SQL_Initialize DB.
        if(SQL_Initialize.isRunning()) {
            String username = userIdTextField.getText(), password = passwordTextField.getText();

            Boolean validLogin = ValidationCheck.isValidLoginInfo(username, password);  // Check login validity.

            if(validLogin) { // Valid info entered.
                User.setUserName(username);
                User.setUserId(Integer.toString(SQL_GET.getUserID(username)));
                User.setRank(SQL_GET.getUserRoleFromUsername(username));
                WindowManager.openMainWindow();
                WindowManager.closeLoginWindow(); // Close the login window.
            }

            // Empty username and password.
            else if(username.equals("") && password.equals("")) {
                HandlePopUp.show("JIMS Login Error", "Cannot log in for the following reasons:\n\n"
                        + "• Missing USER ID.\n"
                        + "• Missing PASSWORD.\n\n"
                        + "Please enter a valid USER ID and PASSWORD.");
            }
            // Empty username.
            else if(username.equals("")) {
                HandlePopUp.show("JIMS Login Error", "Cannot log in for the following reasons:\n\n"
                        + "• Missing USER ID.\n\n"
                        + "Please enter a valid USER ID and PASSWORD.");
            }
            // Empty password.
            else if(password.equals("")) { // Empty username.
                HandlePopUp.show("JIMS Login Error", "Cannot log in for the following reasons:\n\n"
                        + "• Missing PASSWORD.\n\n"
                        + "Please enter a valid USER ID and PASSWORD.");
            }
            // Incorrect/Mismatch password or ID.
            else {
                HandlePopUp.show("JIMS Login Error", "Cannot log in for the following reason(s):\n\n"
                        + "• Invalid User ID and/or PASSWORD.\n\n"
                        + "Please enter a valid USER ID and PASSWORD.");
            }
        }
        // Connection NOT successful. Disable all text fields.
        else {
            userIdTextField.setDisable(true);
            passwordTextField.setDisable(true);
            loginButtonText.setDisable(true);
            HandlePopUp.show("JIMS Connection Error", "Unable to connect to server. Please contact admin for support.");
        }
    }

    @FXML
    protected void loginKeyPressed(KeyEvent event) throws IOException, SQLException {
        // Enter key is pressed.
        if(event.getCode() == KeyCode.ENTER) {
            loginButtonPressed();
        }
    }
}
