package login;

import database.SQL_Initialize;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Access the SQL_Initialize database to verify user's login information.
 */
public class ValidationCheck {

    /**
     * Checks if the entered username and passwords are store in the SQL_Initialize database.
     *
     * @param username - The username entered by the user.
     * @param password - The password entered by the user.
     * @return - TRUE if both username and password matches. FALSE if not.
     */
    public static Boolean isValidLoginInfo(String username, String password) throws SQLException {
        // Execute a query
        Statement statement = SQL_Initialize.connection.createStatement();
        ResultSet resultset = statement.executeQuery("SELECT LOGIN_NAME, LOGIN_PASSWORD FROM " + SQL_Initialize.table_users);

        // Extract data from result set.
        while(resultset.next()) {
            if(resultset.getString("LOGIN_NAME").equals(username)
                    && resultset.getString("LOGIN_PASSWORD").equals(PasswordEncrypt.cryptPassword(password))) {
                return true;
            }
        }
        statement.close();
        resultset.close();

        return false;
    }

    /**
     * Finds the user in the database and stamps the user's current login date and time.
     *
     * @param userId - The user's username (or id).
     */
    public static void updateUserLastLogin(String userId) throws SQLException {
        PreparedStatement statement
                = SQL_Initialize.connection.prepareStatement("UPDATE users SET LAST_LOGIN = ? " + "WHERE USER_ID = ?");
        statement.setString(1, stampCurrentDateTime());
        statement.setString(2, userId);
        statement.executeUpdate();
        statement.close();
    }

    /**
     * Stamps the current time and date.
     *
     * @return - The current date and time in the format: "YYYY-MM-DD HH:MM:SS".
     */
    public static String stampCurrentDateTime() {
        return new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
    }
}
