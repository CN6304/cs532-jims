package daily_log;

import com.itextpdf.text.DocumentException;
import database.SQL_GET;
import inmate.Inmate;
import inmate.InmateFromDB;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import report.CreateForm;
import window_manager.WindowManager;

public class DailyLogControl implements Initializable {

    @FXML
    private TableView<ViewableInmate> inmateTable;
    @FXML
    private TableColumn<ViewableInmate, String> columnLastName, columnFirstName, columnMiddleName, columnSuffix,
            columnBookingNumber, columnBookingTime, columnDOB, columnArrestAgency, columnArrestLocation,
            columnSex, columnRace, columnCharges;

    ObservableList<ViewableInmate> viewableInmateList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        inmateTable.setStyle("-fx-focus-color: transparent;");

        // Sets up the table column with ViewableInmates
        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnMiddleName.setCellValueFactory(new PropertyValueFactory<>("middleName"));
        columnSuffix.setCellValueFactory(new PropertyValueFactory<>("suffix"));
        columnBookingNumber.setCellValueFactory(new PropertyValueFactory<>("bookingNumber"));
        columnBookingTime.setCellValueFactory(new PropertyValueFactory<>("bookingTime"));
        columnDOB.setCellValueFactory(new PropertyValueFactory<>("DOB"));
        columnArrestAgency.setCellValueFactory(new PropertyValueFactory<>("arrestAgency"));
        columnArrestLocation.setCellValueFactory(new PropertyValueFactory<>("arrestLocation"));
        columnSex.setCellValueFactory(new PropertyValueFactory<>("sex"));
        columnRace.setCellValueFactory(new PropertyValueFactory<>("race"));
        columnCharges.setCellValueFactory(new PropertyValueFactory<>("charges"));

        inmateTable.setItems(viewableInmateList); // Add all columns to table.
        JesusSetup();
    }

    @FXML
    void refreshButton() {
        JesusSetup();
    }

    @FXML
    void quickPrintButton() throws IOException, DocumentException {
        if(!inmateTable.getSelectionModel().isEmpty()) { // A selection is highlighted.
            ViewableInmate temp = inmateTable.getSelectionModel().getSelectedItem();
            // MAKE PRINT
            InmateFromDB fromdb = SQL_GET.getAllBookingsForInmateFromBookingID(Integer.parseInt(temp.getBookingNumber())).get(0);
            Inmate in = new Inmate(fromdb);

            new CreateForm(in, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE, Boolean.TRUE);
        }
    }

    @FXML
    void displayInmateButton() throws IOException {
        if(!inmateTable.getSelectionModel().isEmpty()) { // A selection is highlighted.
            ViewableInmate temp = inmateTable.getSelectionModel().getSelectedItem();
            // MAKE PRINT
            InmateFromDB fromdb = SQL_GET.getAllBookingsForInmateFromBookingID(Integer.parseInt(temp.getBookingNumber())).get(0);
            Inmate in = new Inmate(fromdb);

            WindowManager.openDisplayWindow(in);
        }
    }

    private void JesusSetup() {
        viewableInmateList.clear();
        ObservableList<ViewableInmate> obList = FXCollections.observableArrayList();
        ArrayList<Jesus> jesus = SQL_GET.getDailyLogForToday();

        for(Jesus ele : jesus) {
            obList.add(new ViewableInmate(ele.bookingNumber, ele.bookingTime, ele.lastName, ele.firstName, ele.middleName,
                    ele.suffix, ele.DOB, ele.arrestingAgency, ele.arrestLocation, ele.sex, ele.race, ele.charges));
        }
        viewableInmateList.addAll(obList);
    }
}
