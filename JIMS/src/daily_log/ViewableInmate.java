package daily_log;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Used to present inmate object information in a viewable format.
 */
public class ViewableInmate {

    private final StringProperty lastName, firstName, middleName,
            suffix, bookingNumber, bookingTime, DOB, arrestAgency,
            arrestLocation, sex, race, charges;

    public ViewableInmate(String bookingNumber, String bookingTime, String lastName, String firstName, String middleName, String suffix,
            String DOB, String arrestAgency, String arrestLocation, String sex, String race, String charges) {

        this.lastName = new SimpleStringProperty(lastName);
        this.firstName = new SimpleStringProperty(firstName);
        this.middleName = new SimpleStringProperty(middleName);
        this.suffix = new SimpleStringProperty(suffix);
        this.bookingNumber = new SimpleStringProperty(bookingNumber);
        this.bookingTime = new SimpleStringProperty(bookingTime);
        this.DOB = new SimpleStringProperty(DOB);
        this.arrestAgency = new SimpleStringProperty(arrestAgency);
        this.arrestLocation = new SimpleStringProperty(arrestLocation);
        this.sex = new SimpleStringProperty(sex);
        this.race = new SimpleStringProperty(race);
        this.charges = new SimpleStringProperty(charges);
    }

///////////
    public String getFirstName() {
        return firstName.get();
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public StringProperty firstNameProperty() {
        return firstName;
    }
////////////

    public String getLastName() {
        return lastName.get();
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public StringProperty lastNameProperty() {
        return lastName;
    }
//////////

    public String getMiddleName() {
        return middleName.get();
    }

    public void setMiddleName(String middleName) {
        this.middleName.set(middleName);
    }

    public StringProperty middleNameProperty() {
        return middleName;
    }
//////////

    public String getSuffix() {
        return suffix.get();
    }

    public void setSuffix(String suffix) {
        this.suffix.set(suffix);
    }

    public StringProperty suffixProperty() {
        return suffix;
    }
///////////

    public String getBookingNumber() {
        return bookingNumber.get();
    }

    public void setBookingNumber(String bookingNumber) {
        this.bookingNumber.set(bookingNumber);
    }

    public StringProperty bookingNumberProperty() {
        return bookingNumber;
    }
////////////    

    public String getBookingTime() {
        return bookingTime.get();
    }

    public void setBookingTime(String bookingTime) {
        this.bookingTime.set(bookingTime);
    }

    public StringProperty bookingTimeProperty() {
        return bookingTime;
    }
///////////

    public String getDOB() {
        return DOB.get();
    }

    public void setDOB(String DOB) {
        this.DOB.set(DOB);
    }

    public StringProperty DOBProperty() {
        return DOB;
    }
//////////////

    public String getArrestAgency() {
        return arrestAgency.get();
    }

    public void setArrestAgency(String arrestAgency) {
        this.arrestAgency.set(arrestAgency);
    }

    public StringProperty arrestAgencyProperty() {
        return arrestAgency;
    }
//////////////

    public String getArrestLocation() {
        return arrestLocation.get();
    }

    public void setArrestLocation(String arrestLocation) {
        this.arrestLocation.set(arrestLocation);
    }

    public StringProperty arrestLocationProperty() {
        return arrestLocation;
    }
//////////////

    public String getSex() {
        return sex.get();
    }

    public void setSex(String sex) {
        this.sex.set(sex);
    }

    public StringProperty sexProperty() {
        return sex;
    }
////////////////    

    public String getRace() {
        return race.get();
    }

    public void setRace(String race) {
        this.race.set(race);
    }

    public StringProperty raceProperty() {
        return race;
    }
/////////////////

    public String getCharges() {
        return charges.get();
    }

    public void setCharges(String charges) {
        this.charges.set(charges);
    }

    public StringProperty chargesProperty() {
        return charges;
    }

}
