package admin;

import database.SQL_GET;
import database.SQL_REMOVE;
import database.SQL_SET;
import error_handle.HandlePopUp;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;

public class AdminControl implements Initializable {

    @FXML
    private ComboBox umCB;

    @FXML
    private TextField addListTF, removeListTF;

    @FXML
    private Pane usermaintainPane;

    @FXML
    private Text umlistTitle;

    @FXML
    private ListView<String> umList;

    private String listName;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // I took out "UM_Impound_Companies" for the sake of time.
        umCB.getItems().addAll(FXCollections.observableList(FXCollections.observableArrayList("Arrest Charges", "Colors", "Descent",
                "English Ability", "Eye Color", "Gangs", "Hair Color", "Relationships", "Skills",
                "Vehicle Makes", "Vehicle Models")));

        umCB.valueProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue ov, String t, String t1) {
                listName = umCB.getValue().toString();
                umlistTitle.setText(listName.toUpperCase());
                try {
                    updateList();
                }
                catch(SQLException ex) {
                    Logger.getLogger(AdminControl.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        umList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {

            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if(!umList.getSelectionModel().isEmpty() && getHighlightedClient() != null) { // Has a valid selection from the list.
                    removeListTF.setText(getHighlightedClient());
                }
            }
        });
        umCB.getSelectionModel().select(0);
        try {
            updateList();
        }
        catch(SQLException ex) {
            Logger.getLogger(AdminControl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    void userMaintainButton() {
        usermaintainPane.setVisible(true);
    }

    @FXML
    void addumButton() throws SQLException, IOException {
        if(!addListTF.getText().equals("")) {
            addList(addListTF.getText());
        }
    }

    @FXML
    void removeumButton() throws SQLException, IOException {
        if(!umList.getSelectionModel().isEmpty() && getHighlightedClient() != null) { // Has a valid selection from the list.
            remove(removeListTF.getText());
            removeListTF.setText("");
        }
    }

    private String getHighlightedClient() {
        return umList.getSelectionModel().getSelectedItem();
    }

    private void updateList() throws SQLException {
        SQL_GET fetch = new SQL_GET();
        ObservableList<String> items = null;

        switch(listName) {
            case "Arrest Charges":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getArrestChargesList()));
                break;
            case "Colors":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getVehicleColorList()));
                break;
            case "Descent":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getDescentList()));
                break;
            case "English Ability":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getEnglishAbilityList()));
                break;
            case "Eye Color":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getEyeColorList()));
                break;
            case "Gangs":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getGangsList()));
                break;
            case "Hair Color":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getHairColorList()));
                break;
            //case "UM_Impound_Companies":SQL_SET.addToImpoundCompanies(null, addressID)
            case "Relationships":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getRelationshipsList()));
                break;
            case "Skills":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getSkillsList()));
                break;
            case "Vehicle Makes":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getVehicleMakesList()));
                break;
            case "Vehicle Models":
                items = FXCollections.observableList(FXCollections.observableArrayList(fetch.getVehicleModelList()));
                break;
        }
        if(umList != null) {
            umList.setItems(items);
        }
    }

    private void addList(String element) throws SQLException, IOException {
        switch(listName) {
            case "Arrest Charges":
                SQL_SET.addToUMArrestCharges(element, "");
                break;
            case "Colors":
                SQL_SET.addToUMColors(element);
                break;
            case "Descent":
                SQL_SET.addToUMDescent(element);
                break;
            case "English Ability":
                SQL_SET.addToUMEnglishAbility(element);
                break;
            case "Eye Color":
                SQL_SET.addToUMEyeColor(element);
                break;
            case "Gangs":
                SQL_SET.addToUMGangs(element);
                break;
            case "Hair Color":
                SQL_SET.addToUMHairColor(element);
                break;
            //case "UM_Impound_Companies":SQL_SET.addToImpoundCompanies(null, addressID)
            case "Relationships":
                SQL_SET.addToUMRelationships(element);
                break;
            case "Skills":
                SQL_SET.addToUMSkills(element);
                break;
            case "Vehicle Makes":
                SQL_SET.addToUMVehicleMakes(element);
                break;
            case "Vehicle Models":
                SQL_SET.addToUMVehicleModels(element);
                break;
        }
        updateList();
        addListTF.setText("");
        HandlePopUp.showSuccess("Add to List Success Message", "Successfully added "
                + element + " to " + listName + " table.");
    }

    private void remove(String element) throws SQLException, IOException {
        switch(listName) {
            case "Arrest Charges":
                SQL_REMOVE.removeFromUMArrestCharges(element);
                break;
            case "Colors":
                SQL_REMOVE.removeFromUMColors(element);
                break;
            case "Descent":
                SQL_REMOVE.removeFromUMDescent(element);
                break;
            case "English Ability":
                SQL_REMOVE.removeFromUMEnglishAbility(element);
                break;
            case "Eye Color":
                SQL_REMOVE.removeFromUMEyeColor(element);
                break;
            case "Gangs":
                SQL_REMOVE.removeFromUMGangs(element);
                break;
            case "Hair Color":
                SQL_REMOVE.removeFromUMHairColor(element);
                break;
            //case "UM_Impound_Companies":SQL_SET.addToImpoundCompanies(null, addressID)
            case "Relationships":
                SQL_REMOVE.removeFromUMRelationships(element);
                break;
            case "Skills":
                SQL_REMOVE.removeFromUMSkills(element);
                break;
            case "Vehicle Makes":
                SQL_REMOVE.removeFromUMVehicleMakes(element);
                break;
            case "Vehicle Models":
                SQL_REMOVE.removeFromUMVehicleModels(element);
                break;
        }
        updateList();
        addListTF.setText("");
        HandlePopUp.showSuccess("Remove from List Success Message", "Successfully removed "
                + element + " from " + listName + " table.");
    }
}
