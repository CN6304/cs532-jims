package error_handle;

import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class HandleControl implements Initializable {

    @FXML
    private Label messageField;

    @FXML
    private ImageView handleImage;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        handleImage.setImage(new Image(HandlePopUp.getIcon()));
        messageField.setText(HandlePopUp.getMessage());
    }

    @FXML
    private void okClicked() {
        HandlePopUp.closeHandlePopUp();
    }

    @FXML
    private void okKeyPress(KeyEvent event) {

        // User pressed ESC or ENTER key. Close the popup menu.
        if(Arrays.asList(KeyCode.ENTER, KeyCode.ESCAPE).contains(event.getCode())) {
            okClicked();
        }
    }
}
