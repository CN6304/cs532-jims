package error_handle;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import window_manager.WindowManager;

/**
 * Manages the error/warning dialog windows (pop ups).
 */
public class HandlePopUp {

    protected static Stage handleStage;

    protected static String handleMessage;
    protected static String handleIcon;

    /**
     * Shows the pop up window to screen.
     *
     * @param type The window type (error, warning, etc.)
     * @param title The window title
     * @param message The window message.
     * @throws IOException
     */
    public static void show(String title, String message) throws IOException {
        handleMessage = message;

        handleIcon = "assets/error_icon.png";

        handleStage = new Stage();
        handleStage.setTitle(title);

        Parent root = FXMLLoader.load(WindowManager.class.getResource("/error_handle/GUI-Handle.fxml"));
        handleStage.setScene(new Scene(root, 490, getEstWindowLength(message)));
        handleStage.setResizable(false); // User cannot resize this window.
        handleStage.initModality(Modality.APPLICATION_MODAL);
        handleStage.initStyle(StageStyle.UTILITY);

        handleStage.show();
    }

    public static void showSuccess(String title, String message) throws IOException {
        handleMessage = message;

        handleIcon = "assets/ok_mark.png";

        handleStage = new Stage();
        handleStage.setTitle(title);

        Parent root = FXMLLoader.load(WindowManager.class.getResource("/error_handle/GUI-Handle.fxml"));
        handleStage.setScene(new Scene(root, 490, getEstWindowLength(message)));
        handleStage.setResizable(false); // User cannot resize this window.
        handleStage.initModality(Modality.APPLICATION_MODAL);
        handleStage.initStyle(StageStyle.UTILITY);

        handleStage.show();
    }

    /**
     * The error message.
     *
     * @return The
     */
    public static String getMessage() {
        return handleMessage;
    }

    /**
     *
     * @return The the window's graphical icon.
     */
    public static String getIcon() {
        return handleIcon;
    }

    /**
     * Close the current error/warning window.
     */
    public static void closeHandlePopUp() {
        handleStage.close();
    }

    /**
     * Helper method to get set the error pop up window's length accordingly.
     *
     * @param message
     * @return
     */
    private static int getEstWindowLength(String message) {
        String[] lines = message.split("\r\n|\r|\n");

        if(((lines.length) * 25 + 85) > 600) {
            return 600;
        }
        return (lines.length) * 25 + 85;
    }
}
