package main;

import daily_log.Jesus;
import daily_log.ViewableInmate;
import database.SQL_GET;
import inmate.Inmate;
import inmate.InmateFromDB;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.WindowEvent;
import login.User;
import window_manager.WindowManager;

public class MainControl implements Initializable {

    @FXML
    private TableView<ViewableInmate> inmateTable;

    @FXML
    private TableColumn<ViewableInmate, String> columnLastName, columnFirstName, columnMiddleName, columnSuffix,
            columnBookingNumber, columnBookingTime, columnDOB, columnArrestAgency, columnArrestLocation,
            columnSex, columnRace, columnCharges;

    @FXML
    private Button adminTools;

    @FXML
    private Text userNameTF;

    ObservableList<ViewableInmate> viewableInmateList = FXCollections.observableArrayList();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(User.rank.equalsIgnoreCase("admin")) {
            adminTools.setDisable(false);
        }
        userNameTF.setText(User.getDisplayName());
        // Exits the program when the use exits the login window.
        WindowManager.mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                System.exit(0);
            }
        });

        inmateTable.setStyle("-fx-focus-color: transparent;");

        // Sets up the table column with ViewableInmates
        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnMiddleName.setCellValueFactory(new PropertyValueFactory<>("middleName"));
        columnSuffix.setCellValueFactory(new PropertyValueFactory<>("suffix"));
        columnBookingNumber.setCellValueFactory(new PropertyValueFactory<>("bookingNumber"));
        columnBookingTime.setCellValueFactory(new PropertyValueFactory<>("bookingTime"));
        columnDOB.setCellValueFactory(new PropertyValueFactory<>("DOB"));
        columnArrestAgency.setCellValueFactory(new PropertyValueFactory<>("arrestAgency"));
        columnArrestLocation.setCellValueFactory(new PropertyValueFactory<>("arrestLocation"));
        columnSex.setCellValueFactory(new PropertyValueFactory<>("sex"));
        columnRace.setCellValueFactory(new PropertyValueFactory<>("race"));
        columnCharges.setCellValueFactory(new PropertyValueFactory<>("charges"));

        inmateTable.setItems(viewableInmateList); // Add all columns to table.
        JesusSetup();
    }

    @FXML
    void refreshButton() {
        JesusSetup();
    }

    @FXML
    void selectInmateButton() throws IOException {
        if(!inmateTable.getSelectionModel().isEmpty()) { // A selection is highlighted.
            ViewableInmate temp = inmateTable.getSelectionModel().getSelectedItem();
            // MAKE PRINT
            InmateFromDB fromdb = SQL_GET.getAllBookingsForInmateFromBookingID(Integer.parseInt(temp.getBookingNumber())).get(0);
            Inmate in = new Inmate(fromdb);

            WindowManager.openDisplayWindow(in);
        }
    }

    @FXML
    void adminToolsButton() throws IOException {
        WindowManager.openAdminWindow();
    }

    /**
     * The FIND INMATE menu item is selected and pressed.
     *
     * @throws java.io.IOException
     */
    @FXML
    private void findInmateMenuItem() throws IOException {
        WindowManager.openDailyLogWindow();
    }

    /**
     * The SIGN OUT menu item is selected and pressed.
     *
     * @throws java.io.IOException
     */
    @FXML
    private void signOutMenuItem() throws IOException {
        // Resets the main screen and prepare for log out.
        WindowManager.closeMainWindow();
        WindowManager.openLoginWindow();
    }

    /**
     * The NEW BOOKING menu item is selected and pressed.
     *
     * @throws java.io.IOException
     */
    @FXML
    private void newBookingMenuItem() throws IOException {
        WindowManager.openBookingWindow();
    }

    @FXML
    void inmateBookingButton() throws IOException {
        newBookingMenuItem();
    }

    @FXML
    void dailyLogButton() throws IOException {
        findInmateMenuItem();
    }

    @FXML
    void signOutButton() throws IOException {
        signOutMenuItem();
    }

    private void JesusSetup() {
        viewableInmateList.clear();
        ObservableList<ViewableInmate> obList = FXCollections.observableArrayList();
        ArrayList<Jesus> jesus = SQL_GET.getDailyLogForToday();

        for(Jesus ele : jesus) {
            obList.add(new ViewableInmate(ele.bookingNumber, ele.bookingTime, ele.lastName, ele.firstName, ele.middleName,
                    ele.suffix, ele.DOB, ele.arrestingAgency, ele.arrestLocation, ele.sex, ele.race, ele.charges));
        }
        viewableInmateList.addAll(obList);
    }
}
