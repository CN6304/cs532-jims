package main;

import database.SQL_Initialize;
import javafx.application.Application;
import javafx.stage.Stage;
import window_manager.WindowManager;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        SQL_Initialize.SQLSetup(); // Setup the SQL_Initialize database.
        WindowManager.openLoginWindow(); // Loads the login stage.
    }

    public static void main(String[] args) {
        launch(args);
    }
}
